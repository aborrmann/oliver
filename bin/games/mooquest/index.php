<?php
include "configs/siteConfig.php";
if ($_SERVER['HTTP_HOST'] == "mooquest.com") { 

header("Location: http://www.mooquest.com/");

}

if(isset($_GET['lang']) && $_GET['lang']=='fr'){
    $session_language = 'fr';  
}else{
  $session_language = 'en';  
}

$colname_userData = "-1";
if (isset($_GET['Sender'])) {
  $colname_userData = (get_magic_quotes_gpc()) ? $_GET['Sender'] : addslashes($_GET['Sender']);
}
if (isset($_GET['Recipient'])) {
  //$colname_userData = (get_magic_quotes_gpc()) ? $_GET['Recipient'] : addslashes($_GET['Recipient']);
}
//=== Tracking system =====================================================
if ($_SERVER['HTTP_USER_AGENT']!='' && substr($_SERVER['HTTP_USER_AGENT'],0,11)!="InterMapper") {
    session_start();
    $debug = 0;
    if(isset($_GET['WebSiteTrackID'])) $WebSiteTrackID = $_GET['WebSiteTrackID'];
    else $WebSiteTrackID = 900000;

    $Session_ID = md5(uniqid(rand(),1));
    $Computer_ID = $Session_ID;
    $Expire = time() + 60*60*24*356*5;
    $IP_Address = $_SERVER['REMOTE_ADDR'];  // get the users IP address

    if(empty($_COOKIE['BangoVisit']))
    {
    	setcookie('BangoVisit',$Computer_ID,$Expire);
    }
    else
    {
    	$Computer_ID = $_COOKIE['BangoVisit'];
    }

    if(empty($_SESSION['Tracking_Flag']))
    {

    include $server_address . "/tracking.php?Session_ID=" . $Session_ID . "&Computer_ID=" . $Computer_ID . "&IP_Address=" . $IP_Address . "&WebSiteTrackID=" . $WebSiteTrackID . "&CPUI=18&colname_userData=" . $colname_userData . "&session_language=" . $session_language . "&user_agent=" .$_SERVER['HTTP_USER_AGENT'];	

    	$_SESSION['Tracking_Flag'] = $Session_ID;
    	//echo "done";
    }
    else
    {
    	$Session_ID = $_SESSION['Tracking_Flag'];
    	//echo "nahhhh";
    }
}


//=== End of Tracking system ===

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php if($session_language=="fr") echo "Natrel pr&eacute;sent MooQuest"; else echo "Natrel presents MooQuest � Play Games to Win Awesome Prizes"; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="MooQuest is a fun and exciting turn-based game created by Natrel where users try to produce as much milk as possible for the chance to win great weekly prizes." />
<meta name="keywords" content="Natrel, Mooquest, Game, Trapeze, Flash, Contest" />

<link REL="SHORTCUT ICON" HREF="favicon.ico">
<script language="JavaScript"  src="./js/swfobject.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./css/main.css">
<link ref="favicon" HREF="http://www.mooquest.ca/favicon.ico">

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">

var pageTracker = _gat._getTracker("UA-4370662-2");

pageTracker._initData();

pageTracker._trackPageview();

</script>

</head>

<body>

<!-- Google Analytics ========= -->
<!-- END Google Analytics -->

<body>
    <!-- Embed the Flash object  =========================  -->
<div id="container">
	<div id="flashcontent">
		<!-- search friendly text here-->
	</div>
	<script type="text/javascript">
	   var so = new SWFObject("./WorldOfNatrel.swf", "natrel", "1024", "650", "9", "#e3f5fb");
	   so.addParam("scale", "noScale");
	   so.addParam("allowScriptAccess", "always");
	   so.addVariable("lang", "<?=$session_language;?>");
	   <?php
	    if($colname_userData !='-1'){
	        ?>
	            so.addVariable("Sender", "<?=$colname_userData;?>");
        <?php } ?>
	   
	   so.write("flashcontent");
	</script>
	<!-- END: flash ======================================= -->
</div>
</body>
</html>
