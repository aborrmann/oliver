<?php
require_once("phpmailer/class.phpmailer.php");
require_once("phpmailer/language/phpmailer.lang-en.php");
require_once("phpmailer/language/phpmailer.lang-fr.php");

if((isset($_POST['formMailer'])) && ($_POST['formMailer'] == TRUE)) {
	$mail = new PHPMailer();
	$mail->From     = 'adrian@trapeze.com';
	$mail->FromName = "MooQuest Comment Form";
	$mail->Host     = "mail.trapeze.com"; // you need to set this to something else smtp server
	$mail->Mailer   = "mail"; 
	$mail->IsHTML(true);

	// Adjustable Settings
	//$mail->AddAddress("joanne@hlgrp.com");
	//$mail->Subject  = "HL Group Contact Submission";

	// HTML body
	$body  = 'First Name: '.$_POST['firstName'].'<br/>'."\r\n";
	$body  = 'Last Name: '.$_POST['lastName'].'<br/>'."\r\n";
	$body .= 'Email Address: '.$_POST['emailAddress'].'<br/>'."\r\n";
	$body .= 'Message: '.$_POST['formMessage'].'<br/>'."\r\n";

	// Plain text body (for mail clients that cannot read HTML)
	$text_body  = 'First Name: '.$_POST['firstName'].'<br/>'."\r\n";
	$text_body  = 'Last Name: '.$_POST['firstName'].'<br/>'."\r\n";
	$text_body .= 'Email Address: '.$_POST['emailAddress'].'<br/>'."\r\n";                        
	$text_body .= 'Message: '.$_POST['formMessage'].'<br/>'."\r\n";

	$mail->Body    = $body;
	$mail->AltBody = $text_body;   

	if(!$mail->Send()) {
			echo "There has been a mail error sending to " . $email . "<br>";
			return true;
		} else {	
			return false;
		}
}
?>
