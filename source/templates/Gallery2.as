﻿package templates {
    import org.cove.ape.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import com.greensock.TweenLite;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
    public class Gallery2 extends MovieClip
	{  
		private var photos:Group;
		private var walls:Group;
		private var cursor:Group;
		private var particles:Array;
		private var particleClips:Array;
		
		private var isDragging:Boolean = false;
		private var dragClip:MovieClip;
		private var dragParticle:RectangleParticle;
		private var lastDragParticle:RectangleParticle;
		private var mouseCursor:RectangleParticle;
		private var cursorConstraint:SpringConstraint;
		
		private var lastX:Number = 0;
		private var currentX:Number = 0;
		private var lastY:Number = 0;
		private var currentY:Number = 0;
		
		private var accelX:Number = 0;
		private var accelY:Number = 0;
		private var accelDecay:Number = 0.9;
		
		private var stageWidth:Number = 1280;
		private var stageHeight:Number = 720;
		
		private var photoWidth:Number = 150;
		private var photoHeight:Number = 111;
		private var wallDepth:Number = 50;
		private var photoTotal:Number = 50;
		
		private var isExpanded:Boolean = false;
		private var expandedClip:MovieClip;
		private var save:Object = {};
		
		private var margins:Number = 100;
		
		private var localX:Number;
		private var localY:Number;
		
		private var directionAngle:Number;
		
        public function Gallery2()
		{
			this.addEventListener( Event.ADDED_TO_STAGE, init );
		}
		
        public function init( e:Event ):void
		{
			this.removeEventListener( Event.ADDED_TO_STAGE, init );
			
			initAPE();
			initWalls();
			initObjects();
			initCursor();
						
			this.addEventListener( MouseEvent.MOUSE_DOWN, screenPress );
			this.addEventListener( MouseEvent.MOUSE_UP, screenRelease );
			this.addEventListener( MouseEvent.DOUBLE_CLICK, doubleClick );
			
			this.addEventListener(Event.ENTER_FRAME, run);
			
			var timer:Timer = new Timer( 3000, 1 );
			timer.addEventListener( TimerEvent.TIMER, function() { photos.collideInternal = false; } );
			timer.start();
        }
		
		private function initAPE():void
		{			
			APEngine.init( 1/3 );
			APEngine.container = this.container;
			APEngine.container.visible = false;
			
			APEngine.damping = 0.88;
			APEngine.constraintCollisionCycles = 3;
		}
		 
		 
		 private function initCursor():void
		 {
			 cursor = new Group();
			 
			 mouseCursor = new RectangleParticle(230,250,15,15,0);
        	cursor.addParticle( mouseCursor );
			APEngine.addGroup( cursor );
        
	       	mouseCursor.px = mouseX;
			mouseCursor.py = mouseY;			
		 }
		 
		private function initWalls():void
		{
			walls = new Group();
			walls.collideInternal = false;
			
			// top
			var top:RectangleParticle = new RectangleParticle( stageWidth / 2, -(wallDepth / 2 + margins), stageWidth + margins, wallDepth, 0, true );
			
			// bottom
			var bottom:RectangleParticle = new RectangleParticle( stageWidth / 2, stageHeight + wallDepth / 2 + margins, stageWidth + margins, wallDepth, 0, true );
			
			// left
			var left:RectangleParticle = new RectangleParticle( -( wallDepth / 2 + margins ), stageHeight / 2, wallDepth, stageHeight + margins * 2, 0, true );
			
			// right
			var right:RectangleParticle = new RectangleParticle( stageWidth + wallDepth / 2 + margins, stageHeight / 2, wallDepth, stageHeight + margins * 2, 0, true );
			
			
			walls.addParticle( top );
			walls.addParticle( bottom );
			walls.addParticle( left );
			walls.addParticle( right );
			
			APEngine.addGroup(walls);
		}
		
		private function initObjects():void
		{
			photos = new Group();
			photos.collideInternal = true;
			particles = [];
			particleClips = [];
			
			for ( var i = 0; i < photoTotal; i++ )
			{
				var startX:Number = wallDepth + ( photoWidth / 2 ) + Math.random() * ( stageWidth - wallDepth - ( photoWidth / 2 ) );
				var startY:Number = wallDepth + ( photoHeight / 2 ) + Math.random() * ( stageHeight - wallDepth - ( photoHeight / 2 ) );
				var startAngle:Number = Math.random() * 1;
				
				var box:RectangleParticle = new RectangleParticle( startX ,startY, photoWidth, photoHeight, startAngle, false, 1 );
				box.setStyle(0,0,0,0x999999);
				particles.push( box  );
				photos.addParticle(box);
				
				var spriteClip:SpriteClip = new SpriteClip();
				
				spriteClip.id = i;
				spriteClip.width = photoWidth;
				spriteClip.height = photoHeight;
				spriteClip.rotation = startAngle * 180 / Math.PI;
				//spriteClip.alpha = Math.random() * 1;
				
				particleClips.push( spriteClip );
				clips.addChild( spriteClip );
			}
			
			photos.addCollidable( walls );
			
			APEngine.addGroup(photos);
		}
		
		private function doubleClick( e:MouseEvent ):void
		{
			trace(e);
		}
		
		private function run(evt:Event):void
		{
			mouseCursor.px = mouseX;
			mouseCursor.py = mouseY;			 
							
			APEngine.step();
			//APEngine.paint();
			
			if ( isDragging )
			{
				dragParticle.angle = cursorConstraint.angle;
			}
			
			/*lastX = currentX;
			currentX = mouseX;

			lastY = currentY;
			currentY = mouseY;
			
			//directionAngle = Math.atan2( mouseY - dragParticle.py, mouseX - dragParticle.px );
			//dragParticle.angle = -directionAngle * 180 / Math.PI;
				//
			if ( isDragging )
			{
				//directionAngle = Math.atan2( mouseY - dragParticle.py, mouseX - dragParticle.px );
				//dragParticle.angle = Math.abs( directionAngle * 180 / Math.PI );
				
				// update acceleration
				accelX = mouseX - lastX;
				accelY = mouseY - lastY;
				
				dragParticle.angle = cursorConstraint.angle;
			}
			
			if ( dragParticle != null )
			{
				// apply the accel x, y 
				//dragParticle.addForce( new VectorForce( false, accelX * accelDecay, accelY * accelDecay ) );
			}*/
			
			if ( !isExpanded ){
				renderClips();
			}
			
		}
		
		private function screenPress( e:MouseEvent ):void
		{
			var objects = clips.getObjectsUnderPoint(new Point(mouseX, mouseY));
			var clip;
			
			if ( objects[1] != null )
			{
				if ( !isExpanded )
				{
					// reset
					accelX = 0;
					accelY = 0;
					
					localX = e.localX;
					localY = e.localY;
				
					isDragging = true;
					
					dragParticle = particles[ objects[1].parent.id ] as RectangleParticle;
					
					// swap depths
					clip = objects[1].parent;
					clips.setChildIndex( clip, clips.numChildren - 1 );
						
					// reposition constraint to simulate drag
					while ( cursor.constraints.length > 0 ) cursor.removeConstraint( cursor.constraints[0] );
					cursorConstraint = new SpringConstraint( mouseCursor, dragParticle, 0.03, false, 1, 1, false );
					cursor.addConstraint( cursorConstraint );
				}
				
				if ( dragParticle == lastDragParticle )
				{
					
					
					if ( !isExpanded )
					{
						isExpanded = true;
						
						clip = objects[1].parent;
						
						// save the state
						save = {}
						save.width = dragParticle.width;
						save.height = dragParticle.height;
						save.rotation = dragParticle.angle;
						save.x = dragParticle.px;
						save.y = dragParticle.py
						
						// swap depths
						clips.setChildIndex( clip, clips.numChildren - 1 );

						// set ref
						expandedClip = clip;
						
						TweenLite.to( clip, 0.2, { width: stageWidth, height: stageHeight, x: stageWidth / 2, y: stageHeight / 2, rotation: 0 } );
						
					} else {
						
						clip = objects[1].parent;
						trace(clip);
						TweenLite.to( clip, 0.2, { width: save.width, height: save.height, x: save.x, y: save.y, rotation: save.rotation, onComplete: expandComplete } );
					}
				}
			}
		}
		
		private function expandComplete():void
		{
			isExpanded = false;
		}
		
		private function screenRelease( e:MouseEvent ):void
		{
			// remove contraint
			while ( cursor.constraints.length > 0 ) cursor.removeConstraint( cursor.constraints[0] );
			
			isDragging = false;
			lastDragParticle = dragParticle;
			dragParticle = null;
		}
		
		
		private function renderClips():void
		{
			// draw physical movieclips
			for ( var i in particleClips )
			{				
				particleClips[i].x = particles[i].px;
				particleClips[i].y = particles[i].py;
				particleClips[i].rotation = particles[i].angle;
			}
		}
    }
}