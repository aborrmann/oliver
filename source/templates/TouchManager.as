﻿/**
* ...
* @author Adrian Stiegler
*/
package templates
{
	import com.trapeze.framework.managers.FrameworkManager;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.events.EventDispatcher;
	import flash.events.TouchEvent;
	import flash.display.Stage;
	
	/* This class is responsible for working with the TOUCH POINT listeners and reading the gesture*/
	public class TouchManager extends EventDispatcher
	{
		// Left / right swipe
		private var swipeStartX:Number;
		private var swipeEndX:Number;
		
		// Up / down swipe
		private var swipeStartY:Number;
		private var swipeEndY:Number;
		
		private var swipeDiff:Number;
		private var swipeMinValue:Number = 100;
		
		// Zoom
		private var zoomStartDistance:Number;
		private var zoomEndDistance:Number;
		
		
		private var pointsTotal:Number;
		private var points:Array;
		private var pointMode:String;
	
		public function TouchManager( stage:Stage ):void
		{
			points = [];
			
			// let's set our mode
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			// Gesture Events
			if( Multitouch.supportsGestureEvents )
			{			
				FrameworkManager.stage.addEventListener( TouchEvent.TOUCH_BEGIN, onTouch );
				FrameworkManager.stage.addEventListener( TouchEvent.TOUCH_MOVE, onTouch );
				FrameworkManager.stage.addEventListener( TouchEvent.TOUCH_END, onTouch );
				FrameworkManager.stage.addEventListener( TouchEvent.TOUCH_TAP, onTouch );
			}
		}
		
		private function onTouch( e:TouchEvent ):void
		{			
			//trace( "ID: " + e.touchPointID + ", isPrimary? " + e.isPrimaryTouchPoint + ", x: " + e.stageX + ", y: " + e.stageY + ", pressure: " + e.pressure ); 
			if ( e.type == TouchEvent.TOUCH_BEGIN )
			{
				// Register a new point into our points
				var point:Object = {};
				point.id = e.touchPointID;
				points.push( point );
				
				pointsTotal = points.length;
				pointMode = ( pointsTotal > 1 ) ? 'twoPoint' : 'onePoint';
				
				trace(pointMode);
				
				if ( pointMode == 'onePoint' )
				{
					// Register for swipes
					swipeStartX = e.stageX;
					swipeStartY = e.stageY;
				}
			}
			
			if ( e.type == TouchEvent.TOUCH_END )
			{
				
				
				if ( pointMode == 'onePoint' )
				{
					// Check for Swipes
					swipeEndX = e.stageX;
					swipeEndY = e.stageY;
					
					var xDiff:Number = Math.abs( swipeStartX - swipeEndX );
					var yDiff:Number = Math.abs( swipeStartY - swipeEndY );
					
					if ( xDiff >= swipeMinValue || yDiff >= swipeMinValue )
					{
						// determine if we're doing a horizontal / vertical swipe by measuring the greater number
						if ( xDiff > yDiff )
						{
							// left and right
							swipeDiff = swipeStartX - swipeEndX;
							
							if ( swipeDiff > 0 ) 
							{
								// left swipe
								dispatchEvent( new TouchScreenEvent( TouchScreenEvent.SWIPE_LEFT ) );
								
							} else {
								
								// right swipe
								dispatchEvent( new TouchScreenEvent( TouchScreenEvent.SWIPE_RIGHT ) );
							}
							
						} else {
							
							// up and down
							swipeDiff = swipeStartY - swipeEndY;
							
							if ( swipeDiff > 0 ) 
							{
								// up swipe
								dispatchEvent( new TouchScreenEvent( TouchScreenEvent.SWIPE_DOWN ) );
								
							} else {
								
								// down swipe
								dispatchEvent( new TouchScreenEvent( TouchScreenEvent.SWIPE_UP ) );
							}
						}
					}
				}
				
				// remove point from memory
				for ( var i = 0; i < points.length; i++ ) if ( points[i].id == e.touchPointID ) points.splice( i, 1 );
				
				pointsTotal = points.length;
				pointMode = ( pointsTotal > 1 ) ? 'twoPoint' : 'onePoint';
				
				trace(pointMode);
			}
			
			if ( e.type == TouchEvent.TOUCH_MOVE )
			{
				
			}
		}
	}
}