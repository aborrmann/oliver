﻿package templates.utils
{
	import org.papervision3d.objects.DisplayObject3D;
 
	public class PV3DUtils
	{
		public static function getWidth(displayObject3D:DisplayObject3D):Number{
			return (displayObject3D.geometry.aabb.maxX -  displayObject3D.geometry.aabb.minX)*displayObject3D.scaleX;
		}
		public static function getHeight(displayObject3D:DisplayObject3D):Number{
			return (displayObject3D.geometry.aabb.maxY -  displayObject3D.geometry.aabb.minY)*displayObject3D.scaleY;
		}
	}
}