﻿package templates
{
    import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.TemplateManager;
	import com.trapeze.framework.managers.CopyManager;
	import com.trapeze.framework.templates.BaseNavigation;
	import flash.display.MovieClip;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
    public class Navigation extends BaseNavigation
	{
		private var stageWidth:Number = 1360;
		private var stageHeight:Number = 768;
	
		private var pressY:Number;
		private var releaseY:Number;
		
		private var closeY:Number = 700;
		private var openY:Number = 580;
		private var currentY:Number = closeY;
		private var isOpen:Boolean = false;
		
        public function Navigation()
		{
			hide();
		}
		
        override public function onFrameworkComplete( e:Event ):void
		{
			// set up handlers for navigation items
			navHotspot.alpha = 0;
			navHotspot.addEventListener( MouseEvent.MOUSE_DOWN, onHotspotPress );
			navHotspot.addEventListener( MouseEvent.MOUSE_UP, onHotspotRelease );
			navHotspot.addEventListener( MouseEvent.CLICK, onHotspotClick );
			this.addEventListener( Event.ENTER_FRAME, update );
			
			/*
			This is how u avoid any out of bounds MOUSE_UP issues
			navHotspot.addEventListener(MouseEvent.MOUSE_DOWN,onHotspotPress);
			stage.addEventListener(MouseEvent.MOUSE_UP,onHotspotRelease);
			*/
			
			navTab.navButton1.label.label_txt.htmlText = CopyManager.getValue('frontAsset_title');
			navTab.navButton2.label.label_txt.htmlText = CopyManager.getValue('rightAsset_title');
			navTab.navButton3.label.label_txt.htmlText = CopyManager.getValue('backAsset_title');
			navTab.navButton4.label.label_txt.htmlText = CopyManager.getValue('leftAsset_title');
			
			navTab.navButton1.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
			navTab.navButton2.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
			navTab.navButton3.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
			navTab.navButton4.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
			
			navTab.navButton1.addEventListener( MouseEvent.MOUSE_UP, onUp );
			navTab.navButton2.addEventListener( MouseEvent.MOUSE_UP, onUp );
			navTab.navButton3.addEventListener( MouseEvent.MOUSE_UP, onUp );
			navTab.navButton4.addEventListener( MouseEvent.MOUSE_UP, onUp );
			
			navTab.y = closeY;
			navHotspot.y = closeY;
			
			navArea.buttonMode = false;
			navArea.addEventListener( MouseEvent.MOUSE_DOWN, onAreaDown );
			navArea.visible = false
			navArea.alpha = 0;
			
			navTab.backButton.addEventListener( MouseEvent.MOUSE_DOWN, onUp );
			navTab.logoutButton.addEventListener( MouseEvent.MOUSE_DOWN, onUp );
        }
		
		private function update( e:Event ):void
		{
			// update
			navTab.y = navHotspot.y;
		}
		
		private function onAreaDown( e:MouseEvent ):void
		{
			close();
		}
		
		private function onHotspotClick( e:MouseEvent ):void
		{
			if ( isOpen ) 
			{
				open();
				
			} else {
				
				close();
			}
		}
		private function onHotspotPress( e:MouseEvent ):void
		{
			pressY = navHotspot.y;
			
			var rect = new Rectangle( 0, openY, 0, closeY - openY ); 
			navHotspot.startDrag( false, rect );
			
			navHotspot.addEventListener( Event.ENTER_FRAME, onHotspotUpdate );
		}
		
		private function onHotspotRelease( e:MouseEvent ):void
		{
			releaseY = navHotspot.y;
			
			navHotspot.stopDrag();
			
			// check to see if the drag spanned enough pixels to snap to top, otherwise snap back down
			var range:Number = closeY - openY;
			var amountDragged:Number = Math.abs( pressY - releaseY );
			
			if ( amountDragged <= range / 2 )
			{
				// not enough pulled
				// return to default
				close();
				
			} else {
				
				// pulled more than half, snap to open
				open();
			}
		}
		
		private function onHotspotUpdate( e:Event ):void
		{
			if ( e.currentTarget.mouseY < 0 || e.currentTarget.mouseY > navHotspot.height )
			{
				// out of bounds
				onHotspotRelease( null );
				navHotspot.removeEventListener( Event.ENTER_FRAME, onHotspotUpdate );
			}
		}
		
		public function open():void
		{
			isOpen = true;
			navArea.visible = true;
			
			reset();
			
			// disable any mouse children on TemplateManager.container
			TemplateManager.container.mouseChildren = false;
			TemplateManager.container.mouseEnabled = false;
			
			//navHotspot.y = openY;
			TweenLite.to( navHotspot, 10, { y: openY, useFrames: true, ease: Back.easeOut } );
		}
		
		public function close():void
		{
			isOpen = false;
			navArea.visible = false;
			
			reset();
			
			// enable any mouse children on TemplateManager.container
			TemplateManager.container.mouseChildren = true;
			TemplateManager.container.mouseEnabled = true;
						
			//navHotspot.y = closeY;
			TweenLite.to( navHotspot, 10, { y: closeY, useFrames: true, ease: Back.easeOut } );
		}
		
		private function reset():void
		{
			navTab.navButton1.gotoAndStop('out');
			navTab.navButton2.gotoAndStop('out');
			navTab.navButton3.gotoAndStop('out');
			navTab.navButton4.gotoAndStop('out');
		}
		
		public function show():void
		{
			this.visible = true;
			navTab.y = closeY;
			navHotspot.y = closeY;
		}
		
		public function hide():void
		{
			this.visible = false;
			navTab.y = closeY;
			navHotspot.y = closeY;
		}
		
		private function onDown( e:MouseEvent ):void
		{
			var navButton:MovieClip = e.currentTarget as MovieClip;
			navButton.gotoAndStop('over');
		}
		
		private function onUp( e:MouseEvent ):void
		{
			var navButton:MovieClip = e.currentTarget as MovieClip;
			navButton.gotoAndStop('out');
			
			// load the templates
			if ( navButton.name == 'backButton' ) TemplateManager.load( PrefsManager.getValue("Lobby") );
			if ( navButton.name == 'logoutButton' ) TemplateManager.load( PrefsManager.getValue("Awake") );
			if ( navButton.name == 'navButton1' ) TemplateManager.load( PrefsManager.getValue( CopyManager.getValue("frontAsset") ) );
			if ( navButton.name == 'navButton2' ) TemplateManager.load( PrefsManager.getValue( CopyManager.getValue("rightAsset") ) );
			if ( navButton.name == 'navButton3' ) TemplateManager.load( PrefsManager.getValue( CopyManager.getValue("backAsset") ) );
			if ( navButton.name == 'navButton4' ) TemplateManager.load( PrefsManager.getValue( CopyManager.getValue("leftAsset") ) );
			
			// auto close
			close();
		}
    }
}