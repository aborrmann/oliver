﻿/**
* ...
* @author Adrian Stiegler
*/
package templates
{
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.IOErrorEvent;
	
	public class ToutClipMaterial extends MovieClip
	{
		public var id:Number;
		private var scope:Object;
		private var content:Object;
		
		public function ToutClipMaterial():void {}
		
		public function init( id:Number, scope:Object, content:Object ):void
		{
			this.id = id;
			this.scope = scope;
			this.content = content;
			
			// mouse interaction
			this.mouseChildren = true;
			
			this.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
			this.addEventListener( MouseEvent.MOUSE_UP, onUp );
			
			// setup content
			setupContent();
		}
		
		private function setupContent():void
		{
			// bind text
			label.client_txt.htmlText = content.name;
			label.project_txt.htmlText = content.title;
			//description.label_txt.htmlText = content.description;
			
			// load image
			if ( content.image != null || content.image != '' ) 
			{
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener( Event.COMPLETE, loadComplete );
				loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, loadError );
				loader.load( new URLRequest( content.image ) );
			}
		}
		
		private function loadComplete( e:Event ):void
		{
			// resize
			image.container.addChild( e.target.content );
			
			// this is a hack because pv3d seems to not understand accurate w/h on this loaded asset into the container
			//container.image.scaleX = container.image.scaleY = 2;
		}
		
		private function loadError( e:IOErrorEvent ):void
		{
			trace("Error loading the image for: Tout" + id );
		}
		
		public function open():void
		{
			// go to open state
			this.gotoAndPlay('open');
		}
		
		public function close():void
		{
			// goto default on state
			this.gotoAndStop('on');
		}
		
		private function onDown( e:MouseEvent ):void
		{
			this.gotoAndStop('on');
		}
		
		private function onUp( e:MouseEvent ):void
		{
			this.gotoAndStop('off');
			
			// send to scoped class
			scope.activateItem( id, content );
		}
	}
}