﻿package templates
{
	import org.cove.ape.RectangleParticle;
	
    public class Photo extends RectangleParticle
	{
		public var id:Number;
		
		/*
				x:Number, 
				y:Number, 
				width:Number, 
				height:Number, 
				rotation:Number = 0, 
				fixed:Boolean = false,
				mass:Number = 1, 
				elasticity:Number = 0.3,
				friction:Number = 0)
				
				*/
				
					
        public function Photo( x:Number, y:Number, width:Number, height:Number, rotation:Number, fixed:Boolean, mass:Number )
		{
			super( x,y,width,height,rotation,fixed,mass );
		}
    }
}