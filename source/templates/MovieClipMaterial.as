﻿/**
* ...
* @author Adrian Stiegler
*/
package templates
{
	import com.trapeze.framework.managers.CopyManager;
	import com.trapeze.framework.managers.PrefsManager;
	
	import flash.display.MovieClip;
	import flash.net.URLLoader;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	
	public class MovieClipMaterial extends MovieClip
	{
		public var _id:String;
		private var _title:String;
		private var _desc:String;
		private var _image:String;
		
		private var scope:Object;
		
		public function MovieClipMaterial():void {}
		
		public function init( id:String, scope:Object ):void
		{
			this.scope = scope;
			
			_id = id;
			_title = CopyManager.getValue( id + '_title' );
			_desc = CopyManager.getValue( id + '_desc' );			
			
			title.label_txt.htmlText = _title;
			desc.label_txt.htmlText = _desc;
			
			// hide bg
			container.visible = false;
			
			// mouse interaction
			this.mouseChildren = false;
			this.addEventListener( MouseEvent.CLICK, onClick );
			
			// load image
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener( Event.COMPLETE, loadComplete );
			loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, loadError );
			loader.load( new URLRequest( PrefsManager.getValue( id + '_image' ) ) );
		}
		
		private function onClick( e:MouseEvent ):void
		{
			// send to Lobby for processing
			scope.loadTemplate( _id );
		}
		
		private function loadError( e:Event ):void
		{
			trace("cannot load a file in: " + _id + " MovieClipMaterial class");
		}
		private function loadComplete( e:Event ):void
		{
			// resize
			container.image.addChild( e.target.content );
			
			// this is a hack because pv3d seems to not understand accurate w/h on this loaded asset into the container
			//container.image.scaleX = container.image.scaleY = 2;
		}
		
		// This is called from the 'cubeAsset' timeline which is attached to this class, when the 'open' animation has been completed
		public function onExpand():void
		{
			// call to our scope parent
			//scope.startZoomIn();
		}
	}
}