﻿package templates
{
    import flash.events.Event;
	import templates.FloorPlan;
	import com.trapeze.framework.templates.BaseTemplate;
	import flash.display.MovieClip;
	
    public class FloorView extends BaseTemplate
	{
		private var floorPlan:FloorPlan;
				
        override public function onFrameworkComplete(e:Event):void
		{
        	// floor plan
			floorPlan = new FloorPlan( this );
			floorPlan.addEventListener( Event.ADDED_TO_STAGE, init );
			container.addChild( floorPlan );
			
			function init( event:Event ):void
			{
				floorPlan.init();
			}
        }
    }
}