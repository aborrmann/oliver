﻿package templates {
    import com.trapeze.framework.managers.CopyManager;
    import com.trapeze.framework.templates.BaseTemplate;
	import com.trapeze.framework.managers.TemplateManager;
	import com.trapeze.framework.managers.PrefsManager;
	import com.trapeze.framework.managers.NavigationManager;
    import com.trapeze.text.TextLine;
    import flash.events.Event;
    import flash.text.TextField;
    import flash.events.MouseEvent;

	/**
     * ...
     * @author Adrian Borrmann
     */
    public class Awake extends BaseTemplate {
        
        public var textField:TextField;
        
        public function Awake() {
            
        }
        
        
        override public function onFrameworkComplete(e:Event):void {
            
            //textField = TextLine.convertTextField(textField);
            //textField.text = CopyManager.getValue("Asleep - Default Text");
			
			// Tell the navigation to hide ( hacked in )
			if ( NavigationManager.navigationClip != null ) 
			{
				var navClip:MovieClip = NavigationManager.navigationClip as MovieClip;
				navClip.hide();
			}
			
			loginButton.buttonMode = true;
			loginButton.addEventListener( MouseEvent.MOUSE_DOWN, login );
        }
		
		private function login( e:MouseEvent ):void
		{
			// change templates
			 TemplateManager.load( PrefsManager.getValue('Lobby') );
			
		}
    }
}