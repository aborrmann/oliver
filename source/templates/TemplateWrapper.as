package templates {
	import com.trapeze.framework.managers.FrameworkManager;
	import com.trapeze.framework.managers.LoadManager;
	import com.trapeze.framework.managers.PrefsManager;
	import com.trapeze.framework.templates.BaseTemplate;
	import com.trapeze.utils.Log;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author ...
	 */
	public class TemplateWrapper extends BaseTemplate {
		
		private var content:Loader;
		
		public function TemplateWrapper() {
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		
		private function onContentLoad(e:Event) {
			
			content.x = stage.stageWidth / 2 - content.width / 2;
			content.y = stage.stageHeight / 2 - content.height / 2;
			addChild(content);
		}
		
		
		private function onAddedToStage(e:Event):void {
			
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			Log.info("Loading " + contentURL);
			content = new Loader();
			content.contentLoaderInfo.addEventListener(Event.COMPLETE, onContentLoad);
			content.load(new URLRequest(FrameworkManager.preparePath(contentURL)));
		}
	}
}