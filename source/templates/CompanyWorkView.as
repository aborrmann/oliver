﻿package templates
{
	import com.trapeze.framework.templates.BaseTemplate;
	import com.trapeze.framework.managers.PrefsManager;
	import com.trapeze.framework.managers.FrameworkManager;
	import com.trapeze.framework.managers.LoadManager;
	import com.adobe.serialization.json.JSON;
	import templates.CompanyWork;
	
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
    final public class CompanyWorkView extends BaseTemplate
	{
		private var content:Object;
		private var work:CompanyWork;
		
        public function CompanyWorkView() {}
		
        override public function onFrameworkComplete(e:Event):void
		{
			// Load the json file through LoadManager
			content = LoadManager.retrieve( 'json' );
			
			if ( content != null  )
			{
				// already exists, call feedLoaded
				feedLoaded( content );
				
			} else {
				
				// load the file
				LoadManager.load( 'json', FrameworkManager.preparePath( PrefsManager.getValue('companyFeedFile') ), feedLoaded );
			}
        }
		
		//private function feedLoaded( e:Event ):void
		private function feedLoaded( data:Object ):void
		{
			// decode using JSON library
			content = JSON.decode( data.toString() );
			
			// Create a new company work gallery
        	work = new CompanyWork( this, content );
			work.addEventListener( Event.ADDED_TO_STAGE, init );
			container.addChild( work );
			
			function init( event:Event ):void
			{
				work.init( null );
			}
		}
		
    }
}