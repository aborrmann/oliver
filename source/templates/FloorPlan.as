﻿package templates
{
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;

	import flash.text.TextField;

	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.events.TouchEvent;
	import flash.events.GestureEvent;
	import flash.events.GesturePhase;
	import flash.events.TransformGestureEvent;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.display.Sprite;
		
    public class FloorPlan extends MovieClip
	{
		private var scope:Object;
		private var map:MovieClip;
		private var isDragging:Boolean 				= false;
		private var stageWidth:Number				= 1360;
		private var stageHeight:Number 				= 768;
				
        public function FloorPlan( scope:Object )
		{
			this.scope = scope;
		}
		
		public function init():void
		{
			map = new FloorPlanClip();
			addChild( map );
			
			map.x = stageWidth / 2;
			map.y = stageHeight / 2 - 50;
			
			startHandlers();
		}
		
		private function startHandlers():void
		{
			// let's set our mode
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			
			map.addEventListener( TransformGestureEvent.GESTURE_ROTATE, gestureRotate );
			map.addEventListener( TransformGestureEvent.GESTURE_ZOOM, gestureZoom );
				
			map.addEventListener( MouseEvent.MOUSE_DOWN, mouseDown );
			map.addEventListener( MouseEvent.MOUSE_UP, mouseUp );
		}
				
		
		private function gestureRotate( e:TransformGestureEvent ):void
		{
			map.rotation += e.rotation;
		}
		
		private function gestureZoom( e:TransformGestureEvent ):void
		{
			map.scaleX *= ( e.scaleX + e.scaleY ) / 2;
			map.scaleY *= ( e.scaleX + e.scaleY ) / 2;
		}
		
		private function mouseDown( e:MouseEvent ):void
		{
			isDragging = true;
			map.startDrag();
		}
		
		private function mouseUp( e:MouseEvent ):void
		{
			isDragging = false;
			map.stopDrag();
		}
    }
}