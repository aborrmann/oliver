﻿package templates {
	
	import org.papervision3d.objects.primitives.Plane;
	import org.papervision3d.objects.DisplayObject3D;
	import org.papervision3d.view.BasicView;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	
	import fl.transitions.easing.*;
	import flash.utils.Timer;
	import flash.geom.Point;

	import org.papervision3d.cameras.Camera3D;
	import org.papervision3d.cameras.CameraType;
	import org.papervision3d.events.InteractiveScene3DEvent;
	import org.papervision3d.materials.ColorMaterial;
	import templates.utils.PV3DUtils;
	import com.greensock.TweenLite;
	import org.papervision3d.materials.BitmapAssetMaterial;
	import flash.filters.BlurFilter;
	import flash.display.DisplayObject;
	import flash.events.KeyboardEvent;
	import org.papervision3d.core.utils.Mouse3D;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import org.papervision3d.materials.BitmapMaterial;
	
    public class CompanyGallery extends BasicView
	{  
		private var stageWidth:Number = 1360;
		private var stageHeight:Number = 768;
		
		private var parentDisplayObject:DisplayObject3D;
		private var displayObject:DisplayObject3D;
		
		private var gridX:Number = 10;
		private var gridY:Number = 5;
		private var gridSpacing:Number = 0;
		private var gridSquareWidth:Number = 75;
		private var gridSquareHeight:Number = 60;
		
		private var displayObjectWidth:Number;
		private var displayObjectHeight:Number;
		private var isActivated = false;
		
		private var cameraFOV:Number = 45;
		private var defaultCameraZ:Number;
		private var defaultCameraX:Number;
		private var defaultCameraY:Number;
		private var defaultZoom:Number = 20;
		private var defaultCameraScale:Number = 3;
		
		private var isOribiting:Boolean;
		private var cameraPitch:Number = 90;
		private var cameraYaw:Number = 270;
				
		private var previousMouseX:Number;
		private var previousMouseY:Number;
		
		private var targetDisplayObject:DisplayObject3D;
		private var isTweening:Boolean = false;
		private var targetCameraX:Number;
		private var targetCameraY:Number;
		private var targetCameraZ:Number = -900;
		private var targetCameraScale:Number = defaultCameraScale;
		private var planes:Array;
		private var planeRefs:Array;
		private var rollOverDistance:Number = 200;
		private var rollOverDepthMax:Number = 200;
		private var scrollSpeed:Number = 40;
		private var cameraPanMax:Number = 10;
		private var timerInc:Number;
		private var flipInc:Number = 1;		// default to back side
	
		private var mouseUpdateRotationX:Number;
		private var mouseUpdateRotationY:Number;
		private var mouse3D:Mouse3D;
		
		private var bgBitmap:Bitmap;
		private var bgBitmapData:BitmapData;
		private var bitmapSampleX:Number;
		private var bitmapSampleY:Number;
		
        public function CompanyGallery()
		{
			super( stageWidth, stageHeight, false );
			//this.addEventListener( Event.ADDED_TO_STAGE, init );
		}
		
		private function onKey( e:KeyboardEvent ):void
		{
			if ( e.keyCode == 81 )
			{
				// flip transition
				if ( !isTweening && !isActivated )transitionToFlip();
			}
		}
		
        public function init( e:Event ):void
		{
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKey );
			
			// render
			startRendering();
			
			// setup pv3d
			viewport.interactive = true;			
			viewport.autoScaleToStage = false;
			viewport.viewportWidth = stageWidth;
			viewport.viewportHeight = stageHeight;
			
			camera.target = null;
			camera.fov = cameraFOV;
			camera.zoom = defaultZoom;
			
			Mouse3D.enabled = true;
			mouse3D = viewport.interactiveSceneManager.mouse3D;

			// setup parent displayObject
			parentDisplayObject = new DisplayObject3D();
			displayObject = new DisplayObject3D();
			parentDisplayObject.addChild( displayObject );
			scene.addChild( parentDisplayObject )
			
			bitmapSampleX = 0;
			bitmapSampleY = 0;
			bgBitmapData = new trapezeLogo();
			//bgBitmap = new Bitmap( bgBitmapData );
			/*var newBitmapData = new BitmapData( gridSquareWidth, gridSquareHeight, false, 0x000000 );
			newBitmapData.copyPixels( bgBitmapData, new Rectangle( bitmapSampleX, bitmapSampleY, gridSquareWidth, gridSquareHeight ), new Point( 0, 0 ) );
			bgBitmap = new Bitmap( newBitmapData );
			addChild( bgBitmap );
			trace(newBitmapData);*/
			
			// add planes
			// create grid
			planes = [];
			planeRefs = [];
			var inc:Number = 0;
			
			for (var i = 0; i < gridY; i++)
			{
			  for (var j = 0; j < gridX; j++)
			  {
				//var planeMaterial:ColorMaterial = new ColorMaterial( 0x00ADEF, 1, true );
				/*var planeMaterial:BitmapAssetMaterial = new BitmapAssetMaterial( 'bitmapTest', true );
				var plane:Plane = new Plane( planeMaterial, gridSquareWidth, gridSquareHeight, 1, 1 );
				
				planeMaterial.interactive = true;
				planeMaterial.oneSide = false;
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_PRESS, planeClick );
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_OVER, planeOver );
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_OUT, planeOut );
				*/
				
				var plane:DisplayObject3D = new DisplayObject3D();
				var libraryString:String = ( inc + 1 > 43 ) ? 'defaultImage' : 'image' + ( inc + 1 );
				var faceMat:BitmapAssetMaterial = new BitmapAssetMaterial( libraryString, true ); 
							
				var newBitmapData = new BitmapData( gridSquareWidth, gridSquareHeight, false, 0x000000 );
				newBitmapData.copyPixels( bgBitmapData, new Rectangle( gridSquareWidth * ( gridX - ( j + 1 ) ), gridSquareHeight * ( gridY - ( i + 1 ) ), gridSquareWidth, gridSquareHeight ), new Point( 0, 0 ) );
				var backMat:BitmapMaterial = new BitmapMaterial( newBitmapData, true );
				backMat.oneSide = false;
				
				var face:Plane = new Plane( faceMat, gridSquareWidth, gridSquareHeight, 1, 1 );				
				var back:Plane = new Plane( backMat, gridSquareWidth, gridSquareHeight, 1, 1 );
				
				plane.addChild(back); 
				plane.addChild(face);         
				
				back.z = 1;
				
				faceMat.interactive = true;
				face.addEventListener( InteractiveScene3DEvent.OBJECT_PRESS, planeClick );
				face.addEventListener( InteractiveScene3DEvent.OBJECT_OVER, planeOver );
				face.addEventListener( InteractiveScene3DEvent.OBJECT_OUT, planeOut );
				
				
				plane.x = ( gridSquareWidth + gridSpacing ) * j;
				plane.y = ( gridSquareHeight + gridSpacing ) * i;
				
				var ref = {};
				ref.displayObject3D = plane;
				ref.id = inc;
				planeRefs.push( ref );
								
				inc++;
				displayObject.addChild( plane );
				planes.push( plane );
				plane.rotationY = 180;
			  }
			}
			
			// update some vars
			displayObjectWidth = ( gridSquareWidth + gridSpacing ) * gridX - gridSpacing;
			displayObjectHeight = ( gridSquareHeight + gridSpacing ) * gridY - gridSpacing;
			
			// position camera proportionally to the 
			// size of the displayObject
			
			defaultCameraX = -15 * defaultCameraScale;
			defaultCameraY = -90 * defaultCameraScale;
			defaultCameraZ = -Math.sqrt(displayObjectWidth*displayObjectWidth+displayObjectHeight*displayObjectHeight) * 1.12;
			
			// set scale
			parentDisplayObject.scaleX = parentDisplayObject.scaleY = defaultCameraScale;
			
			// position displayObject
			displayObject.x = -displayObjectWidth / 2;
			displayObject.y = -displayObjectHeight / 2;
			
			// transition in
			transitionIn();
		}
		
		private function transitionIn():void
		{
			/*
			Camera pan down, once hit the bottom, we do a flip and resolve to default position
			*/
			
			// start position
			camera.x = defaultCameraX;
			camera.y = defaultCameraY;
			camera.z = -3200;
			
			// tween to end position
			TweenLite.to( camera, 40, { z: defaultCameraZ, useFrames: true, onComplete: transitionInComplete } );
		}
		
		private function transitionInComplete():void
		{
			transitionToFlip();
			this.addEventListener( Event.ENTER_FRAME, update );
		}
		
		private function planeOver( e:InteractiveScene3DEvent ):void
		{
			//if ( !isActivated && !isTweening ) TweenLite.to( e.displayObject3D, 15, { z: -100, ease: Back.easeOut, useFrames: true } );
			
			
			// TRACES IDS
			// FOR DEBUG ONLY
			/*for ( var i in planeRefs )
			{
				if ( planeRefs[i].displayObject3D == DisplayObject3D( e.displayObject3D.parent ) )
				{
					trace( planeRefs[i].id );
				}
			}*/
			
			if ( !isActivated && !isTweening )
			{
				// Bubble around central mouse pointer effect
				for ( var i in planes )
				{
					var displayObject = planes[i];
										
					if ( displayObject != DisplayObject3D( e.displayObject3D.parent ) )
					{
						var mouseObject:Object = {};
						mouseObject.x = DisplayObject3D( e.displayObject3D.parent ).x;
						mouseObject.y = DisplayObject3D( e.displayObject3D.parent ).y;
						var distance:Number = distance( mouseObject, displayObject );
						var averageZ:Number = Math.floor( ( distance / rollOverDistance ) * rollOverDepthMax );
						
						if ( distance < rollOverDistance )
						{
							TweenLite.to( displayObject, 10, { z: -( rollOverDepthMax - averageZ ), useFrames: true } );
						} else {
							TweenLite.to( displayObject, 10, { z: 0, ease: Regular.easeInOut, useFrames: true } );
						}
					} else {
						
						 TweenLite.to( DisplayObject3D( e.displayObject3D.parent ), 10, { z: -rollOverDepthMax, useFrames: true } );
					}
				}
			}
		}
		
		private function planeOut( e:InteractiveScene3DEvent ):void
		{
			if ( !isActivated && !isTweening )
			{
				for ( var i in planes )
				{
					var displayObject = planes[i];
					TweenLite.to( displayObject, 25, { z: 0, ease: Regular.easeInOut, useFrames: true } );
				}
			}
		}
				
		private function planeClick( e:InteractiveScene3DEvent ):void
		{		
			if ( !isTweening )
			{
				if ( isActivated )
				{
					isActivated = false;
					transitionToCenter();
					
				} else {
		
					isActivated = true;
					targetDisplayObject =  DisplayObject3D( e.displayObject3D.parent );
					targetCameraX = ( DisplayObject3D( e.displayObject3D.parent ).x - displayObjectWidth / 2 ) * targetCameraScale;
					targetCameraY = ( DisplayObject3D( e.displayObject3D.parent ).y - displayObjectHeight / 2 ) * targetCameraScale - 20;
					
					transitionToTarget();
				}
			}
		}
		
		private function transitionToFlip():void
		{
			timerInc = 0;
						
			var timer:Timer = new Timer( 5, planes.length );
			timer.addEventListener( TimerEvent.TIMER, flipUpdate );
			timer.start();
			isTweening = true;
		}
		
		private function flipUpdate( e:TimerEvent ):void
		{
			var displayObject = planes[timerInc];
			var newRotationY:Number = ( flipInc == 0 ) ? 180 : 0;
			timerInc++;
			if ( timerInc == planes.length )
			{
				TweenLite.to( displayObject, 15, { rotationY: newRotationY,  z: 0, ease: Back.easeOut, onComplete: flipComplete, useFrames: true } );
			} else {
				TweenLite.to( displayObject, 15, { rotationY: newRotationY,  z: 0, ease: Back.easeOut, useFrames: true } );
			}
		}

		private function flipComplete():void
		{
			isTweening = false;
			flipInc = ( flipInc + 1 > 1 ) ? 0 : flipInc + 1;
		}
		
		/* Transition to last position before clicking on a item */
		private function transitionToCenter():void
		{
			isTweening = true;
			
			// turn off smoothing
			//targetDisplayObject.material.smooth = false;
			
			//TweenLite.to( camera, 50, { x: 0, y: 0, ease: Regular.easeInOut, useFrames: true } );
			TweenLite.to( parentDisplayObject, 50, { scaleX: defaultCameraScale, scaleY: defaultCameraScale, ease: Regular.easeInOut, useFrames: true, onComplete: transitionToCenterComplete } );
			TweenLite.to( targetDisplayObject, 50, { z: 0, useFrames: true } );
			TweenLite.to( camera, 50, { z: defaultCameraZ, x: defaultCameraX, y: defaultCameraY, useFrames: true } );
						
			for ( var i in planes )
			{
				var displayObject = planes[i];
				TweenLite.to( displayObject, 10, { z: 0, useFrames: true } );
			}
		}
		
		private function transitionToCenterComplete():void
		{
			isTweening = false;
		}
		
		/* Transitions camera to target plane */
		private function transitionToTarget():void
		{
			isTweening = false;
			//TweenLite.to( camera, 20, { x: targetCameraX, y: targetCameraY, rotationY: 0, rotationX: 0, useFrames: true } );
			
			TweenLite.to( camera, 40, { x: targetCameraX, y: targetCameraY, z: targetCameraZ - 150, ease: Regular.easeInOut, useFrames: true } );
			//TweenLite.to( parentDisplayObject, 20, { scaleX: targetCameraScale, scaleY: targetCameraScale, useFrames: true, onComplete: transitionToTargetComplete } );
			TweenLite.to( targetDisplayObject, 40, { z: targetCameraZ, useFrames: true, ease: Regular.easeInOut, onComplete: transitionToTargetComplete } );
						
			// enable some smoothing
//			targetDisplayObject.material.smooth = true;
			
			// transition ALL other planes 
			for ( var i in planes )
			{
				var displayObject = planes[i];
				
				if ( displayObject != targetDisplayObject )
				{
					// calculate the distance from the targetDisplayObject
					var distance:Number = distance( targetDisplayObject, displayObject );
					//var averageZ:Number = Math.floor( ( distance / ( displayObjectWidth / 2 ) ) * 2000 );
					var averageZ:Number = Math.random() * 1000;
					
					TweenLite.to( displayObject, 100, { z: averageZ, useFrames: true } );
				}
			}
		}
		
		public function distance( o1:Object, o2:Object ):Number
		{
			var a:Number = o1.x - o2.x;
			var b:Number = o1.y - o2.y;
			
			return Math.sqrt( Math.pow( a, 2 ) + Math.pow( b, 2 )) ;
		}

		
		private function transitionToTargetComplete():void
		{
			isTweening = false;
		}

		private function update( e:Event ):void
		{
			var stageHalfW:Number = stageWidth / 2;
			var posX:Number = stageHalfW - mouseX;
			
			var stageHalfH:Number = stageHeight / 2;
			var posY:Number = stageHalfH - mouseY;
			
			var averageFov:Number = Math.abs( ( posX / stageHalfW ) * 40 );
			var averagePosX:Number = ( posX / stageHalfW ) * displayObjectWidth / 2;
			var averagePosY:Number = ( posY / stageHalfH ) * 100;
			var averageRotationY:Number = ( posX / stageHalfW ) * cameraPanMax;
			var averageRotationX:Number = ( posY / stageHalfH ) * 10;
			
			// set properties
			camera.rotationY = -averageRotationY;
			camera.rotationX = -averageRotationX;
			
			mouseUpdateRotationX = camera.rotationX;
			mouseUpdateRotationY = camera.rotationY;
			

			// transition ALL other planes 
			/*for ( var i in planes )
			{
				var displayObject = planes[i];

				// calculate the distance from the targetDisplayObject
				var distance:Number = distance( targetDisplayObject, displayObject );
				//var averageZ:Number = Math.floor( ( distance / ( displayObjectWidth / 2 ) ) * 2000 );
				var averageZ:Number = Math.random() * 1000;
				
				TweenLite.to( displayObject, 20, { z: averageZ, useFrames: true } );

			}*/

			/*if ( !isActivated )
			{
				var xPositionFromCenter:Number = stageWidth / 2 - mouseX;
				var yPositionFromCenter:Number = stageHeight / 2 - mouseY;
				var scrollSpeedX:Number = Math.floor( ( xPositionFromCenter / ( stageWidth / 2 ) ) * scrollSpeed );
				var scrollSpeedY:Number = Math.floor( ( yPositionFromCenter / ( stageWidth / 2 ) ) * scrollSpeed );
				
				camera.x += -scrollSpeedX;
				camera.y += scrollSpeedY;
				
				
				// xbounds
				if ( camera.x <= -( displayObjectWidth * targetCameraScale ) / 3  )
				{
					camera.x = - (displayObjectWidth * targetCameraScale ) / 3;
					scrollSpeedX = 0;
				}
				
				if ( camera.x >= ( displayObjectWidth * targetCameraScale ) / 3) 
				{
					camera.x = ( displayObjectWidth * targetCameraScale ) / 3; 
					scrollSpeedX = 0;
				}
				
				// ybounds
				if ( camera.y <= -( displayObjectHeight * targetCameraScale ) / 2 )
				{
					camera.y = -( displayObjectHeight * targetCameraScale ) / 2;
					scrollSpeedY = 0;					
				}
				
				if ( camera.y >= ( displayObjectHeight * targetCameraScale ) / 2 ) 
				{
					camera.y = ( displayObjectHeight * targetCameraScale ) / 2;
					scrollSpeedY = 0;
				}
				
				// motion blur
				//var blurAverageX:Number = Math.floor( ( Math.abs( scrollSpeedX ) / scrollSpeed ) * 8 );
				//var blurAverageY:Number = Math.floor( ( Math.abs( scrollSpeedY ) / scrollSpeed ) * 8 );
				//viewport.containerSprite.filters = [ new BlurFilter( Math.abs(blurAverageX), Math.abs(blurAverageY), 1 ) ];
				 
			}*/
		}
	}
}