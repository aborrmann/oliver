﻿package templates
{	
	import org.papervision3d.objects.primitives.Plane;
	import org.papervision3d.objects.DisplayObject3D;
	import org.papervision3d.materials.MovieAssetMaterial;
	import org.papervision3d.core.proto.MaterialObject3D;
	import org.papervision3d.events.InteractiveScene3DEvent;
	import org.papervision3d.view.BasicView;
	import templates.TouchManager;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import com.greensock.TweenLite;
	import fl.transitions.easing.Regular;
	import com.greensock.easing.*;
	import flash.display.MovieClip;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.display.StageQuality;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.IOErrorEvent;	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import com.oliver.Settings;
	import com.trapeze.framework.managers.TemplateManager;
	import com.trapeze.framework.managers.PrefsManager;
	
    final public class CompanyWork extends BasicView
	{  
		private var stageWidth:Number = 1360;
		private var stageHeight:Number = 768;
		
		private var parentDisplayObject:DisplayObject3D;
		private var displayObject:DisplayObject3D;
		
		private var toutSpacingX:Number = 0;
		private var toutSpacingZ:Number = 200;
		
		private var toutGridSpacingX:Number = 25;
		private var toutGridSpacingY:Number = 25;
		
		private var toutWidth:Number = 274;
		private var toutHeight:Number = 212;
		private var toutAmount:Number;
		private var toutSegments:Number = 5;
		
		private var displayObjectWidth:Number;
		private var displayObjectHeight:Number;
		
		private var cameraFOV:Number = 45;
		private var defaultCameraZ:Number = -700;
		//private var defaultCameraY:Number = -250;
		private var defaultZoom:Number = 20;
		private var defaultCameraScale:Number = 3;
		private var cameraRotationMaxY:Number = 10;
		
		private var defaultOverlayScale:Number = 0.35;
		
		private var cameraPitch:Number = 90;
		private var cameraYaw:Number = 270;
		private var cameraPanMax:Number = 10;
		
		private var targetDisplayObject:DisplayObject3D;
		
		private var targetCameraX:Number;
		private var targetCameraY:Number;
		private var targetCameraZ:Number;
		
		private var planes:Array;
		private var materials:Array;
		
		private var index:Number;
		private var isTweening:Boolean	= false;
		private var isActivated:Boolean = false;
		private var viewMode:String = 'line';
		private var viewModeIndex:Number = 0;
		
		private var lastCameraRotationY:Number;
		private var lastCameraX:Number;
		private var lastCameraZ:Number;
		private var lastCameraY:Number;
		
		private var activatedDuration:Number = 25;
		private var activatedCameraDistanceZ:Number = 300;
		
		private var arrangeCameraDurationIn:Number = 30;
		private var arrangeItemDurationIn:Number = 15;
		
		private var arrangeCameraDurationOut:Number = 25;
		private var arrangeItemDurationOut:Number = 10;
		
		private var touchManager:TouchManager;
		
		private var scope:Object;
		private var json:Object;
		private var content:Array;
		private var overlay:MovieClip;
		
		// Circle mode variables
		// Degrees to Radians
		private var startAngleInDegrees:Number = 180;
		private var startAngleInRadians			 = startAngleInDegrees * Math.PI / 180
		private var currentAngleInDegrees:Number = startAngleInDegrees;
		private var defaultRadius:Number		= 500;
		private var radius:Number 				= defaultRadius;
		private var center:Point 				= new Point( 0, 0 );
		private var inc:Number 					= 0.2;
		private var radiusInc:Number 			= 10;
		private var radiusMin:Number 			= 100;
		private var zoomRange:Number 			= 100;
		private var isScrolling:Boolean			= false;
		private var currentX:Number				= 0;
		private var lastX:Number				= 0;
		private var accelX:Number				= 0;
		private var currentY:Number				= 0;
		private var lastY:Number				= 0;
		private var accelY:Number				= 0;
		private var accelDecay:Number			= 0.9;
		private var offsetY:Number;
		private var dirY:Number;
		private var clips:Array;
		private var dir:Number 					= 0;
		private var itemAmount:Number			= 40;
		
		private var bitmap:Bitmap;
		private var bitmapData:BitmapData;
		private var bitmapRectangle:Rectangle;
		
		private var webBrowserDefaultWebsite:String;
		
        public function CompanyWork( scope:Object, json:Object )
		{			
			// set our parent scope
			this.scope = scope;
			
			// set the content from the JSON object
			this.json = json;
			
			// start the content array
			content = [];
			
			webBrowserDefaultWebsite = null;
			
			// Parse content
			for ( var i in json )
			{
				var slug = json[i].slug;
				var name = json[i].name;
																
				for ( var p in json[i].projects )
				{				
					var project = json[i].projects[p];
					
					var description = project.description;
					var image = project.image;
					var title = project.title;
					var url = project.url;
					
					var element:Object = {};
					element.name = name;
					element.title = title;
					element.description = description;
					element.image = image;
					element.url = url;
										
					content.push( element );
				}
			}
			
			// update our tout amount
			toutAmount = content.length - 1;
						
			super( stageWidth, stageHeight, false );
		}

		/* Creates a MaterialObject3D for use as a tout in 3d space */
		private function createLibraryMaterial( id:Number ):MaterialObject3D
		{
			var material:MovieAssetMaterial = new MovieAssetMaterial('toutAsset',true,true,true);

			material.interactive = true;
			material.name = id.toString();
			material.smooth = true;
			
			// set id by getting instance of MovieClipMaterial base class
			var movieClip:ToutClipMaterial = material.movie as ToutClipMaterial;
			movieClip.init( id, this, content[id] );
			
			// add to items
			materials.push( movieClip );
			return material;
		}
	
        public function init( e:Event ):void
		{
			// adjust FOV for overlay
			root.transform.perspectiveProjection.projectionCenter = new Point( stageWidth / 2, stageHeight / 2 );
			root.transform.perspectiveProjection.fieldOfView = cameraFOV * 2;
			
			// Keyboard event Listeners
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKey );
			
			// Mouse event listeners ( for circle mode )
			stage.addEventListener( MouseEvent.MOUSE_DOWN, onScrollPress );
			stage.addEventListener( MouseEvent.MOUSE_UP, onScrollRelease );
			
			// Register for Touch Events
			touchManager = new TouchManager( stage );
			touchManager.addEventListener( TouchScreenEvent.SWIPE_LEFT, swipeLeft );
			touchManager.addEventListener( TouchScreenEvent.SWIPE_RIGHT, swipeRight );
			touchManager.addEventListener( TouchScreenEvent.SWIPE_UP, swipeUp );
			touchManager.addEventListener( TouchScreenEvent.SWIPE_DOWN, swipeDown );

			// set up line/grid buttons
			scope.selectorClip.addEventListener( MouseEvent.MOUSE_DOWN, viewModeClick );
			
			// OEF Update
			addEventListener( Event.ENTER_FRAME, update );
			
			// render
			startRendering();
			
			// set index ( default to first item )
			index = 0;
			
			// setup pv3d
			this.cacheAsBitmap = true;
			viewport.interactive = true;
			viewport.containerSprite.cacheAsBitmap = true;
			viewport.autoScaleToStage = false;
			viewport.viewportWidth = stageWidth;
			viewport.viewportHeight = stageHeight;
			
			// set up general fov/zoom
			camera.fov = cameraFOV;
			camera.zoom = defaultZoom;			
			camera.target = null;
			
			// setup parent displayObject
			parentDisplayObject = new DisplayObject3D();
			displayObject = new DisplayObject3D();
			parentDisplayObject.addChild( displayObject );
			scene.addChild( parentDisplayObject );
			
			/*bitmapData = new BitmapData( stageWidth, stageHeight, true, 0x000000 );
			bitmapData.draw( viewport.containerSprite );
			bitmap = new Bitmap( bitmapData, "auto", true );
			addChild( bitmap );*/
			
			// create line of planes to house the touts
			planes = [];
			materials = [];
			
			for (var i:int = 0; i < toutAmount; i++)
			{
				// Create material from library and create a Plane for use
				var planeMaterial:MaterialObject3D = createLibraryMaterial( i );
				var plane:Plane = new Plane( planeMaterial, toutWidth, toutHeight, toutSegments, toutSegments );
				
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_PRESS, planeClick );
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_OVER, planeOver );
				plane.addEventListener( InteractiveScene3DEvent.OBJECT_OUT, planeOut );
									
				displayObject.addChild( plane );
				planes.push( plane );
				
				//plane.x = ( toutWidth + toutSpacingX ) * i;
				//plane.z = toutSpacingZ * i;
			}
			
			// update some variables for use of boundaries
			displayObjectWidth = ( toutWidth + toutSpacingX ) * toutAmount;
			displayObjectHeight = toutHeight;
						
			// set up overlay
			overlay = new OverlayClip();
			overlay.x = stageWidth / 2;
			overlay.y = stageHeight / 2;
			overlay.visible = false;
			scope.overlay.addChild( overlay );
			
			// set up closebutton
			overlay.closeButton.addEventListener( MouseEvent.MOUSE_UP, closeOverlay );
			overlay.closeButton.addEventListener( MouseEvent.MOUSE_DOWN, closeOverlayDown );
			overlay.launchButton.addEventListener( MouseEvent.MOUSE_DOWN, launchOverlayDown );
			overlay.launchButton.addEventListener( MouseEvent.MOUSE_UP, launchOverlayLink );
						
			// set scale
			parentDisplayObject.scaleX = parentDisplayObject.scaleY = defaultCameraScale;
			
			// adjust graphic
			scope.selectorClip.gotoAndStop( viewMode );
				
			// arrange
			arrange();
		}
		
		
		private function onScrollPress( e:MouseEvent ):void
		{
			// reset accel
			accelX = 0;
			accelY = 0;
			
			// update lastX/currentX
			lastX = mouseX;
			currentX = mouseX;
		
			// update lastY/currentY
			lastY = mouseY;
			currentY = mouseY;
			
			// set scrolling active
			isScrolling = true;
		}
		
		
		private function onScrollRelease( e:MouseEvent ):void
		{
			// get latest acceleration vectors
			accelX = mouseX - lastX;
			accelY = mouseY - lastY;
			
			// update lastX/currentX
			lastX = mouseX;
			currentX = mouseX;
		
			// update lastY/currentY
			lastY = mouseY;
			currentY = mouseY;
			
			// set scrolling inactive
			isScrolling = false;
			
			dir = ( accelY > 0 ) ? 1 : -1;
			
			//isTweening = true;
		}
		
		
		private function wheelUpdate( e:MouseEvent ):void
		{
			//radius = ( e.delta > 0 ) ? radius + radiusInc : radius - radiusInc;
			//if ( radius <= radiusMin ) radius = radiusMin;
			defaultRadius = ( e.delta > 0 ) ? defaultRadius + radiusInc : defaultRadius - radiusInc;
			if ( defaultRadius <= radiusMin ) defaultRadius = radiusMin;
			
			if ( e.delta > 0 ) accelY -= 5;
			if ( e.delta < 0 ) accelY += 5;
		}

		
		private function viewModeClick( e:MouseEvent ):void
		{
			if ( !isTweening )
			{
				// let's increment our 'viewModeIndex'
				viewModeIndex = ( viewModeIndex + 1 > 2 ) ? 0 : viewModeIndex + 1;
				
				if ( viewModeIndex == 0 )
				{
					viewMode = 'line';
				}
				
				if ( viewModeIndex == 1 )
				{
					viewMode = 'grid';
				}
				
				if ( viewModeIndex == 2 )
				{
					viewMode = 'circle';
				}
				
				// adjust graphic
				scope.selectorClip.gotoAndStop( viewMode );
				
				// arrange
				arrange();
			}
		}
		
		/* Arranges touts in formation given */
		private function arrange():void
		{
			// toggle low quality during transitions
			setQuality('low');
			
			var i:int;
			var j:int;
			var plane; 
			var material;
			
			var newX:Number;
			var newY:Number;
			var newZ:Number;
			
			if ( viewMode == 'line' )
			{
				// arrange by 3d perspective line
				for ( i = 0; i < toutAmount; i++)
				{
					plane = planes[i];
					material = materials[i];
					
					/*plane.x = ( toutWidth + toutSpacingX ) * i;
					plane.y = 0;
					plane.z = toutSpacingZ * i;*/
					
					// reset alpha
					material.alpha = 1;
							
					newX = ( toutWidth + toutSpacingX ) * i;
					newY = 0;
					newZ = toutSpacingZ * i;
					
					TweenLite.to( plane, arrangeItemDurationOut, { x: newX, z: newZ, y: newY, useFrames: true } );
							
				}
				
				// update some variables for use of boundaries
				displayObjectWidth = ( toutWidth + toutSpacingX ) * toutAmount;
				displayObjectHeight = toutHeight;
				
				// position displayObject
				displayObject.x = 0;
				displayObject.y = 0;
								
				// Tween Camera/DisplayObject
				var targetCameraX:Number = ( ( toutWidth + toutSpacingX ) * index ) * defaultCameraScale;
				var targetCameraZ:Number = defaultCameraZ + ( toutSpacingZ * index );
				var targetCameraRotationY:Number =  ( index / ( toutAmount - 1 ) ) * cameraRotationMaxY;
				
				TweenLite.to( camera, arrangeCameraDurationOut, { x: targetCameraX, z: targetCameraZ, y: -50, rotationY: targetCameraRotationY, useFrames: true, onComplete: tweenArrangeComplete } );
				
				// focus the planes
				focusPlanes();
				
				// set tweening
				//isTweening = true;
			}
			
			if ( viewMode == 'grid' )
			{
			    var colcount = Math.ceil ( Math.sqrt( toutAmount  ) );
				var rowcount = Math.floor ( Math.sqrt( toutAmount ) ); 
				
				// arrange by grid
				var inc:Number = 0;
				
				// Alternate grid loop
				/*var boxNum:int = toutAmount;
				var cols:int = Math.sqrt(boxNum);
				 
				for ( i = 0; i < boxNum; i++ )
				{
				    var plane = planes[i];
					if ( plane != null )
					{
					    plane.x = ( toutWidth + toutGridSpacingX ) * (i & (cols-1));
					    plane.y = ( toutHeight + toutGridSpacingY ) * int(i / cols);
						plane.z = 0;
					}
				}*/
								
				// update some variables for use of boundaries
				displayObjectWidth = ( toutWidth + toutGridSpacingX ) * colcount;
				displayObjectHeight = -( toutHeight + toutGridSpacingY ) * rowcount;
				
				for ( i = 0; i < colcount; i++)
				{
					for ( j = 0; j < rowcount; j++)
					{
						plane = planes[inc]; 
						material = materials[inc];
						
						if ( plane != null )
						{
							//plane.x = ( toutWidth + toutGridSpacingX ) * j;
							//plane.y = -( toutHeight + toutGridSpacingY ) * i;
							//plane.z = 0;
							
							// reset alpha
							material.alpha = 1;
							
							newX = -displayObjectWidth / 2 + ( toutWidth + toutGridSpacingX ) * j;
							newY = -displayObjectHeight / 2 + -( toutHeight + toutGridSpacingY ) * i;
							newZ = 0;
							
							TweenLite.to( plane, arrangeItemDurationIn, { x: newX, z: newZ, y: newY, useFrames: true } );
							
							inc++;
						}
					}
				}
				
				
				// update lastRotationY
				//lastCameraRotationY = camera.rotationY;
				//lastCameraX = camera.x;
				//lastCameraZ = camera.z;

				// position displayObject
				displayObject.x = 400;
				displayObject.y = 0;
				
				// Tween Camera/DisplayObject
				// ToDo: How do i correctly get the cameraZ for a sized area on screen ( this is approx'ed for display only )
				var newCameraZ = -Math.sqrt( displayObjectWidth * displayObjectWidth + displayObjectHeight * displayObjectHeight ) * 1.45;
				
				//TweenLite.to( displayObject, arrangeCameraDurationIn, { x: -displayObjectWidth / 2 + 400, y: -displayObjectHeight / 2, useFrames: true, ease: Regular.easeInOut } );
				TweenLite.to( camera, arrangeCameraDurationIn, { x: 0, z: newCameraZ, y: -250, rotationY: 0, useFrames: true, onComplete: tweenArrangeComplete } );
				
				// unfocus the planes
				unfocusPlanes();

				// set tweening				
				//isTweening = true;
			}
			
			if ( viewMode == 'circle' )
			{
				// assign current angle
				var currentAngleInRadians = currentAngleInDegrees * Math.PI / 180;
				
				var l:int = planes.length;
				for ( i = 0; i < l; i++)
				{
					// X/Y/Z calc
					plane = planes[i];
					material = materials[i];
					
					var t:Number = currentAngleInRadians + Math.PI * 2 * i/ l;
					newX = Math.cos(t) * radius / 2; 
					newY = Math.sin(t) * radius; 
					newZ = Math.cos(t) * radius * 3;
					
					// Smily contribution
					/*x = radius*Math.sin(theta)*Math.cos(phi);
					y = radius*Math.sin(theta)*Math.sin(phi);
					z = radius*Math.cos(theta);
					*/
					
					// Angle calc
					var itemAngle:Number = t * 180 / Math.PI;
					var targetAngle:Number = startAngleInRadians;

					//var angleDifference:Number = 180 - Math.abs( angleDifference( targetAngle, itemAngle ) );
					var angleDifference:int = 180 - Math.abs( angleDifference( targetAngle, itemAngle ) );
					
					// Alpha
					//var averageAlpha:Number = 1 - Math.floor ( ( angleDifference / 180 ) * 100 ) / 100;
					//var roundedAverageAlpha:Number = 1 + int((averageAlpha)*100)/100 - 0.75;
					//material.alpha = roundedAverageAlpha;						
							
					TweenLite.to( plane, arrangeItemDurationIn, { x: newX, y: newY, z: newZ, useFrames: true } );	
				}
								
				// update lastRotationY
				//lastCameraRotationY = camera.rotationY;
				//lastCameraX = camera.x;
				//lastCameraZ = camera.z;
				//lastCameraY = camera.y;
									 
				// update some variables for use of boundaries
				//displayObjectWidth = ( toutWidth + toutGridSpacingX ) * colcount;
				//displayObjectHeight = -( toutHeight + toutGridSpacingY ) * rowcount;
				
				// position displayObject
				displayObject.x = 0;
				displayObject.y = 0;
				
				// Tween Camera/DisplayObject
				//TweenLite.to( displayObject, arrangeCameraDurationIn, { x: 0, y: 0, z:0, useFrames: true, ease: Regular.easeInOut } );
				TweenLite.to( camera, arrangeCameraDurationIn, { x: 0, z: -3200, y: -250, rotationY: -8, useFrames: true, onComplete: tweenArrangeComplete } );
				
				// unfocus the planes
				unfocusPlanes();

				// set tweening				
				//isTweening = true;
			}
		}
		
		private function setQuality( lvl:String ):void
		{
			var m:String;
			
			if ( lvl == 'low' )
			{
				for ( m in materials) materials[m].smooth = false;
				stage.quality = StageQuality.LOW;
			}

			if ( lvl == 'med' )
			{
				for ( m in materials) materials[m].smooth = true;
				stage.quality = StageQuality.MEDIUM;

			}
			
			if ( lvl == 'high' )
			{
				for ( m in materials) materials[m].smooth = true;
				stage.quality = StageQuality.HIGH;
			}
		}

		private function tweenArrangeComplete():void
		{
			isTweening = false;
			
			if ( viewMode != 'circle' )
			{
				setQuality( 'high' );
				
			} else {
				
				setQuality( 'low' );
			}
		}
		
		private function focusPlanes():void
		{
			for ( var i in materials ) materials[i].gotoAndStop('off');
			materials[index].gotoAndStop('on');
		}
		
		private function unfocusPlanes():void
		{
			for ( var i in materials ) materials[i].gotoAndStop('off');
		}
		
		
		private function removeHandlers():void
		{
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKey );
			
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_LEFT, swipeLeft );
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_RIGHT, swipeRight );
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_UP, swipeUp );
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_DOWN, swipeDown );
		}
				
		private function swipeLeft( e:TouchScreenEvent ):void
		{
			// Custom controls for line mode
			if ( viewMode == 'line' )
			{
				if ( !isActivated )
				{
					// move right
					index = ( index + 1 > toutAmount - 1 ) ? toutAmount - 1 : index + 1;
					
					// focus items
					focusPlanes();
					
					// tween
					tweenCamera();
				}
			}
		}
		
		private function swipeRight( e:TouchScreenEvent ):void
		{
			if ( viewMode == 'line' )
			{
				if ( !isActivated )
				{
					// move left
					index = ( index - 1 < 0 ) ? 0 : index - 1;
					
					// focus items
					focusPlanes();
					
					// tween
					tweenCamera();
				}
			}
		}


		private function swipeUp( e:TouchScreenEvent ):void
		{
		}

		private function swipeDown( e:TouchScreenEvent ):void
		{
		}
		
		private function planeClick( e:InteractiveScene3DEvent ):void
		{			
		}
		
		private function planeOver( e:InteractiveScene3DEvent ):void
		{
		}
		
		private function planeOut( e:InteractiveScene3DEvent ):void
		{			
		}
		
		private function onKey( e:KeyboardEvent ):void
		{
			if ( viewMode == 'line' )
			{
				if ( !isActivated )
				{
					var dir:Number;
					
					if ( e.keyCode == 39 )
					{
						// move right
						index = ( index + 1 > toutAmount - 1 ) ? toutAmount - 1 : index + 1;
						dir = 1;
						
						// focus items
						focusPlanes();
						
						// tween
						tweenCamera();
					}
					
					if ( e.keyCode == 37 )
					{
						// move left
						index = ( index - 1 < 0 ) ? 0 : index - 1;
						dir = -1;
						
						// focus items
						focusPlanes();
						
						// tween
						tweenCamera();
					}
				}
			}
		}
		
		private function tweenCamera():void
		{
			// Tween
			var targetCameraX:Number = planes[index].x * defaultCameraScale;
			var targetCameraZ:Number = defaultCameraZ + planes[index].z;
			var targetCameraRotationY:Number =  ( index / ( toutAmount - 1 ) ) * cameraRotationMaxY;
		
			TweenLite.to( camera, 20, { x: targetCameraX, z: targetCameraZ, rotationY: ( cameraRotationMaxY / 2 ) - targetCameraRotationY, useFrames: true, onComplete: tweenCameraComplete } );
			isTweening = true;
		}
		
		private function update( e:Event ):void
		{
			// draw
			/*bitmapData = new BitmapData( stageWidth, stageHeight, true, 0x000000 );
			bitmapData.draw( viewport.containerSprite, null, null, null, new Rectangle( 0, 0, stageWidth, stageHeight ), true );
			bitmap = new Bitmap( bitmapData, "auto", true );*/
			
			// Only process acceleration if we;re in circle mode
			if ( viewMode == 'circle' )
			{
				// Only update if not tweening in this case ( since we want to see the transition )
				if ( !isTweening )
				{
					// ACCEL
					var roundedAccelY:Number;
					
					// update acceleration
					if ( isScrolling )
					{
						// calculate acceleration
						lastX = currentX;
						currentX = mouseX;
				
						lastY = currentY;
						currentY = mouseY;
						
						accelX = mouseX - lastX;
						accelY = mouseY - lastY;
						
						// apply accel to scrollV
						roundedAccelY = int( accelY * 10 ) / 10;				
						
						
					} else {
						
						accelY *= accelDecay;
						
						// apple accelt to scrollV
						roundedAccelY = int( accelY * 10 ) / 10;
					}
					
					// apply acceleration with dampening
					currentAngleInDegrees += roundedAccelY * 0.15;
					
					// assign direction
					if ( accelY > 0 ) dirY = -1;
					if ( accelY < 0 ) dirY = 1;
					
					// assign current angle
					var currentAngleInRadians = currentAngleInDegrees * Math.PI / 180;
					
					// check bounds
					if ( currentAngleInDegrees > 360 ) currentAngleInDegrees = 0;
					if ( currentAngleInDegrees < 0 ) currentAngleInDegrees = 360;
				

					var len:int = planes.length;
					for (var i:int = 0; i < len; i++)
					{
						// X/Y calc
						var obj = planes[i];
						var material = materials[i];
						var t:Number = currentAngleInRadians + Math.PI * 2 * i/len;
						var ix:Number = Math.cos(t) * radius / 2;
						var iy:Number = Math.sin(t) * radius;
						var iz:Number = Math.cos(t) * radius * 3;
						
						obj.x = ix + center.x;
						obj.y = iy + center.y;
						obj.z = iz + center.x;
						
						// Angle calc
						var itemAngle:Number = t * 180 / Math.PI;
						var targetAngle:Number = startAngleInRadians;

						//var angleDifference:Number = 180 - Math.abs( angleDifference( targetAngle, itemAngle ) );
						var angleDifference:int = 180 - Math.abs( angleDifference( targetAngle, itemAngle ) );
						
						// Alpha
						/*var averageAlpha:Number = 1 - Math.floor ( ( angleDifference / 180 ) * 100 ) / 100;
						var roundedAverageAlpha:Number = 1 + int((averageAlpha)*100)/100 - 0.75;
						material.alpha = roundedAverageAlpha;*/
					}
				}
			}
		}
		
		private function tweenCameraComplete():void
		{
			isTweening = false;
		}
		
		private function launchOverlayDown( e:MouseEvent ):void
		{
			overlay.launchButton.gotoAndStop('over');
		}
		
		private function launchOverlayLink( e:MouseEvent ):void
		{
			overlay.launchButton.gotoAndStop('up');
			
			// set website in settings
			Settings.webBrowserDefaultWebsite = 'http://www.trapeze.com' + webBrowserDefaultWebsite;
			
			// Launch the browser
			TemplateManager.load( PrefsManager.getValue('WebBrowser') );		
		}
		
		public function activateItem( id:Number, content:Object ):void
		{
			// activates the item that has been clicked by loading the overlay
			
			// Do nothing if currently tweening
			if ( !isTweening )
			{
				// bind the data
				overlay.title.label_txt.htmlText = content.title;
				overlay.project.label_txt.htmlText = content.name;
				overlay.desc.label_txt.htmlText = content.description;
				
				// set default link out 
				webBrowserDefaultWebsite = content.url;
				
				// load the image
				var imageLoader:Loader = new Loader();
				imageLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, imageLoaded );
				imageLoader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, imageError );
				imageLoader.load( new URLRequest( content.image ) );
				
				while ( overlay.image.container.numChildren > 0 ) overlay.image.container.removeChildAt( 0 );
				
				function imageLoaded( e:Event ):void
				{
					// size
					e.currentTarget.content.width = 489;
					e.currentTarget.content.height = 300;
					
					// add the image
					overlay.image.container.addChild( e.currentTarget.content );
				}
				
				function imageError( e:IOErrorEvent ):void
				{
					trace("error loadig overlay image, see CompanyWork class");
				}
			
				//  Grid mode activation
				if ( viewMode == 'grid' )
				{
					// GRID MODE click
					index = id;
					
					// show overlay
					showOverlay();
				}
				
				// Line mode activation
				if ( viewMode == 'line' )
				{
					// LINE MODE click
					if ( id == index )
					{
						// show overlay
						showOverlay();
					}
				}
				
				// Circle mode activation
				if ( viewMode == 'circle' )
				{
					// GRID MODE click
					index = id;
					
					// show overlay
					showOverlay();
				}
				
				// unfocus planes
				unfocusPlanes();
			}
		}
				
		//private function angleDifference( firstAngle:Number, secondAngle:Number ):Number
		private function angleDifference( firstAngle:Number, secondAngle:Number ):int
		{
			var difference:Number = secondAngle - firstAngle;
			while (difference < -180) difference += 360;
			while (difference > 180) difference -= 360;
			return difference;
		}

		private function showOverlay():void
		{
			overlay.visible = true;
			overlay.gotoAndPlay('open');
			
			// let's hide our viewpoint
			viewport.containerSprite.visible = false;
			
			// hide the viewMode button
			scope.selectorClip.visible = false;
		}
		
		private function tweenShowOverlayComplete():void
		{
			isTweening = false;
		}
		
		private function hideOverlay():void
		{			
			overlay.visible = false;
			overlay.gotoAndStop(1);
		}
		
		private function closeOverlayDown( e:MouseEvent ):void
		{
			overlay.closeButton.gotoAndStop('over');
		}
		
		private function closeOverlay( e:MouseEvent ):void
		{
			overlay.closeButton.gotoAndStop('up');
			
			// close button inside overlay
			hideOverlay();
			viewport.containerSprite.visible = true;
			
			// show viewMode button
			scope.selectorClip.visible = true;
			
			// refocus planes
			if ( viewMode == 'line' ) focusPlanes();
		}
		
		private function tweenHideOverlayComplete():void
		{
			//overlay.visible = false;
			isTweening = false;
		}
		
	}
}