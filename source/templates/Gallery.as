﻿package templates {
    import com.trapeze.framework.templates.BaseTemplate;
    import flash.events.Event;
	import org.cove.ape.*;
	import flash.events.MouseEvent;
	
    public class Gallery extends BaseTemplate
	{    
		private var gallery:LobbyView;
		private var photos:Group;
		private var walls:Group;
		
        public function Gallery() {}
		
        override public function onFrameworkComplete(e:Event):void
		{
			initAPE();
			initWalls();
			initObjects();
			
			this.addEventListener(Event.ENTER_FRAME, run);
        }
		
		private function initAPE():void
		{			
			APEngine.init();
			APEngine.container = this;
			
			// Add gravity
			APEngine.addForce( new VectorForce( false, 0, 10 ) );
			
			APEngine.damping = .99;
			APEngine.constraintCollisionCycles = 1;
		}
		 
		private function initWalls():void
		{
			walls = new Group();
			walls.collideInternal = false;
			
			// top
			var top:RectangleParticle = new RectangleParticle( 1280 / 2, 10, 1280, 20, 0, true );
			
			// bottom
			var bottom:RectangleParticle = new RectangleParticle( 1280 / 2, 720 - 20, 1280, 20, 0, true );
			
			// left
			var left:RectangleParticle = new RectangleParticle( 10, 720 / 2, 20, 720, 0, true );
			
			// right
			var right:RectangleParticle = new RectangleParticle( 1280 - 20, 720 / 2, 20, 720, 0, true );
			
			walls.addParticle( top );
			walls.addParticle( bottom );
			walls.addParticle( left );
			walls.addParticle( right );
			
			APEngine.addGroup(walls);
		}
		
		private function initObjects():void
		{
			photos = new Group();
			photos.collideInternal = false;
			
			for ( var i = 0; i < 25; i++ )
			{
				var box:Photo = new Photo();
			
				box.sprite.setStyle(0,0,0,0x999999);
				photos.addParticle(box.sprite);
				
				box.sprite.addEventListener( MouseEvent.MOUSE_DOWN, photoPress );
				box.sprite.addEventListener( MouseEvent.MOUSE_UP, photoRelease );
			}
			
			photos.addCollidable( walls );
			
			APEngine.addGroup(photos);
		}
		
		private function run(evt:Event):void {
			
			APEngine.step();
			APEngine.paint();
		}
		
		private function photoPress( e:MouseEvent ):void
		{
			
		}
		private function photoRelease( e:MouseEvent ):void
		{
			
		}
    }
}