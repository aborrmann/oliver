﻿package templates
{
    import flash.events.Event;
    
    public class TouchScreenEvent extends Event
    {
        public static const SWIPE_LEFT:String = "swipeLeft";
		public static const SWIPE_RIGHT:String = "swipeRight";
		public static const SWIPE_UP:String = "swipeUp";
		public static const SWIPE_DOWN:String = "swipeLeft";
        
        public function TouchScreenEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
        {
            super(type, bubbles, cancelable);
        }
        
    }
}