﻿package templates
{
	import com.trapeze.framework.managers.CopyManager;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.TemplateManager;
	import com.trapeze.framework.managers.NavigationManager;
	import com.trapeze.framework.templates.BaseTemplate;
	
	import com.greensock.TweenLite;
	
    import flash.events.Event;
	import templates.LobbyView;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.MouseEvent;
	import com.oliver.Settings;
	
    public class LobbyView extends BaseTemplate
	{    
		private var lobby:Lobby;	
		private var contentLoader:Loader;
		
        public function LobbyView() {}
		
        override public function onFrameworkComplete(e:Event):void
		{
        	// lobby
			lobby = new Lobby( this );
			lobby.addEventListener( Event.ADDED_TO_STAGE, init );
			container.addChild( lobby );
			
			// Tell the navigation to hide ( hacked in )
			if ( NavigationManager.navigationClip != null ) 
			{
				var navClip:MovieClip = NavigationManager.navigationClip as MovieClip;
				navClip.hide();
			}
			
			function init( event:Event ):void
			{
				lobby.init();
			}
        }
		
		public function loadTemplate( id:String ):void
		{
			trace("Loading template: " + id );
			var pointerTemplate:String = CopyManager.getValue( id );
			TemplateManager.load( PrefsManager.getValue( pointerTemplate ) );
			
			// Tell the navigation to hide ( hacked in )
			if ( NavigationManager.navigationClip != null ) 
			{
				var navClip:MovieClip = NavigationManager.navigationClip as MovieClip;
				navClip.show();
			}
		}
    }
}