﻿package templates
{
	import com.trapeze.framework.templates.BaseTemplate;
	import templates.CompanyGallery;
	import flash.events.Event;
	import flash.display.Sprite;
	
    public class CompanyGalleryView extends BaseTemplate
	{
		private var gallery:CompanyGallery;
		
        public function CompanyGalleryView() {}
		
		// USED AS A FRAMEWORK TEMPLATE
        override public function onFrameworkComplete(e:Event):void
		{
			// Create a new gallery
        	gallery = new CompanyGallery();
			gallery.addEventListener( Event.ADDED_TO_STAGE, init );
			container.addChild( gallery );
			
			function init( event:Event ):void
			{
				gallery.init( null );
			}
        }
	}
}