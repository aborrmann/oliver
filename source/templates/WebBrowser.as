﻿package templates {
    import com.greensock.easing.Strong;
    import com.greensock.plugins.AutoAlphaPlugin;
    import com.greensock.plugins.TweenPlugin;
    import com.greensock.TweenLite;
    import com.oliver.Settings;
    import com.trapeze.framework.templates.BaseTemplate;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.StringUtils;
    import flash.display.SimpleButton;
    import flash.display.Sprite;
    import flash.errors.IOError;
    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.html.HTMLLoader;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;
    import flash.text.TextField;
    import templates.TouchManager;
    import templates.TouchScreenEvent;
    
    /**
     * ...
     * @author Adrian Borrmann
     */
    public class WebBrowser extends BaseTemplate {
        
        public var addressBar:TextField;
        public var backButton:SimpleButton;
        public var forwardButton:SimpleButton;
        public var stopButton:SimpleButton;
        public var refreshButton:SimpleButton;
        public var htmlContainer:Sprite;
        public var progressBar:Sprite;
        public var errorField:TextField;
        public var scrollBar:Sprite;
        public var scrollTrack:Sprite;
        
        private var currentLocation:String;
        
        private var httpLoader:URLLoader = new URLLoader();
        private var htmlLoader:HTMLLoader = new HTMLLoader();
        private var htmlFocusedElement:Object;
        
        private var isScrolling:Boolean            = false;
        private var isFading:Boolean            = false;
        
        private var scrollPositionY:Number        = 0;
        private var scrollPositionX:Number        = 0;
        
        private var currentX:Number                = 0;
        private var lastX:Number                = 0;
        private var accelX:Number                = 0;
        private var currentY:Number                = 0;
        private var lastY:Number                = 0;
        private var accelY:Number                = 0;
        private var accelDecay:Number            = 0.92;
        
        private var accelSpeedMaxY:Number        = 25;
        private var accelSpeedMaxX:Number        = 10;
        
        private var browserWidth:Number            = 1360;
        private var browserHeight:Number        = 695;
        
        private var touchManager:TouchManager;
        
        public function WebBrowser() {
            
            removeChild(errorField);
            progressBar.visible = false;
            
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            addressBar.addEventListener(MouseEvent.CLICK, onAddressBarFocus);
            httpLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHTTPStatus);
            httpLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
            htmlLoader.addEventListener(Event.COMPLETE, onPageComplete);
            htmlLoader.addEventListener(Event.LOCATION_CHANGE, onLocationChange);
            htmlLoader.addEventListener(Event.HTML_DOM_INITIALIZE, onDOMInitialize);
            htmlLoader.addEventListener(Event.HTML_RENDER, onPageRender);
            
            httpLoader.dataFormat = URLLoaderDataFormat.TEXT;
            htmlLoader.placeLoadStringContentInApplicationSandbox = true;
            
            backButton.addEventListener(MouseEvent.CLICK, onBackButton);
            forwardButton.addEventListener(MouseEvent.CLICK, onForwardButton);
            stopButton.addEventListener(MouseEvent.CLICK, onStopButton);
            refreshButton.addEventListener(MouseEvent.CLICK, onRefreshButton);
            
            forwardButton.alpha = backButton.alpha = 0.5;
            forwardButton.enabled = backButton.enabled = false;
            
            TweenPlugin.activate([AutoAlphaPlugin]);
            
            // OEF Update
            this.addEventListener( Event.ENTER_FRAME, update );
            
            // Register for Touch Events
            touchManager = new TouchManager( stage );
            touchManager.addEventListener( TouchScreenEvent.SWIPE_LEFT, swipeLeft );
            touchManager.addEventListener( TouchScreenEvent.SWIPE_RIGHT, swipeRight );
            
            // Register for scrollbar events
            scrollBar.addEventListener(MouseEvent.MOUSE_OVER, scrollBarShow);
            scrollBar.addEventListener(MouseEvent.MOUSE_OUT, scrollBarHide);
        }
        
        
        override public function onFrameworkComplete(e:Event):void { }
        
        
        private function initScrolling():* {
            
            // Listen for a mouse interaction to decide if the user is scrolling or clicking
            htmlLoader.window.document.onmousedown = onHTMLMouseDown;
            
            // Always disable text selection in the document
            htmlLoader.window.document.onselectstart = function() {
                return false;
            };
            
            // hide scrollbar
            scrollBarHide();
        }
        
        
        private function swipeLeft( e:TouchScreenEvent ):void
        {
            onBackButton( null );
        }
        
        private function swipeRight( e:TouchScreenEvent ):void
        {
            onForwardButton( null );
        }
        
        
        /* Start Acceleration / Throw effect */
        private function update( e:Event ):void
        {
            var roundedAccelY:Number;
            
            // update acceleration
            if ( isScrolling )
            {
                // calculate acceleration
                lastX = currentX;
                currentX = mouseX;
    
                lastY = currentY;
                currentY = mouseY;
                
                accelX = mouseX - lastX;
                accelY = mouseY - lastY;
                
                // apply accel to scrollV
                roundedAccelY = int( accelY * 10 ) / 10;                
                htmlLoader.scrollV += roundedAccelY * -1;
                
            } else {
                
                accelY *= accelDecay;
                
                // apple accelt to scrollV
                roundedAccelY = int( accelY * 10 ) / 10;
                htmlLoader.scrollV += roundedAccelY * -1;
            }
            
            //trace(htmlLoader.scrollV + " : " + htmlLoader.contentHeight );
            
            // Fade out scrollbar if accel is 0 and we're not scrolling anymore
            if ( roundedAccelY == 0 && !isScrolling )
            {
                if ( isFading )
                {
                    isFading = false;
                    scrollBarHide();
                }
            }
            
            // update scrollBar
            scrollBar.y = Math.floor( ( htmlLoader.scrollV / htmlLoader.contentHeight ) * ( scrollTrack.height - scrollBar.height ) ) + scrollTrack.y;
            
        }
        
        private function onHTMLMouseDown(e:Object):* {
            
            // Listen for a mouse move. If it moves, then we are scrolling; otherwise we are clicking.
            htmlLoader.window.document.onmousemove = onHTMLMouseMove;
            htmlLoader.window.document.onmouseup = onHTMLMouseUp;
        }
        
        
        private function onHTMLMouseUp(e:Object):* {
            
            if (isScrolling) {
                onScrollRelease();
            }
            
            htmlLoader.window.document.onmousemove = null;
            htmlLoader.window.document.onmouseup = null;
        }
        
        
        private function onHTMLMouseMove(e:Object):* {
            
            htmlLoader.window.document.onmousemove = null;
            onScrollPress();
        }
        
        
        private function onScrollPress( e:Object = null ):*
        {
            // reset accel
            accelX = 0;
            accelY = 0;
            
            // update lastX/currentX
            lastX = mouseX;
            currentX = mouseX;
    
            // update lastY/currentY
            lastY = mouseY;
            currentY = mouseY;
            
            // set scrolling active
            isScrolling = true;
            
            // show scrollBar
            if ( !isFading )
            {
                isFading = true;
                scrollBarShow();
            }
        }
        
        private function onScrollRelease( e:Object = null ):*
        {
            // get latest acceleration vectors
            accelX = mouseX - lastX;
            accelY = mouseY - lastY;
            
            // update lastX/currentX
            lastX = mouseX;
            currentX = mouseX;
    
            // update lastY/currentY
            lastY = mouseY;
            currentY = mouseY;
            
            // set scrolling inactive
            isScrolling = false;
        }
        /* End of Acceleration / Throw effect */
        
        
        
        public function loadWebsite(url:String):void {
            
            if (! (StringUtils.beginsWith(url, "http://") || StringUtils.beginsWith(url, "https://"))) {
                url = "http://" + url;
            }
            addressBar.text = url;
            
            if (errorField.stage) removeChild(errorField);
            
            progressBar.width = 0;
            progressBar.visible = true;
            TweenLite.to(progressBar, 5, { scaleX:0.75, ease:Strong.easeOut } );
            
            Log.info("Loading website " + url);
            htmlLoader.load(new URLRequest(url));
            
            forwardButton.enabled = false;
            forwardButton.alpha = 0.5;
        }
        
        
        private function onLocationChange(e:Event):void {
            
            var location:String = htmlLoader.location;
            
            Log.info("Location change: " + location);
            Settings.keyboardFocusedElement = null;
            addressBar.text = location;
            
            Settings.keyboard.hide();
            
            if (htmlLoader.historyPosition == 0 || htmlLoader.historyLength == 0) {
                backButton.enabled = false;
                backButton.alpha = 0.5;
            }
            else {
                backButton.enabled = true;
                backButton.alpha = 1.0;
            }
            
            if (htmlLoader.historyPosition >= htmlLoader.historyLength - 1) {
                forwardButton.enabled = false;
                forwardButton.alpha = 0.5;
            }
            else {
                forwardButton.enabled = true;
                forwardButton.alpha = 1.0;
            }
            
            
            stopButton.visible = true;
            refreshButton.visible = false;
        }
        
        
        private function onHTTPStatus(e:HTTPStatusEvent):void {
            
            if (httpLoader.data) {
                htmlLoader.loadString(httpLoader.data);
            }
            
            switch (e.status) {
                
                case 200:
                    break;
                
                default:
            }
        }
        
        
        private function onIOError(e:IOErrorEvent):void {
            
            errorField.text = "Error loading URL:\n";
            errorField.appendText(addressBar.text + "\n");
            addChild(errorField);
        }
        
        
        private function onDOMInitialize(e:Event):void {
            
            Log.info("DOM initialized.");
            
            // Listen for focus events so that we can show the on-screen keyboard
            htmlLoader.window.addEventListener("focus", onJavaScriptFocusIn, true);
            htmlLoader.window.addEventListener("blur", onJavaScriptFocusOut, true);
            
            initScrolling();
        }
        
        
        private function onJavaScriptFocusIn(e:Object):void {
            
            if ( e.target.localName == "input" || e.target.localName == "textarea" ) {
                
                htmlFocusedElement = Settings.keyboardFocusedElement = htmlLoader.window.document.activeElement;
                htmlFocusedElement.focus();
                if( htmlFocusedElement != null ) {
                    Settings.keyboard.showKeyboard(htmlFocusedElement, "value", "selectionStart", "selectionEnd", "setSelectionRange", submitHTMLForm);
                }
            }
        }
        
        
        private function onJavaScriptFocusOut(e:Object):void {
            
            if ( e.target.localName == "input" || e.target.localName == "textarea" ) {
                Settings.keyboardFocusedElement = null;
            }
        }
        
        
        private function submitHTMLForm():void {
            
            try {
                htmlFocusedElement.form.submit();
            }
            catch (error:Error) {
                Log.warn("Could not submit HTML form.");
            }
        }
        
        
        private function onPageComplete(e:Event):void {
            
            stopButton.visible = false;
            refreshButton.visible = true;
            
            // Decide if we need a scrollbar
            scrollBar.visible = scrollTrack.visible = htmlLoader.window.document.height > browserHeight ? true : false;
        }
        
        
        private function onPageRender(e:Event):void {
            
            currentLocation = htmlLoader.location;
            
            TweenLite.killTweensOf(progressBar);
            progressBar.scaleX = 1.0;
            TweenLite.to(progressBar, 2, { autoAlpha:0, ease:Strong.easeIn } );
        }
        
        
        private function onBackButton(e:MouseEvent):void {
            
            htmlLoader.historyBack();
            
            forwardButton.alpha = 1.0;
            forwardButton.enabled = true;
            
            if (htmlLoader.historyPosition == 0) {
                backButton.enabled = false;
                backButton.alpha = 0.5;
            }
        }
        
        
        private function onForwardButton(e:MouseEvent):void {
            
            htmlLoader.historyForward();
        }
        
        
        private function onRefreshButton(e:MouseEvent):void {
            
            htmlLoader.reload();
        }
        
        
        private function onStopButton(e:MouseEvent):void {
            
            htmlLoader.cancelLoad();
        }
        
        
        private function onAddedToStage(e:Event):void {
            
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
            
            htmlLoader.width = browserWidth;
            htmlLoader.height = browserHeight;//stage.stageHeight - htmlContainer.y - 25;
            
            htmlContainer.addChild(htmlLoader);
            
            if (Settings.webBrowserDefaultWebsite) {
                loadWebsite(Settings.webBrowserDefaultWebsite);
                Settings.webBrowserDefaultWebsite = "http://www.trapeze.com";
            }
        }
        
        
        private function onAddressBarFocus(e:Event):void {
            
            Settings.keyboardFocusedElement = addressBar;
            if (addressBar.selectedText.length == 0) {
                addressBar.setSelection(0, addressBar.text.length);
            }
            
            Settings.keyboard.showKeyboard(addressBar, "text", "selectionBeginIndex", "selectionEndIndex", "setSelection", onKeyboardComplete);
        }
        
        
        private function onKeyboardComplete():void {
            
            loadWebsite(addressBar.text);
            Settings.keyboard.hide();
        }
        
        
        private function onKeyUp(e:KeyboardEvent):void {
            
            switch (e.keyCode) {
                
                case 13:
                    loadWebsite(addressBar.text);
                break;
            }
        }
        
        
        private function scrollBarShow(e:MouseEvent = null):void {
            
            TweenLite.to( scrollBar, 10, { alpha: 1, useFrames: true } );
        }
        
        
        private function scrollBarHide(e:MouseEvent = null):void {
            
            TweenLite.to( scrollBar, 10, { alpha: 0.25, useFrames: true } );
        }
    }
}