﻿package templates
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.display.BlendMode;

	import org.papervision3d.core.proto.MaterialObject3D;
	import org.papervision3d.lights.PointLight3D;
	import org.papervision3d.materials.ColorMaterial;
	import org.papervision3d.materials.MovieAssetMaterial;
	import org.papervision3d.materials.BitmapMaterial;
	import org.papervision3d.materials.shaders.CellShader;
	import org.papervision3d.materials.shaders.ShadedMaterial;
	import org.papervision3d.materials.shaders.Shader;
	import org.papervision3d.materials.utils.MaterialsList;
	import org.papervision3d.objects.primitives.Cube;
	import org.papervision3d.view.BasicView;
	import org.papervision3d.view.stats.StatsView;
	import org.papervision3d.events.InteractiveScene3DEvent;
	import flash.display.StageQuality;

	import fl.transitions.easing.*;

	import flash.text.TextField;

	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.events.TouchEvent;
	import flash.events.GestureEvent;
	import flash.events.GesturePhase;
	import flash.events.TransformGestureEvent;
	import flash.events.PressAndTapGestureEvent;

	import com.greensock.TweenLite;
	import flash.filters.BlurFilter;
	import org.papervision3d.objects.primitives.Plane;
	import org.papervision3d.objects.DisplayObject3D;

	import com.trapeze.framework.managers.PrefsManager;
	import com.trapeze.framework.managers.TemplateManager;

	import templates.TouchManager;

	public class Lobby extends BasicView
	{
		private var light:PointLight3D;
		private var cube:Cube;
		private var materialsList:MaterialsList;
		private var sides:int = Cube.LEFT + Cube.RIGHT + Cube.FRONT + Cube.BACK;

		private var isTweening:Boolean = false;
		private var isZooming:Boolean = false;
		private var isZoomed:Boolean = false;
		private var isActivated:Boolean = false;
		private var isPanning:Boolean = false;
		private var isDisabled:Boolean = false;

		private var startPositionY:Number = 0;			// start position of cube
		private var isTopLevel:Boolean = false;			// activate movement up and down y axis ( cube y position )
		
		private var startCameraPosZ:Number = -3200;		// start position when entering lobby
		
		private var cameraPosZ:Number = -900;
		private var cameraPosY:Number = 0;
		private var cameraPosX:Number = 0;

		private var cameraFOV:Number = 30;
		private var cameraZoomMax:Number = 60;
		private var cameraZoomMin:Number = 32;

		private var zoomInc:Number = 3;

		private var angles:Array = [0,90,180,270];
		private var angle_inc:Number = 90;

		private var transitionDuration:Number = 10;
		private var zoomInDuration:Number = 10;
		private var zoomOutDuration:Number = 10;
		private var tweenType:Function = Back.easeOut;

		private var ids:Array;
		private var items:Array;
		private var index:Number = 0;

		private var currentID:String;
		private var currentMaterial:MovieClipMaterial;
		private var lastMaterial:MovieClipMaterial;

		private var currentSpeedX:Number = 0;
		private var currentSpeedY:Number = 0;

		public var newZoom:Number = 0;
		public var rotateY:Number = 0;

		public var lastRotateY:Number = 0;
		public var newRotateY:Number = 0;

		private var cubeWidth:Number = 1000;
		private var cubeHeight:Number = 500;
		private var cubeSegments:Number = 10;

		private var stageWidth:Number = 1360;
		private var stageHeight:Number = 768;
		
		private var zoomScale:Number = 0;

		private var face:Plane;

		private var qualityToggle:Boolean = false;

		private var scope:Object;
		
		public var touchManager:TouchManager;
				
		public function Lobby( scope:Object )
		{
			this.scope = scope;
		}

		public function init():void
		{
			touchManager = new TouchManager( stage );

			items = [];
			ids = ['frontAsset','leftAsset','backAsset','rightAsset'];

			createMaterialsList();

			this.cacheAsBitmap = false;
			viewport.containerSprite.cacheAsBitmap = false;
			viewport.interactive = true;
			viewport.autoScaleToStage = false;
			viewport.viewportWidth = stageWidth;
			viewport.viewportHeight = stageHeight;

			// create 3d lobby
			cube = new Cube(materialsList,cubeWidth,cubeWidth,cubeHeight,cubeSegments,cubeSegments,cubeSegments,sides);
			cube.y = startPositionY;
			scene.addChild(cube);

			// create face
			// create face
			/*var faceMaterial:MaterialObject3D = createFaceMaterial();
			face = new Plane(faceMaterial,250,428,5,5);
			scene.addChild( face );*/

			// initial camera settings
			camera.fov = cameraFOV;
			camera.z = cameraPosZ;
			camera.y = cameraPosY;
			camera.x = cameraPosX;
			camera.zoom = cameraZoomMin;
			
			currentID = ids[index];
			currentMaterial = items[index];

			focusItems();

			startRendering();
			startHandlers();
			startAnimation();
		}

		// This enables all lobby function
		public function enable():void
		{
			// enables lobby again, after sub content is removed
			isDisabled = false;
			transitionClose();
		}
		
		// This disablesall lobby function
		public function disable():void
		{
			isDisabled = true;
		}
		
		private function removeHandlers():void
		{
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, keyHandler );
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_LEFT, swipeLeft );
			touchManager.removeEventListener( TouchScreenEvent.SWIPE_RIGHT, swipeRight );
		}
		
		private function startHandlers():void
		{
			// Keyboard Handlers
			stage.addEventListener( KeyboardEvent.KEY_DOWN, keyHandler );

			// let's set our mode
			//Multitouch.inputMode = MultitouchInputMode.GESTURE;
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;

			// Gesture Events
			/*if (Multitouch.supportsGestureEvents)
			{
				var supportedGesturesVar:Vector.<String >  = Multitouch.supportedGestures;
				var deviceSupports:TextField = new TextField();
				deviceSupports.width = 200;
				deviceSupports.height = 200;
				deviceSupports.wordWrap = true;
				addChild(deviceSupports);

				for (var i:int=0; i<supportedGesturesVar.length; ++i)
				{
					deviceSupports.appendText(supportedGesturesVar[i] + ",  ");
				}

				
				Windows 7
				•TransformGestureEvent.GESTURE_PAN
				•TransformGestureEvent.GESTURE_ROTATE
				•TransformGestureEvent.GESTURE_ZOOM
				•GestureEvent.GESTURE_TWO_FINGER_TAP
				•PressAndTapGestureEvent.GESTURE_PRESS_AND_TAP
				*/

				// handlers
				/*
				stage.addEventListener( TransformGestureEvent.GESTURE_PAN, gesturePan );
				stage.addEventListener( TransformGestureEvent.GESTURE_ROTATE, gestureRotate );
				stage.addEventListener( TransformGestureEvent.GESTURE_ZOOM, gestureZoom );
				stage.addEventListener( GestureEvent.GESTURE_TWO_FINGER_TAP, gestureTwoFingerTap );
				stage.addEventListener( PressAndTapGestureEvent.GESTURE_PRESS_AND_TAP, gesturePressAndTap );
			}*/

			// Touch Events
			// llisten to TouchManager
			touchManager.addEventListener( TouchScreenEvent.SWIPE_LEFT, swipeLeft );
			touchManager.addEventListener( TouchScreenEvent.SWIPE_RIGHT, swipeRight );
		}


		/* SWIPE EVENTS from TouchManager */
		private function swipeLeft( e:TouchScreenEvent ):void
		{
			transitionLeft();
		}

		private function swipeRight( e:TouchScreenEvent ):void
		{
			transitionRight();
		}

		private function swipeUp( e:TouchScreenEvent ):void
		{
			if (! isTopLevel)
			{
				transitionUp();
			}
			if (isActivated)
			{
				transitionClose();
			}
			if (isZoomed)
			{
				transitionZoomOut();
			}
		}

		private function swipeDown( e:TouchScreenEvent ):void
		{
			if (isTopLevel)
			{
				transitionDown();
			}
		}
		/* END OF SWIPE EVENT HANDLERS */


		private function startZoomOut():void
		{
			// zoom close (pinch)
			if (isActivated && ! isZoomed)
			{
				// close the tout
				if (! isTweening)
				{
					transitionClose();
				}

			}
			else
			{


				// zoom out to default
				if (isZoomed && ! isZooming && ! isTweening)
				{
					transitionZoomOut();
				}
			}
		}

		public function startZoomIn():void
		{
			// zoom in
			if (! isActivated)
			{
				// open the tout
				if (! isTweening)
				{
					transitionOpen();
				}

			}
			else
			{

				// zoom into the open tout
				if (! isZoomed && ! isZooming && ! isTweening)
				{
					transitionZoomIn();
				}
			}
		}


		private function startAnimation():void
		{
			// intro animation
			cube.y = startPositionY;
			camera.z = startCameraPosZ;

			TweenLite.to( camera, 20, { z: cameraPosZ, onComplete: startAnimationComplete, useFrames: true, ease: Strong.easeOut } );
			isTweening = true;
		}

		private function startAnimationComplete():void
		{
			isTweening = false;
		}


		private function transitionLeft():void
		{
			index = ( index - 1 < 0 ) ? angles.length - 1 : index - 1;
			rotateY -=  angle_inc;

			// update last material
			updateForTransition();

		}

		private function transitionRight():void
		{
			index = ( index + 1 >= angles.length ) ? 0 : index + 1;
			rotateY +=  angle_inc;

			updateForTransition();
		}

		private function updateForTransition():void
		{
			if (currentMaterial != null)
			{
				lastMaterial = currentMaterial;
			}

			// reset last material
			if (lastMaterial != null && isActivated)
			{
				transitionClose();
			}

			// update currentID
			currentMaterial = items[index];
			currentID = ids[index];

			// unfocus all but the currentMaterial
			focusItems();

			if (isZoomed)
			{
				transitionZoomOut();
			}

			transition();
		}

		private function keyHandler( e:KeyboardEvent ):void
		{
			if ( !isDisabled )
			{
				
				if (e.keyCode == 39 && ! isZooming && ! isTopLevel)
				{
					// back
					index = ( index - 1 < 0 ) ? angles.length - 1 : index - 1;
					rotateY -=  angle_inc;
				}
	
				if (e.keyCode == 37 && ! isZooming && ! isTopLevel)
				{
					// forward
					index = ( index + 1 >= angles.length ) ? 0 : index + 1;
					rotateY +=  angle_inc;
				}
	
				if (e.keyCode == 37 || e.keyCode == 39 && ! isZooming && ! isTopLevel)
				{
					// update last material
					if (currentMaterial != null)
					{
						lastMaterial = currentMaterial;
					}
	
					// reset last material
					if (lastMaterial != null && isActivated)
					{
						transitionClose();
					}
	
					// update currentID
					currentMaterial = items[index];
					currentID = ids[index];
	
					// unfocus all but the currentMaterial
					focusItems();
	
					if (isZoomed)
					{
						transitionZoomOut();
					}
	
					transition();
				}
	
				if (e.keyCode == 49)
				{
					if (! isTopLevel)
					{
						startZoomIn();
					}
				}
	
				if (e.keyCode == 50)
				{
					if (! isTopLevel)
					{
						transitionClose();
						//startZoomOut();
					}
				}
	
				// Keyboard UP, Windows 7 Key 34
				if (e.keyCode == 38 || e.keyCode == 33)
				{
					if (! isTopLevel)
					{
						transitionUp();
					}
					if (isActivated)
					{
						transitionClose();
					}
					if (isZoomed)
					{
						transitionZoomOut();
					}
				}
	
				// Keyboard DOWN, Windows 7 Key 33
				if (e.keyCode == 40 || e.keyCode == 34)
				{
					if (isTopLevel)
					{
						transitionDown();
					}
				}
			}
		}

		private function transitionDown():void
		{
			isTweening = true;

			TweenLite.to( cube, 25, { y: 0, useFrames: true, onComplete: transitionDownComplete } );
			TweenLite.to( face, 25, { y: 1000, useFrames: true } );
			//TweenLite.to( camera, 25, { z: cameraPosZ, useFrames: true } );
		}

		private function transitionDownComplete():void
		{
			isTopLevel = false;
			isTweening = false;

			face.visible = false;
		}

		private function transitionUp():void
		{
			isTweening = true;
			face.visible = true;

			TweenLite.to( cube, 25, { y: startPositionY, useFrames: true, onComplete: transitionUpComplete } );
			TweenLite.to( face, 35, { y: 0, useFrames: true } );
			//TweenLite.to( camera, 25, { z: 0, useFrames: true } );
		}

		private function transitionUpComplete():void
		{
			isTopLevel = true;
			isTweening = false;
		}

		private function focusItems():void
		{
			for (var i in items)
			{
				if (items[i] != currentMaterial)
				{
					items[i].alpha = 0.5;
				}

				currentMaterial.alpha = 1;
			}
		}

		private function transitionPanEnd():void
		{
			TweenLite.to( cube, 15, { rotationY: newRotateY, useFrames: true } );
			TweenLite.to( camera, 15, { y: 0, useFrames: true, onComplete: transitionPanComplete } );
		}

		private function transitionPanComplete():void
		{
			isPanning = false;
			face.visible = true;
		}

		private function transitionOpen():void
		{
			isActivated = true;
			currentMaterial.gotoAndPlay('open');
			//transitionZoomIn();
		}

		private function transitionClose():void
		{
			isActivated = false;
			currentMaterial.gotoAndPlay('close');
			//transitionZoomOut();
		}

		private function transition():void
		{
			isTweening = true;
			TweenLite.to( this, transitionDuration, { newRotateY: rotateY, useFrames: true, onComplete: transitionComplete, ease: tweenType } );

			// set quality to low
			if ( stage ) stage.quality = StageQuality.LOW;

			// disable smooth
			for (var i in items)
			{
				items[i].smooth = false;
			}
		}

		private function transitionComplete():void
		{
			isTweening = false;

			// set quality to high
			if ( stage ) stage.quality = StageQuality.HIGH;

			// disable smooth
			for (var i in items)
			{
				items[i].smooth = true;
			}
		}

		private function transitionZoomIn():void
		{
			isZooming = true;
			TweenLite.to( camera, zoomInDuration, { zoom: cameraZoomMax, useFrames: true, onComplete: transitionZoomInComplete, ease: tweenType } );
		}

		private function transitionZoomOut():void
		{
			isZooming = true;
			TweenLite.to( camera, zoomOutDuration, { zoom: cameraZoomMin, useFrames: true, onComplete: transitionZoomOutComplete, ease: tweenType } );
		}

		private function transitionZoomInComplete():void
		{
			isZooming = false;
			isZoomed = true;

			// load the required id
			//scope.loadTemplate( currentID );
		}
		
		private function transitionZoomOutComplete():void
		{
			isZooming = false;
			isZoomed = false;
		}

		private function createMaterialsList():void
		{
			materialsList = new MaterialsList();

			light = new PointLight3D();

			// library movieclip based materials
			materialsList.addMaterial( createLibraryMaterial( 'frontAsset' ), "front" );
			materialsList.addMaterial( createLibraryMaterial( 'leftAsset' ), "left" );
			materialsList.addMaterial( createLibraryMaterial( 'backAsset' ), "back" );
			materialsList.addMaterial( createLibraryMaterial( 'rightAsset' ), "right" );

			// transparent top / bottom materials;
			materialsList.addMaterial( createDefaultMaterial(), "bottom" );
			materialsList.addMaterial( createDefaultMaterial(), "top" );
		}

		private function createLibraryMaterial( id:String ):MaterialObject3D
		{
			var material:MovieAssetMaterial = new MovieAssetMaterial('cubeAsset',true,true,true);

			material.interactive = true;
			material.name = id;
			material.smooth = true;
			
			// set id by getting instance of MovieClipMaterial base class
			var movieClip:MovieClipMaterial = material.movie as MovieClipMaterial;
			//movieClip.init( id, this );
			movieClip.init( id, this );
			
			// add to items
			items.push( movieClip );

			return material;
		}
		
		/* This function delegates the loading of the template to the top level 'LobbyView' class
		   If the current ID does not match the facing asset we see, it is not loaded */
		public function loadTemplate( id:String ):void
		{
			if ( currentID == id )
			{
				removeHandlers();
				scope.loadTemplate( id );
				startZoomIn();
			}			
		}

		private function createDefaultMaterial():MaterialObject3D
		{
			var material:ColorMaterial = new ColorMaterial(0xFFFFFF,0,true);
			return material;
		}

		private function createFaceMaterial():MaterialObject3D
		{
			var material:MovieAssetMaterial = new MovieAssetMaterial('faceAsset',true,true,true);
			material.interactive = false;
			material.name = 'faceAsset';
			material.smooth = true;
			material.movie.blendMode = BlendMode.MULTIPLY;

			return material;
		}

		override protected function onRenderTick(event:Event=null):void
		{
			// get current x/y speeds
			// x 
			updateSpeed();
			//updateBlur();

			// update cube rotation
			if (! isPanning)
			{
				cube.rotationY = newRotateY;
			}
			else
			{
				lastRotateY = cube.rotationY;
			}

			// update render
			renderer.renderScene(scene, camera, viewport);
		}

		private function updateSpeed():void
		{
			//newRotateY
			if (! isPanning)
			{
				currentSpeedX = Math.abs(cube.rotationY - newRotateY);
			}
			else
			{
				currentSpeedX = Math.abs(cube.rotationY - lastRotateY);
			}
		}

		private function updateBlur():void
		{
			if (! isTopLevel)
			{
				// Apply Blur
				var blurAmountX:Number = Math.floor( ( currentSpeedX / 20 ) * 10 );
				viewport.containerSprite.filters = ( currentSpeedX <= 3 ) ? [] : [ new BlurFilter( blurAmountX, 0, 1 ) ];
			}
			else
			{
				viewport.containerSprite.filters = [];
			}
		}
	}
}