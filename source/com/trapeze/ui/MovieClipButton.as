﻿package com.trapeze.ui {
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	
	import flash.events.MouseEvent;
	
	/**
	 * Provides functionality to turn a MovieClip into a button.
	 * To use, create a button in your FLA library with frames labelled "up", "over", "down", and
	 * "disabled". Export the symbol for ActionScript and set its Base Class to be this file
	 * (com.trapeze.ui.MovieClipButton).
	 */
	public class MovieClipButton extends MovieClip {
		
		private var _label:DisplayObject;
		/**
		 * The label of the button. Could be a TextField or another MovieClip or a Loader
		 */
		public function get label():DisplayObject {
			
			return _label;
		}
		public function set label(newLabel:DisplayObject):void {
			
			if (_label) {
				removeChild(_label);
			}
			
			newLabel.cacheAsBitmap = true;
			
			newLabel.x = -newLabel.width/2;
			newLabel.y = -newLabel.height/2;
			
			_label = newLabel;

			addChild(_label);
			
		}
		
		
		/**
		 * Sets the disabled state of the button. False by default. Enabling this property causes
		 * the button to no longer respond to MouseEvents and goes to the "disabled" label
		 * on the timeline
		 */
		public function get disabled():Boolean {
			
			return !mouseChildren;
		}
		public function set disabled(value:Boolean):void {
			
			mouseChildren = mouseEnabled = !value;
			
			if (value) {
				removeEventListener(MouseEvent.ROLL_OVER, over);
				removeEventListener(MouseEvent.ROLL_OUT, out);
				removeEventListener(MouseEvent.MOUSE_DOWN, down);
				removeEventListener(MouseEvent.MOUSE_UP, up);
				
				gotoAndStop("disabled");
			}
			else {
				addEventListener(MouseEvent.ROLL_OVER, over);
				addEventListener(MouseEvent.ROLL_OUT, out);
				addEventListener(MouseEvent.MOUSE_DOWN, down);
				addEventListener(MouseEvent.MOUSE_UP, up);
				
				gotoAndStop("up");
			}
		}
		
		
		public function MovieClipButton() {
			
			stop();
			buttonMode = true;
			disabled = false;
		}
		
		
		/**
		 * Handles ROLL_OVER events.
		 * Goes to the "over" framelabel on the timeline.
		 * If the label is a MovieClip, it goes to the "over" framelabel on its timeline.
		 * @param	event
		 */
		protected function over(event:MouseEvent):void {
			
			gotoAndStop("over");
			
			if (label) {
				setChildIndex(label, numChildren-1);
				if (label is MovieClip) this["label"].gotoAndStop("over");
			}
		}
		
		
		/**
		 *  Handles ROLL_OUT events.
		 * Goes to the "up" framelabel on the timeline.
		 * If the label is a MovieClip, it goes to the "up" framelabel on its timeline.
		 * @param	event
		 */
		protected function out(event:MouseEvent):void {
			
			gotoAndStop("up");
			if (label) {
				setChildIndex(label, numChildren-1);
				if (label is MovieClip) this["label"].gotoAndStop("up");
			}
		}
		
		
		/**
		 * Handles MOUSE_DOWN events.
		 * Goes to the "down" framelabel on the timeline.
		 * If the label is a MovieClip, it goes to the "down" framelabel on its timeline.
		 * @param	event
		 */
		protected function down(event:MouseEvent):void {
			
			gotoAndStop("down");
			if (label) {
				setChildIndex(label, numChildren-1);
				if (label is MovieClip) this["label"].gotoAndStop("down");
			}
		}
		
		
		/**
		 * Handles MOUSE_UP events.
		 * Goes to the "up" framelabel on the timeline.
		 * If the label is a MovieClip, it goes to the "up" framelabel on its timeline.
		 * @param	event
		 */
		protected function up(event:MouseEvent):void {
			
			gotoAndStop("over");
			if (label) {
				setChildIndex(label, numChildren-1);
				if (label is MovieClip) this["label"].gotoAndStop("over");
			}
		}
	}
}