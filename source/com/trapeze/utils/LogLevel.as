﻿package com.trapeze.utils {
	
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class LogLevel {
		
		public static const NONE:int		= 0;
		public static const ERROR:int		= 1;
		public static const WARN:int		= 2;
		public static const INFO:int		= 3;
	}
}