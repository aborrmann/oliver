﻿package com.trapeze.utils {
	
	import flash.external.ExternalInterface;
	
	/**
	* A debugging class that traces error messages both in Flash and to a browser's console (if supported)
	*
	* @usage Log.info("This is a message for your information only");
	*
	*/
	public class Log {
		
		
		private static var _level:int = -1;
		/**
		 * Variable that controls the type of messages to output
		*/
		public static function get level():int {
			
			if (_level == -1) {
				_level = LogLevel.ERROR;
			}
			return _level;
		}
		public static function set level(value:int):void {
			
			if (
				value == LogLevel.NONE	||
				value == LogLevel.INFO	||
				value == LogLevel.WARN	||
				value == LogLevel.ERROR
			) {
				_level = value;
			}
		}
		
		/**
		 * Ouputs an error message prefixed by four spaces
		 * @param	message
		 */
		public static function error(message:Object):void {
			
			if (level >= LogLevel.ERROR) {
				write("    " + message);
			}
		}
		
		
		/**
		 * Outputs a warning message prefixed by two spaces
		 * @param	message
		 */
		public static function warn(message:Object):void {
			
			if (level >= LogLevel.WARN) {
				write("  " + message);
			}
		}
		
		/**
		 * Outputs an info message
		 * @param	message
		 */
		public static function info(message:Object):void {
			
			if (level >= LogLevel.INFO) {
				write(message);
			}
		}
		
		
		/**
		* Outputs a message to Flash or the console panel of a browser
		*/
		public static function write(message:Object):void {
			
			trace(message);
			
			// Try calling the Javascript console in Firebug, Chrome, and Safari
			// If no console is available, don't worry about it
			try {
				ExternalInterface.call("console.log", message);
			}
			catch (error:Error) { }
		}
		
		
	}
}