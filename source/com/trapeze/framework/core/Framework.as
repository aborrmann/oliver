﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.core {
    
    import com.trapeze.framework.events.LocaleEvent;
    import com.trapeze.framework.managers.CopyManager;
    import com.trapeze.framework.managers.FontManager;
    import com.trapeze.framework.managers.FrameworkManager;
    import com.trapeze.framework.managers.LoadManager;
    import com.trapeze.framework.managers.LocaleManager;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.StyleManager;
    import com.trapeze.framework.managers.TemplateManager;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.LogLevel;
    
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.system.Capabilities;

	/** Core framework object used to load essential assets including Preferences, Stylesheet, and CopyStrings, and Locale
	* @param
	* @return
	* @members
	**/
	public class Framework {
        
		/* private init - get's defined on first creation of framework */
		private var init:Boolean = false;
        
        private var supportAssetsCount:int = 0;
        private var supportAssetsLoaded:int = 0;
        
        
		/** Constructor
		* @param	displayObject:MovieClip that spawned the Framework object
		* @return
		**/
		public function Framework(displayObject:MovieClip = null):void {
            
            FrameworkManager.client = displayObject;
        }
		
        
		/** Begins sequence of loading preferences, locales, stylesheets, copy, and fonts files **/
		public function load():void {
            
			// Check for the debug flashvar
			if (FrameworkManager.client.loaderInfo.parameters.debug) {
				Log.level = FrameworkManager.client.loaderInfo.parameters.debug;
			}
			else if (Capabilities.playerType == "External" || Capabilities.playerType == "StandAlone") {
				Log.level = LogLevel.INFO;
			}
			Log.write("log level: " + Log.level);
			
			// fire our frameworkManager event
			FrameworkManager.dispatchEvent( new Event( Event.INIT ) );
		
			/* PrefsManager setup */
			// check for preferences path, if not, default
			var prefsPath:String;
			if ( FrameworkManager.flashvars && FrameworkManager.flashvars.prefsPath ) {
				
				prefsPath = FrameworkManager.flashvars.prefsPath;
			}
			else {
				
				var swfFileName:String = FrameworkManager.client.loaderInfo.url;
				if ( swfFileName )
				{
					var swfFilePath:String = swfFileName.slice(0, swfFileName.lastIndexOf("/") + 1);
					swfFileName = swfFileName.substring(swfFileName.lastIndexOf("/") + 1, swfFileName.indexOf(".swf"));
				}
				
				// ToDo: CHANGE THIS BACK!
				//prefsPath = "../xml/" + swfFileName + "-preferences.xml";
				prefsPath = "../xml/oliver-preferences.xml";
			}
                
			Log.info("Preferences URL: " + prefsPath);
            LoadManager.load("preferences", prefsPath, onPreferencesLoad, "xml");
		}
        
        
		/** Sets locale into the LocaleManager **/
		private function loadLocale():void {
            
			// setup localeManager, since required from this point to have fully qualified locale paths
			var localePath:String = ( FrameworkManager.flashvars.locale == null ) ? PrefsManager.xml.locales.attribute('default') : FrameworkManager.flashvars.locale;
			Log.info("Locale: " + localePath);
			LocaleManager.locale = localePath;
			LocaleManager.addEventListener( LocaleEvent.CHANGE, loadSupportingAssets );
		}
        
        
        private function loadSupportingAssets( e:Event = null):void {
            
            LoadManager.remove("stylesheet");
            LoadManager.remove("copy");
            LoadManager.remove("fonts");
            
			var stylesPath:String = PrefsManager.getValue('styleSheetFile');
			if (stylesPath) {
                supportAssetsCount++;
                stylesPath = FrameworkManager.preparePath(stylesPath);
				Log.info("Stylesheet Path: " + stylesPath);
                LoadManager.load("stylesheet", stylesPath, onStylesLoad, "css");
            }
            
			var copyStringsPath:String = PrefsManager.getValue('copyStringsFile');
			if (copyStringsPath) {
                supportAssetsCount++;
                copyStringsPath = FrameworkManager.preparePath(copyStringsPath);
				Log.info("Copy Strings Path: " + copyStringsPath);
                LoadManager.load("copy", copyStringsPath, onCopyLoad, "xml");
            }
            
			var fontsPath:String = PrefsManager.getValue('fontsFile');
			if (fontsPath) {
                supportAssetsCount++;
                fontsPath = FrameworkManager.preparePath(fontsPath);
				Log.info("Fonts Path: " + fontsPath);
                LoadManager.load("fonts", fontsPath, onFontsLoad, "swf");
            }
            
            if (supportAssetsCount == supportAssetsLoaded) complete();
        }
        
        
		/** complete - Framework is spawned, loading of framework assets and init of all managers complete.
		*	FrameworkManager dispatches out to BaseTemplate / BaseFramework listeners
		* @param	e:Event - the flash.events.Event dispatched
		* @return
		**/
		private function complete():void {
            
			// dispatch out
			FrameworkManager.dispatchEvent( new Event( Event.COMPLETE ) );

			/*  This is required to be init'ed here because, once the framework is loaded, all the various
				listeners have been applied, and ready for any loaded templates. Therefore, once the navigation
				is loaded, the framework is ready to go, and SWF Address can init properly
			*/
			if ( !init ) {
				init = true;
				TemplateManager.init();
			}
		}
        
        
        private function onPreferencesLoad(preferences:XML):void {
            
			PrefsManager.preferences = preferences;
			FrameworkManager.mediaURL = PrefsManager.getValue("mediaURL");
			loadLocale();
			loadSupportingAssets();
        }
        
        
        private function onStylesLoad(content:*):void {
            StyleManager.css = content;
            supportAssetsLoaded++;
            if (supportAssetsCount == supportAssetsLoaded) complete();
        }
        
        
        private function onCopyLoad(copy:XML):void {
            CopyManager.copy = copy;
            supportAssetsLoaded++;
            if (supportAssetsCount == supportAssetsLoaded) complete();
        }
        
        
        private function onFontsLoad(fonts:*):void {
            FontManager.fonts = LoadManager.getLoader("fonts");
            supportAssetsLoaded++;
            if (supportAssetsCount == supportAssetsLoaded) complete();
        }
	}
}