﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.core {
    
    import com.trapeze.framework.core.Framework;
    import com.trapeze.framework.events.PreloadEvent;
    import com.trapeze.framework.managers.FrameworkManager;
    import com.trapeze.framework.managers.NavigationManager;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.PreloadManager;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.LogLevel;
    
    import flash.display.LoaderInfo;
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.ProgressEvent;
    import flash.system.Capabilities;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
	
	
	/** The base MovieClip that spawns of the framework object. This class is the starting point for the Framework. An application's base code should extend this class. **/
	public class BaseFramework extends MovieClip {
        
		/* Adds the various listeners required for extended base templates of this class. */
		function BaseFramework() {
			
            // If we are running the project in debug mode or in the Flash IDE, use the most verbose output
			if (Capabilities.playerType == "External" || Capabilities.playerType ==  "StandAlone") {
				Log.level = LogLevel.INFO;
			}
            
            var contextMenu:ContextMenu = new ContextMenu();
            contextMenu.hideBuiltInItems();
            contextMenu.customItems = [new ContextMenuItem("Built by Trapeze", true, false)];
            this.contextMenu = contextMenu;
            
            FrameworkManager.flashvars = LoaderInfo(root.loaderInfo).parameters;
            
			// set listeners
			FrameworkManager.addEventListener( Event.INIT, onFrameworkInit );
			FrameworkManager.addEventListener( Event.COMPLETE, onBaseFrameworkComplete );
            PreloadManager.addEventListener(PreloadEvent.START, onPreloadStart);
            PreloadManager.addEventListener(PreloadEvent.FINISH, onPreloadFinish);
            PreloadManager.addEventListener(ProgressEvent.PROGRESS, onPreloadProgress);
            
			/***
			*	 Let's check to see if we already have a framework spawned, which is referenced in 'client' in
			*    our FrameworkManager, if not, let's create a new Framework object and pass a DisplayObject
			*    reference of 'this' to be shared with the Framework Manager classes
			***/
			
			var client:MovieClip = FrameworkManager.client;
			if ( client == null ) {
				var framework:Framework = new Framework( this );
				framework.load();
			}
            else {
				/* Otherwise, let's process the 'onFrameworkComplete' dispatch event which would be dispatched from the
				*  Framework Manager class once the Framework has loaded all it's components
				*/
				onBaseFrameworkComplete( null );
			}
		}
        
        
		/** Removes all listeners set in the constructor. This is called automatically by the TemplateManager when unloading. **/
		public function destroy():void {
            
			FrameworkManager.removeEventListener( Event.INIT, onFrameworkInit );
			FrameworkManager.removeEventListener( Event.COMPLETE, onBaseFrameworkComplete );
		}
		
        
		/** Method that responds to framework initialization
		* @param event The INIT event that triggered this function
		**/
		public function onFrameworkInit( event:Event ):void {}
		
        
		/** Method that responds to framework load. For most applications, this is the starting point of the framework as it is the first point where every part of the Framework is ready to be used.
		* @param event:Event - The COMPLETE event that triggered this function
		**/
		public function onFrameworkComplete( event:Event ):void {}
		
        
		/** Method that plays a custom entrance animation when the template is first loaded. This method is empty, so each template must override it and provide a custom animation. This method must call <code>TransitionManager.transitionedIn()</code> when its animation completes in order to signal that it is ready for interaction
         * @see com.trapeze.managers.TransitionManager#transitionedIn()
         **/
		public function transitionIn():void {}
		
        
		/** Method that plays a custom exit animation as soon as a new template has begun loading. This method is empty, so each template must override it and provide a custom animation. This method must call <code>TransitionManager.transitionedOut()</code> when its animation completes in order to signal that it is ready for removal
         * <p>Note that <code>TransitionManager.transitionedOut()</code> should only ever be called once during a transition. So if you have an animation implemented in the  <code>transitionOutPostLoad</code> function, you should not call that function here.</p>
         * @see com.trapeze.managers.TransitionManager#transitionedOut()
		**/
		public function transitionOutPreLoad():void { }
		
        
		/** Plays a custom exit animation after a new template has finished loading. The animation runs concurrently with the new template's <code>transitionIn()</code> animation. This method is empty, so each template must override it and provide a custom animation. This method must call <code>TransitionManager.transitionedOut()</code> when its animation completes in order to signal that it is ready for removal.
         * <p>Note that if you have an animation in <code>transitionOutPreLoad</code>, you must decide if you will call <code>TransitionManager.transitionedOut()</code> there or in this function. <code>TransitionManager.transitionedOut()</code> should only ever be called once during a transition.</p>
         * @see com.trapeze.managers.TransitionManager#transitionedOut()
		**/
		public function transitionOutPostLoad():void { }
        
        
        private function onBaseFrameworkComplete(e:Event = null):void {
            
			if (!NavigationManager.navigationClip && NavigationManager.container && PrefsManager.getValue("Navigation")) {
				NavigationManager.load(PrefsManager.getValue("Navigation"));
			}
            
            onFrameworkComplete(new Event(Event.COMPLETE));
        }
        
        
        /** Responds to a Preload Finish event. This function should turn off the preloader. **/
        protected function onPreloadFinish(e:PreloadEvent):void {
            
            if (PreloadManager.container) PreloadManager.container.visible = false;
        }
        
        
        /** Responds to a Preload Start event. This function should turn on the preloader. **/
        protected function onPreloadStart(e:PreloadEvent):void {
            
            if (PreloadManager.container) PreloadManager.container.visible = true;
        }
        
        
        /** Reports on the Preload progress. **/
        protected function onPreloadProgress(e:ProgressEvent):void {
            
        }
	}
}