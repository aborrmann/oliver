﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers
{
	import flash.events.EventDispatcher;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.display.Loader;
	import flash.display.DisplayObject;
	import flash.net.URLRequest;
	
	/** Generic loading class
	* @private
	**/
		
	public class DisplayLoader extends EventDispatcher
	{
		/** @private The DisplayObject assoicated with this DisplayLoader **/
		private var data:DisplayObject;
		
		/** @private The loader object used to load this DisplayObject **/
		private var dataLoader:Loader;
		
		/** @private The bytesLoaded of the current template loading **/
		public static var bytesLoaded:Number;
		
		/** @private The bytesTotal of the current template **/
		public static var bytesTotal:Number;
		
		/* Returns our data loader for this Manager instance
		* @param
		* @return	dataLoader:URLLoader;
		**/
		public function get loader():Loader { return dataLoader; }
		
		/** Loads requested file into our data loader
		* @param	dataPath:String
		* @return
		**/
		public function load( dataPath:String ):void
		{
			dataLoader = new Loader();
            PreloadManager.register(dataLoader.contentLoaderInfo);
			dataLoader.load( new URLRequest( dataPath ) );
			dataLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, onLoad );
			//dataLoader.contentLoaderInfo.addEventListener( ProgressEvent.PROGRESS, onProgress );
		}
		
		/** Dispatches our complete event once data has been loaded
		* @param
		* @return
		**/
		public function onLoad( e:Event ):void
		{
			data = dataLoader;
			dispatchEvent( new Event( Event.COMPLETE ) );
		}
		
		/** Dispatches when our progress is changed on our dataLoader
		* @param
		* @return
		**/
		private function onProgress( e:Event ):void
		{
			bytesLoaded = dataLoader.contentLoaderInfo.bytesLoaded;
			bytesTotal = dataLoader.contentLoaderInfo.bytesTotal;
			
			// dispatch to listeners
			dispatchEvent( new ProgressEvent( ProgressEvent.PROGRESS ) );
		}
	}
}