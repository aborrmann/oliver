﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.utils.Log;
    import flash.text.StyleSheet;
	
	/** Provides CSS definitions for TextFields. The StyleManager holds a reference to a stylesheet that, when applied to a TextField, will format the text specified by the CSS in the stylesheet. The StyleManager is initialized on framework load and looks to the PrefsManager to find its CSS stylesheet. It will automatically load the node specified by the name attribute of <code>styleSheetFile</code>. The stylesheet path is either absolute or appended to the media URL of the <code>PrefsManager</code>.
     * 
     * <listing version="3.0">
     * &lt;file name="stylesheetFile"&gt;css/project-stylesheet.css&lt;/file&gt;
     * </listing>
     * 
     * <p>Instead of manually applying a stylesheet to a TextField, using the <code>TextLine</code> or <code>Paragraph</code> class will automatically apply the stylesheet as well as provide other automations for displaying text. It is highly recommended to use one of these classes instead of a simple TextField.</p>
     * 
     * @see com.trapeze.framework.managers.FontManager
     * @see com.trapeze.text.TextLine
     * @see com.trapeze.text.Paragraph
    **/
	public class StyleManager {
        
        /** Sets and/or replaces the current Stylesheet with the new value. The value must be the content of a valid CSS file.
         * <p>Normally this property is automatically set by the framework on initialization, but in unusual circumstances, it can be set manually here.</p>
        **/
        public static function set css(value:String):void {
            styleSheet.parseCSS(value);
        }
        
        
		/** The currently loaded stylesheet.
		* <p>This stylesheet can be set to the <code>stylesheet</code> property of TextFields</p>
		**/
		public static function get styleSheet():StyleSheet {
            
			if (_styleSheet) return _styleSheet;
			else Log.error("Error: Cannot access CSS styles because the stylesheet has not been loaded.");
            return null;
		}
		private static var _styleSheet:StyleSheet = new StyleSheet();
	}
}