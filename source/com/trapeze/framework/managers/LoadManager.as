﻿package com.trapeze.framework.managers {
    
    import com.trapeze.framework.core.BaseFramework;
    import com.trapeze.framework.managers.PreloadManager;
    import com.trapeze.utils.Log;
    import flash.display.LoaderInfo;
    import flash.system.LoaderContext;
    
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.media.Sound;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    
    
	/** Handles loading of any external asset. **/
    public class LoadManager {
        
        
        /* ====== Static properites and methods for interfacing with other classes ======= */
        
        public static var bytesTotal:int = 0;
        
        public static function get active():Boolean {
            return instance.active;
        }
        
        
        // The average download speed of all files loaded so far, measured in bytes per second
        public static function get speed():Number {
            var count:Number = 0;
            
            if (instance.recordedSpeeds.length == 0) return count;
            
            for (var index:* in instance.recordedSpeeds) {
                count += instance.recordedSpeeds[index];
            }
            return count / instance.recordedSpeeds.length;
        }
        
        public static function get assets():Vector.<*> {
        
            return instance.assets;
        }
        
        
        public static function get percentLoaded():int {
            
            if (bytesTotal == 0) {
                Log.warn("Could not get percent loaded because the totalBytes variable has not been set.");
                return bytesTotal;
            }
            else {
            
                var number:int = 0;
                for (var index:* in _instance.assets) {
                    number += _instance.assets[index].contentLoaderInfo.bytesLoaded;
                }
                return Math.round((number / bytesTotal) * 100);
            }
        }
        
        
        public static function load(id:String, url:String, callback:Function, fileType:String = null):void {
            
            if (!url) {
                Log.error("Error loading asset " + id + ". No URL was provided");
                return;
            }
            
            if (retrieve(id)) {
                Log.warn("Duplicate IDs loaded in the LoadManager. Ignoring duplicate id " + id);
                return;
            }
            
            var asset:*;
            
            if (!fileType) fileType = url.substring(url.lastIndexOf(".")+1).toLowerCase();
            switch (fileType) {
                
                case "jpg":
                case "png":
                case "gif":
                case "swf":
                    asset = new Loader();
                    PreloadManager.register(asset.contentLoaderInfo);
                    break;
                    
                case "mp3":
                    asset = new Sound();
                    PreloadManager.register(asset);
                    break;
                
                default:
                    asset = new URLLoader();
                    PreloadManager.register(asset);
                    break;
            }
            
            
            instance.names.push(id);
            instance.urls.push(url);
            instance.assets.push(asset);
            instance.fileTypes.push(fileType);
            instance.callbacks.push(callback);
            
            if (!active) startLoading();
        }
        
        
        public static function retrieve(name:String):* {
            
            for (var index:* in instance.names) {
                if (instance.names[index] == name) {
                    
                    return getLoadedContent(instance.assets[index]);
                }
            }
			return null;
        }
        
        
        public static function remove(name:String):void {
            
            for (var index:* in instance.names) {
                if (instance.names[index] == name) {
                    
                    if (instance.assets[index] is Loader) Loader(instance.assets[index]).unloadAndStop();
                    instance.names.splice(index, 1);
                    instance.urls.splice(index, 1);
                    instance.assets.splice(index, 1);
                    instance.fileTypes.splice(index, 1);
                    instance.callbacks.splice(index, 1);
                    instance.marker--;
                    return;
                }
            }
        }
        
        
        public static function getLoader(name:String):* {
            
            for (var index:* in instance.names) {
                if (instance.names[index] == name) {
                    return instance.assets[index];
                }
            }
        }
        
        
        private static function startLoading():void {
            
            instance.active = true;
            instance.loadStartTime = new Date().valueOf();
            instance.loadStartMarker = instance.marker;
            instance.loadNextAsset();
        }
        
        
        private static function getLoadedContent(loader:*):* {
            
            if (loader is Loader) return Loader(loader).content;
            if (loader is LoaderInfo) {
				return Loader(loader.loader).content;
			}
            
            try {
                return XML(loader.data);
            }
            catch (error:*) { }
            
            if (loader is URLLoader) return URLLoader(loader).data;
            
            return loader;
        }
        
        
        
        /* ===================== Internal methods and classes to facilitate Asset Loading ====================== */
        
        public var names:Vector.<String> = new Vector.<String>();
        public var urls:Vector.<String> = new Vector.<String>();
        public var assets:Vector.<*> = new Vector.<*>();
        public var fileTypes:Vector.<String> = new Vector.<String>();
        public var callbacks:Vector.<Function> = new Vector.<Function>();
        
        public var marker:int = -1;
        public var loadStartTime:Number;
        public var loadStartMarker:int;
        public var speed:Number;
        public var active:Boolean = false;
        
        private var recordedSpeeds:Vector.<Number> = new Vector.<Number>();
        
        public function LoadManager() {}
        
        
        public function loadNextAsset():void {
            
            if (marker + 1 >= assets.length) {
                active = false;
                Log.info("Loaded " + (marker - loadStartMarker) + " files (" + PreloadManager.bytesLoaded + " bytes) in " + (new Date().valueOf() - loadStartTime) + " milliseconds");
                return;
            }
            
            marker++;
            
            var asset:* = assets[marker];
            
            var eventListener:Object;
            if (asset is Loader) {                
                eventListener = asset.contentLoaderInfo;
            }
            else {
                eventListener = asset;
            }
            
            eventListener.addEventListener(Event.COMPLETE, onComplete);
            eventListener.addEventListener(IOErrorEvent.IO_ERROR, onError);
            
            var request:URLRequest = new URLRequest(urls[marker]);
            if (asset is Loader) {
                var context:LoaderContext = new LoaderContext(true);
                Loader(asset).load(request, context);
            }
            else {
                asset.load(request);
            }
        }
        
        
        private function onComplete(e:Event):void {
            
            EventDispatcher(e.target).removeEventListener(Event.COMPLETE, onComplete);
            EventDispatcher(e.target).removeEventListener(IOErrorEvent.IO_ERROR, onError);
            
            // update download speed
            var thisSpeed:Number = (PreloadManager.bytesLoaded * 1000) / (new Date().valueOf() - loadStartTime);
            
            if (thisSpeed > 0) recordedSpeeds.push(thisSpeed);
            
            if (callbacks[marker]) {
                var content:* = getLoadedContent(e.target);
                try {
                    //content = BaseFramework(content);
                    callbacks[marker].call(null, content);
                }
                catch (error:Error) {
                    Log.error(error.message);
                }
            }
           
            loadNextAsset();
        }
        
        
        private function onError(e:IOErrorEvent):void {
            
            Log.error("LoadManager error: " + e.text);
            EventDispatcher(e.target).removeEventListener(Event.COMPLETE, onComplete);
            loadNextAsset();
        }        
        
        
        /* =============================== Singelton Management Functions ============================== */
		
        
        private static var _instance:LoadManager;
		
		/** Returns a singleton of this manager reference
		* @returns TransitionManager;
		**/
		public static function get instance():LoadManager
		{
			if ( _instance == null ) {
                _instance = new LoadManager();
            }
			return _instance;
		}
    }
}