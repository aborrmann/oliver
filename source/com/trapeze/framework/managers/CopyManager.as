﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
	import com.trapeze.utils.Log;
	
	/** Handles loading an external copy file and retrieving strings from that file.
     * The CopyManager is initialized on framework load and looks to the PrefsManager to find its copy file. It will automatically load the node specified by the name attribute of <code>copyStringsFile</code>.
     * 
     * @example This example shows how to specify and use the CopyManager in a project.
     * <p>The copy file must be specified in the preferences file, identified by the name attribute of <code>copyStringsFile</code>. If this file path is not absolute, it will be  appended to the media URL in the <code>PrefsManager</code>.</p>
     * 
     * <listing version="3.0">
     * &lt;file name="copyStringsFile"&gt;xml/%(locale)/project-copy.xml&lt;/file&gt;
     * </listing>
     * 
     * <p>Assume the copy file has the following content:</p>
     * 
     * <listing version="3.0">
     * &lt;?xml version="1.0" encoding="UTF-8"?&gt;
     * &lt;copyStrings&gt;
     *
     *     &lt;copy name="foobar"&gt;Foo bar&lt;/copy&gt;
     *     &lt;copy name="header"&gt;
     *         &lt;p class="header"&gt;Get 50% off now!&lt;/p&gt;
     *     &lt;/copy&gt;
     *
     * &lt;/copyStrings&gt;
     * </listing>
     * 
     * <p>Copy can be retrieved from the CopyManager quite simply:</p>
     * 
     * <listing version="3.0">
     * var copyString:String = CopyManager.getValue("foobar");
     * trace(copyString);       // outputs "Foo bar"
     * 
     * var textField:TextField = new TextField();
     * textField.text = copyString;     // the TextField now displays text from the CopyManager
     * </listing>
     * 
     * <p>A common use of the CopyManager is to place styled text into a TextField. This works in conjunction with the StyleManager and FontManager.</p>
     * 
     * <p>The TextLine and Paragraph classes provides functionality to use all these managers effortlessly.</p>
     * 
     * <listing version="3.0">
     * var textLine:TextLine = new TextLine();
     * textLine.htmlText = CopyManager.getDecodedValue("header")        // places "Get 50% off now!" into the TextLine and applies the CSS styles defined in the stylesheet
     * </listing>
     * 
     * @see com.trapeze.framework.managers.StyleManager
     * @see com.trapeze.framework.managers.FontManager
	**/
	public class CopyManager {
        
        /** Sets a new copy XML file. Any current copy XML file is replaced with the new XML file. Any TextFields that contain copy from the Manager need to be manually updated with the new copy values **/
        public static function set copy(value:XML):void {
            _copy = value;
        }
        private static var _copy:XML;
        
        
		/** Decodes a piece of copy in the XML identified by <code>key</code>
         * <p>The value is decoded with the decodeURI function. This is particularly useful for retrieving copy that contains a <code>%</code> symbol.</p>
         * <p>In all other respects, the function behaves exactly like <code>getValue()</code></p>
         * @see #getValue()
         * @see http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/package.html#decodeURI()
		**/
		public static function getDecodedValue( key:String, replacementVariables:Object = null ):String {
            
			if (_copy) {
				
				for ( var element:String in _copy.children() ) {
                    
					if ( _copy.children()[element].attribute('name') == key ) {
                        
                        var copy:String = decodeURI( _copy.children()[element] );
                        for (var index:* in replacementVariables) {
                            var re:RegExp = new RegExp("%\\(" + index + "\\)", "g");
                            copy = copy.replace(re, replacementVariables[index]);
                        }
                        return copy;
                    }
				}
				return null;
			}
			else {
				Log.error("Error: Cannot get copy value because the copy file has not been loaded");
				return null;
			}
		}
        
        
		/** Finds a piece of copy in the XML identified by <code>key</code>.
         * <p>If <code>key</code> is not found in the XML file, a null value is returned. If the copy file is not yet loaded, an error is logged and a null value is returned.</p>
         * 
         * @example Assume we have this XML content loaded into the CopyManager for the following examples:
         * <listing version="3.0">
         * &lt;copyStrings&gt;
         *     &lt;copy name="header"&gt;Fantastic Deals on Sale Now!&lt;/copy&gt;
         *     &lt;copy name="body"&gt;You have clicked the button %(numClicks) times&lt;/copy&gt;
         * &lt;/copyStrings&gt;
         * </listing>
         * 
         * @example The following code retrieves the "header" copy from the CopyManager:
         * <listing version="3.0">
         * var string:String = CopyManager.getValue("header");
         * trace(string);           // outputs "Fantastic Deals on Sale Now!"
         * </listing>
         * 
         * @example This code retrieves the "body" copy from the CopyManager and replaces variables with ones specified at runtime
         * <listing version="3.0">
         * var clickCount:int = 4;
         * var string:String = CopyManager.getValue("body", { 'numClicks': clickCount });
         * trace(string);           // outputs "You have clicked the button 4 times"
         * </listing>
         * 
		* @param    key:String
        * @param    replacementVariables An optional object of variables to replace in the copy. The object's property name must match the variable to replace in the XML content.
		* @return   value The copy identified by <code>key</code> or null if no copy was found
		**/
		public static function getValue( key:String, replacementVariables:Object = null ):String {
            
			if (_copy) {
				
				for ( var element:String in _copy.children() ) {
                    
					if ( _copy.children()[element].attribute('name') == key ) {
                        var copy:String = String(_copy.children()[element]);
                        for (var index:* in replacementVariables) {
                            var re:RegExp = new RegExp("%\\(" + index + "\\)", "g");
                            copy = copy.replace(re, replacementVariables[index]);
                        }
                        return copy;
                    }
				}
				return null;
			}
			else {
				Log.error("Error: Cannot get copy value because the copy file has not been loaded");
				return null;
			}
		}
	}
}