﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.templates.BaseNavigation;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.StringUtils;
    import flash.display.Loader;
    import flash.display.MovieClip;
    import flash.events.Event;
	
	/** Handles loading and interaction of navigation SWF file  **/	
	public class NavigationManager {
		
        
		/** The path of the Navigation template currently on stage **/
		private static function get template():String { return _template; }
		private static function set template( template:String ):void { if ( _template != template ) _template = template; }
		static private var _template:String;
		
        
		/** The path of the Navigation template currently on stage **/
		public static function get templatePath():String { return _templatePath; }
		public static function set templatePath( templatePath:String ):void { if ( _templatePath != templatePath ) _templatePath = templatePath; }
		static private var _templatePath:String;
		
        
		/** The DisplayObject that contains the <code>navigationClip</code> **/
		public static function set container( container:MovieClip ):void { _templateContainer = container; }
		public static function get container():MovieClip { return _templateContainer; }
		static private var _templateContainer:MovieClip;
        
        
        /** The custom Navigation Template that displays visual assets.
         * The navigation template is always present on the screen and is not automatically removed or modified at any time.
         **/
        public static var navigationClip:BaseNavigation;
		
        
		/** Loads a BaseNavigation SWF file and sets it as the <code>navigationClip</code>
		* @param filePath The filePath to be loaded
		**/
		public static function load( filePath:String ):void {
            
            if (!(StringUtils.beginsWith(filePath, "http://") || StringUtils.beginsWith(filePath, "https://"))) {
                filePath = PrefsManager.getValue("mediaURL") + filePath;
            }
            
			// Load our navigation template
			Log.info("Navigation template path: " + filePath);
            LoadManager.load("navigation", filePath, onLoadComplete, "swf");
		}
		
        
		/** Called when the template has loaded**/
		private static function onLoadComplete(navigation:BaseNavigation):void {
            
			// add our loaded template into our 'navigationClip'
			if ( container.numChildren > 0 ) container.removeChildAt(0);
			container.addChildAt( navigation, 0 );
            navigationClip = navigation;
		}
	}
}