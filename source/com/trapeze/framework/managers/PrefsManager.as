﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
	
	/** Loads the Preferences file and provides an interface to access it **/
	public class PrefsManager {
        
        public static var xml:XML;
        
        
        /** Sets the preferences file **/
		public static function set preferences( value:XML ):void {
			xml = value;
		}
		
        
		/** Finds the xml element with a <code>name</code> attribute matched by <code>key</code>. If such an element does not exist, a null value is returned.
         * <p>If there is more than one element with the same name, only the first one is returned.</p>
		* @param	key The element you would like returned
		* @return	The content of the requested XML element or null if no element was found.
		**/
		public static function getValue( key:String ):String {
            
			var xmlLength:uint = xml.children().length();
			for ( var node:int = 0; node < xmlLength; node++ )
			{
				for ( var element:String in xml.children()[node].children() )
				{
					if ( xml.children()[node].children()[element].attribute('name') == key ){ return xml.children()[node].children()[element]; }
				}		
			}
			return null;
		}
		
        
		/** Finds an element whose contents match <code>value</code> and returns its <code>name</code> attribute. If such an element does not exist, a null value is returned.
         * <p>This method is not reliable if there is more than one element with the same content as only the first element will be returned.</p>
		* @param	value The element content to search for
		* @return	The name attribute of the element whose content was found or null if no element was found.
		**/
		public static function getKey( value:String ):String {
            
			var xmlLength:uint = xml.children().length();
			for ( var node:int = 0; node < xmlLength; node++ )
			{
				for ( var element:String in xml.children()[node].children() )
				{
					if ( xml.children()[node].children()[element] == value ) return xml.children()[node].children()[element].attribute('name');
				}
			}
			return null;
		}
		
        
		/** Finds an XML node with a <code>name</code> attribute matched by <code>key</code>. If such an element does not exist, and empty XMLList is returned.
		* @param	key:String The desired name of the node you wish to return
		* @return	node:XMLList containing all node elements matching the value request
		**/
		public static function getNode( key:String ):XMLList {
            
			var xmlLength:uint = xml.children().length();
			for ( var node:int = 0; node < xmlLength; node++ )
			{
				if ( xml.children()[node].name().toString() == key )
				{
					return xml.children()[node].children();
				}
			}
			return new XMLList();
		}
	}
}