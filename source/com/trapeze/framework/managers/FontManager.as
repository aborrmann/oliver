﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.utils.Log;
    
    import flash.display.DisplayObject;
    import flash.display.Loader;
    import flash.system.ApplicationDomain;
    import flash.text.Font;
    
    import org.bytearray.explorer.SWFExplorer;
	
	/** Handles load and parsing of fonts file. The fonts file is a SWF file with fonts embedded as library assets. In a Framework project, <code>lib/Fonts.fla</code> contains a starting FLA to place fonts into. The library of this FLA file should include ONLY font symbols and NOT MovieClips, Buttons, Graphics, or any other symbols. Publish this FLA file into your project to create the fonts SWF file.
     * <p>Any fonts used in your project should be embedded in the fonts file instead of in the library or in a TextField. This ensures that all templates have access to the fonts and helps maximize compatibility.</p>
     * @example To create a fonts file, create a new Actionscript 3 FLA file in Flash Professional.
     * 
     * <p>Right click in the library and select "New Font".</p>
     * <img src="examples/font-manager-new-font.jpg" />
     * 
     * <p>Choose the font you want from the list and its corresponding style</p>
     * <img src="examples/font-manager-select-font.jpg" />
     * 
     * <p>Ensure the following steps are completed accurately (note that most of them will be set correctly by default):</p>
     * <ol>
     * <li>Give the font an appropriate name (this property is not used by the Framework, only by the developer to easily identify the font in the library)</li>
     * <li>Uncheck "Bitmap text". The "size" field can be any value as it will not be used without Bitmap text being checked.</li>
     * <li>Check "Export for ActionScript"</li>
     * <li>Check "Export in frame 1".</li>
     * <li>Give the font has a unique class name</li>
     * <li>Ensure the Base class is <code>flash.text.Font</code>.</li>
     * <li>Uncheck "Export for runtime sharing"</li>
     * <li>Click OK</li>
     * </ol>
     * 
     * <p>Repeat these steps for each font you want to embed. After all the fonts are embedded, your library will look something like this:</p>
     * <img src="examples/font-manager-library.jpg" />
     * 
     * <p>Export this FLA to the appropriate path and enter the path as a node in the Preferences XML file.</p>
     * <listing version="3.0">
     * &lt;file name="fontsFile"&gt;flash/project-fonts.swf&lt;/file&gt;
     * </listing>
     * 
     * <p>The Framework will handle the loading and initialization of the fonts file at runtime. It will output a list of fonts that are available to the project. These fonts can be used under <code>font-family</code> in your CSS stylesheet when styling TextFields.</p>
     * 
    **/
	public class FontManager {
        
		/** Parses the SWF file that was loaded, and registers Fonts that are embedded in it. This function is called automatically by the framework and is not normally needed in a project. **/
		public static function set fonts(value:Loader):void {
            var i:int;
            var fonts:Array;
            
            var fontsDomain:ApplicationDomain = value.contentLoaderInfo.applicationDomain;
            
            var explorer:SWFExplorer = new SWFExplorer();
            explorer.parse(value.contentLoaderInfo.bytes);
            
            fonts = explorer.getDefinitions();
            for (i = 0; i < fonts.length; i++) {
                var fontClass:Class = Class(fontsDomain.getDefinition(fonts[i]));
                Font.registerFont(fontClass);
            }
            
            Log.info("\nFonts available in this project:");
            
            fonts = Font.enumerateFonts();
            for (i = 0; i < fonts.length; i++) {
                
                Log.info("  " + Font(fonts[i]).fontName);
            }
            
            Log.info("\n");
		}
	}
}