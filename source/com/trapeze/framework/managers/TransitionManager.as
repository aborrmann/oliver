﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.framework.managers.TemplateManager;
    
	/** Coordinates the transitions between loading / unloading templates **/
	public class TransitionManager {
        
		/** Indicates whether or not the TemplateManager is in the process of switching templates **/
        public static function get inTransition():Boolean { return _inTransition }
		private static var _inTransition:Boolean = false;
		
        
        /** Indicates whether the TemplateManager is transitioning a new template onto the stage **/
        public static function get transitioningIn():Boolean { return _transitioningIn }
		private static var _transitioningIn:Boolean = false;
        
        
        /** Indicates whether the TemplateManager is transitioning out an old template from the stage **/
        public static function get transitioningOut():Boolean { return _transitioningOut }
		private static var _transitioningOut:Boolean = false;
        
        
        /** Indicates whether the TemplateManager has begun loading a new template **/
        public static function get preloading():Boolean { return _preloading }
		private static var _preloading:Boolean = false;
		
        
        /** Called by the TemplateManager to begin transitioning from an old template to a new template
         * @private
         **/
		public static function transition():void {
			
			_inTransition = true;
			_preloading = true;
			_transitioningIn = true;
			
			
			if (TemplateManager.currentTemplate) {
				_transitioningOut = true;
				TemplateManager.currentTemplate.transitionOutPreLoad();
			}
		}
		
		
        /** Called by the TemplateManager once a new template has fully loaded
         * @private
        **/
		public static function preloaded():void {
			
			if (transitioningOut) {
				TemplateManager.currentTemplate.transitionOutPostLoad();
			}
			
			TemplateManager.upcomingTemplate.transitionIn();
		}
		
		
        /** Indicates that a new template has fully transitioned in. All templates must call this function after they have finished their entrance animations and are ready for interaction **/
		public static function transitionedIn():void {
			
			_transitioningIn = false;
			if (!transitioningOut) {
				finishTransition();
			}
		}
		
		
        /** Indicates that an old template has fully transitioned out. All templates must call this function after they have finished their exit animations **/
		public static function transitionedOut():void {
			
			_transitioningOut = false;
			TemplateManager.removeOldTemplate();
			if (!transitioningIn) {
				finishTransition();
			}
		}
		
		
        /** Called automatically once both the old template has transitioned out and the new template has transitioned in **/
		private static function finishTransition():void {
			
			TemplateManager.addNewTemplate();
			//TemplateManager.swapTemplates();
			_inTransition = false;
		}
	}
}