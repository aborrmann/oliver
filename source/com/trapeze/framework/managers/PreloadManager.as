﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.framework.events.PreloadEvent;
    import com.trapeze.utils.Log;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.ProgressEvent;
	
	/** Handles load progress of any assets that it is monitoring. Dispatches regular START, PROGRESS, and FINISH events to listeners as required.
     * @see com.trapeze.events.PreloadEvent
     **/
	public class PreloadManager extends EventDispatcher {
        
		
		/** The singleton of the PreloadManager **/
		private static function get instance():PreloadManager {
			if ( _instance == null ) { _instance = new PreloadManager(); }
			return _instance;
		}
		static private var _instance:PreloadManager;
		
		static private var _bytesLoaded:Number = 0;
		static private var _bytesTotal:Number = 0;
		static private var _complete:Boolean = true;
		
		public static var container:MovieClip = new MovieClip();
        
        
        /** The number of items being monitored for load progress. **/
        public static function get numObjects():int {
            return queue.length;
        }
		
        
		/** The total number of bytes loaded so far **/
		public static function get bytesLoaded():Number { return _bytesLoaded; }
		
        
		/** The total number of bytes to load before a FINISH PreloadEvent is fired **/
		public static function get bytesTotal():Number { return _bytesTotal; }
		
        
        /** Controls whether the PreloadManager has finished loading all assets.
         * If it has finished, the PreloadManager is cleared.
         **/
		private static function get complete():Boolean { return _complete };
		private static function set complete( value:Boolean ):void
		{
            if (value && !_complete) {
                clear();
                dispatchEvent( new PreloadEvent( PreloadEvent.FINISH ) )
            }
            else if (!value) {
                _bytesTotal = 0;
                _bytesLoaded = 0;
                dispatchEvent( new PreloadEvent( PreloadEvent.START ) );
            }
            
			_complete = value;
		}
        
        
        private static var queue:Array = [];
        private static var recordIndex:Array = [];
        
        
        /** Registers an object with the manager for sending progress events to the preloader. This object must dispatch progress events in order to be monitored.
         * If the object does not dispatch ProgressEvents, the object will be registered, but the preload progress may never finish as the Manager will not ever receive any progress reports from the object
         * @param object The object to register. The parameter must dispatch ProgressEvents in order to be monitored.
         **/
        public static function register(object:EventDispatcher):void {
            
            if (numObjects == 0) {
                complete = false;
            }
            
            recordIndex.push(object);
            queue.push({
                bytesLoaded : 0,
                bytesTotal : 0
            });
            
            object.addEventListener(ProgressEvent.PROGRESS, onProgress);
        }
        
        
        /** @private **/
        public static function onProgress(e:ProgressEvent):void {
            
            // Find the item that has had a progress event
            var record:* = queue[recordIndex.indexOf(e.target)];
            if (!record) return;
            
            // Updates the bytesLoaded and, if needed, the bytesTotal properties
            record.bytesLoaded = e.bytesLoaded;
            if (!record.bytesTotal) {
                record.bytesTotal = e.bytesTotal;
                _bytesTotal += e.bytesTotal;
            }
            
            // Add up the bytesLoaded of all records and update the total
            var count:Number = 0;
            var index:*;
            for (index in queue) {
                count += parseInt(queue[index].bytesLoaded);
            }
            _bytesLoaded = count;
            
            // Check to see if all preload objects have their bytesTotal set
            var allBytesTotal:Boolean = true;
            for (index in queue) {
                if (queue[index].bytesTotal == 0) {
                    allBytesTotal = false;
                    break;
                }
            }
            
            // Either complete the preloading or dispatch a ProgressEvent, whichever is appropriate
            if (bytesLoaded == bytesTotal && allBytesTotal) {
                complete = true;
            }
            else {
                dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, bytesLoaded, bytesTotal));
            }
        }
        
        
        /** Empties the preload queue **/
        public static function clear():void {
            
            queue = [];
            recordIndex = [];
        }
		
        
		/** Adds an event listener to the PreloadManager class
         * The PreloadManager class will dispatch PreloadEvents on a regular basis. Use this method to respond to them.
         * @param type The type of event
         * @param listener The listener function that processes the event.
         * @param useCapture Determines whether the listener works in the capture phase or the target and bubbling phases. If useCapture is set to true, the listener processes the event only during the capture phase and not in the target or bubbling phase. If useCapture is false, the listener processes the event only during the target or bubbling phase. To listen for the event in all three phases, call addEventListener twice, once with useCapture set to true, then again with useCapture set to false.
         * @param The priority level of the event listener. The priority is designated by a signed 32-bit integer. The higher the number, the higher the priority. All listeners with priority n are processed before listeners of priority n-1. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0.
         * @param Determines whether the reference to the listener is strong or weak. A strong reference (the default) prevents your listener from being garbage-collected. A weak reference does not.
         **/
        public static function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
            instance.addEventListener(type, listener, useCapture, priority, useWeakReference);
        }
        
        
        /** @private **/
        public static function dispatchEvent(event:Event):Boolean {
            return instance.dispatchEvent(event);
        }
        
        
        /** Removes an event listener from the PreloadManager class
         * @param type The type of event
         * @param listener The function that responds to the event
         * @param useCapture Specifies whether the listener was registered for the capture phase or the target and bubbling phases. If the listener was registered for both the capture phase and the target and bubbling phases, two calls to removeEventListener() are required to remove both, one call with useCapture() set to true, and another call with useCapture() set to false.
         **/
        public static function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
            instance.removeEventListener(type, listener, useCapture);
        }
        
        
        /** @private **/
        public static function hasEventListener(type:String):Boolean {
            return instance.hasEventListener(type);
        }
        
        
        /** @private **/
        public static function willTrigger(type:String):Boolean {
            return instance.willTrigger(type);
        }
	}
}