﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.asual.SWFAddress;
    import com.asual.SWFAddressEvent;
    
    import com.trapeze.framework.core.BaseFramework;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.TransitionManager;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.StringUtils;
    
    import flash.display.MovieClip;
    import flash.events.TextEvent;
    
	/** Handles template loading and unloading. SWF Address is fully integrated **/	
	public class TemplateManager {
        
        
		/** The name of the current template that is on stage, as found in the <code>name</code> attribute in the preferences file **/
		public static function get currentTemplateName():String { return _currentTemplateName; }
		private static var _currentTemplateName:String;
        
        
		/** The name of the template that will soon be on stage, as found in the <code>name</code> attribute in the preferences file **/
		public static function get upcomingTemplateName():String { return _upcomingTemplateName; }
		private static var _upcomingTemplateName:String;
		
        
		/** The template path (url) **/
		public static function get currentTemplatePath():String { return _currentTemplatePath; }
		private static var _currentTemplatePath:String;
		
        
		/** The upcoming template path (url) **/
		public static function get upcomingTemplatePath():String { return _upcomingTemplatePath; }
		private static var _upcomingTemplatePath:String;
		
        
		/** The DisplayObject the holds the template. Any current and upcoming templates are placed inside the container. Therefore any transformations applied to the container also apply to all templates **/
		public static function set container( container:MovieClip ):void { _templateContainer = container; }
		public static function get container():MovieClip { return _templateContainer; }
		private static var _templateContainer:MovieClip;
		
		
        /** The current template being displayed **/
		public static function get currentTemplate():BaseFramework {
			return _currentTemplate;
		}
		private static var _currentTemplate:BaseFramework;
		
        
        /** The template that will soon replace <code>currentTemplate</code>.
         * This only has a value when the application has begun to switch templates. Otherwise, the value is null.
         **/
		public static function get upcomingTemplate():BaseFramework {
			return _upcomingTemplate;
		}
		private static var _upcomingTemplate:BaseFramework;
		
        
        /** The default template detected by the framework. If this value is not manually defined at startup, the Framework will try to find a default value. It will look at the URL first, then at the <code>default</code> attribute of the <code>templates</code> node in the preferences file.**/
        public static var defaultTemplatePath:String;
        
        
/* ============== TEMPLATE LOADING FUNCTIONS  ============== */
		
		
		/** Initialization of the manager called in the Framework, after all assets are loaded
		* @private
		**/
		public static function init():void {
            
			// set our references
			_currentTemplateName = null;
			_currentTemplatePath = null;
			
			// SWF Address
			SWFAddress.addEventListener( SWFAddressEvent.INIT, onSWFAddressInit, false, 0, true);
			SWFAddress.addEventListener( SWFAddressEvent.CHANGE, onSWFAddressChange, false, 0, true);
		}
		
        
		/** Begins loading a template SWF file into the container.
         * <p>The template path must point to a SWF file that extends BaseFramework or an error will be thrown later on in the framework load.</p>
		 * @param    templatePath The path to the template SWF. If the path begins with "/", "http://", or "https://", it will be loaded literally as provided. Otherwise it will be prepended with the mediaURL defined in the preferences file.
		 **/
		public static function load( templatePath:String ):void {
            
            templatePath = FrameworkManager.preparePath(templatePath);
			Log.info("Template: " + templatePath);
            
            //PreloadManager.clear();
			
			// if the filePath is not the current path in memory and we're not transitioning
			if ( templatePath != _currentTemplatePath && !TransitionManager.inTransition ) {
                
				// set our templatePath
                _upcomingTemplatePath = templatePath;
			
                // set our current template's name
                _upcomingTemplateName = PrefsManager.getKey( upcomingTemplatePath.replace(PrefsManager.getValue("mediaURL"), "") );
                
                // Define SWF Address URL value
                SWFAddress.setValue( upcomingTemplateName );
                
				TransitionManager.transition();
			}
            
		}
		
		
		/** Called from the TransitionManager when current template has finished it's transition out routine and is ready to add the new template
		* @private
		**/
		public static function swapTemplates():void {
            
            // let's destroy the current template
            if ( currentTemplate ) {
                container.removeChild(currentTemplate);
                currentTemplate.destroy();
                LoadManager.remove(currentTemplateName);
            }
			
            _currentTemplateName = _upcomingTemplateName;
            _currentTemplatePath = _upcomingTemplatePath;
			_currentTemplate = _upcomingTemplate;
            _upcomingTemplateName = _upcomingTemplatePath = null;
            _upcomingTemplate = null;
		}
		
		
		public static function removeOldTemplate():void {
            
            // let's destroy the current template
            if ( currentTemplate ) {
                container.removeChild(currentTemplate);
                currentTemplate.destroy();
                LoadManager.remove(currentTemplateName);
            }
		}
		
		
		public static function addNewTemplate():void {
			
            _currentTemplateName = _upcomingTemplateName;
            _currentTemplatePath = _upcomingTemplatePath;
			_currentTemplate = _upcomingTemplate;
            _upcomingTemplateName = _upcomingTemplatePath = null;
            _upcomingTemplate = null;
		}
        
		
		/** Handles any incoming TextEvent.LINK events registered in any templates
		* @param    event The incoming TextEvent triggered from the current template TextField
		**/
		public static function onLinkEvent( event:TextEvent ):void {
            
			var args:Array = event.text.split(',');
			var linkCommand:String = args[0];
			var linkKey:String = args[1];
			
			if ( linkCommand == 'gotoTemplate' ) load( PrefsManager.getValue( linkKey ) );
		}
		
        
		/** Called when the SWF Address object has initilized **/
		private static function onSWFAddressInit( event:SWFAddressEvent ):void {
            
            // If the default template hasn't been set manually, then try to find it
            // First in the URL (using SWFAddress), and lastly in the preferences file
            if (!defaultTemplatePath) {
            
                Log.info("Trying to find default template...");
                
                // check to see if we're loading the page with a bookmark, if so, we need to load the bookmark path, not the default template
                var templateNameInURL:String = SWFAddress.getValue().substring(1);
                if ( templateNameInURL.length != 0 && currentTemplateName != templateNameInURL ) {
                    defaultTemplatePath = PrefsManager.getValue( templateNameInURL );
                }
                else {
                    var templateNameInPreferences:String = PrefsManager.xml.templates.attribute('default');
                    if (templateNameInPreferences) {
                        defaultTemplatePath = PrefsManager.getValue( templateNameInPreferences );
                    }
                }
            }
            
            if (defaultTemplatePath) {
                load(defaultTemplatePath);
            }
            else {
                Log.warn("No default template found.");
            }
		}
		
        
		/** Called when the SWF Address url has changed **/
		private static function onSWFAddressChange( event:SWFAddressEvent ):void {
            
            // If we are already prepared for switching templates, just do it. Otherwise, try to find the necessary information for template switching
            if (!_upcomingTemplateName) {
                _upcomingTemplateName = event.value.substring(1);
                
                // if the templatePath is not 0 length, and not equal to the currently loaded template ...
                if ( _upcomingTemplateName.length != 0 && currentTemplateName != _upcomingTemplateName ) {
                    
                    // reverse lookup through our preferences to get target swf
                    _upcomingTemplatePath = PrefsManager.getValue( _upcomingTemplateName );
                }
            }
            
            if (upcomingTemplateName) {
                LoadManager.load(upcomingTemplateName, upcomingTemplatePath, onLoadComplete, "swf");
            }
		}
        
        
		/** Called once the template swf has been fully preloaded **/
		private static function onLoadComplete(template:BaseFramework):void {
            
			// If our templateContainer is not defined, we're testing from inside a Template, so we must use the Template as our base DisplayObject
			if ( container == null ) container = FrameworkManager.client;
			
			_upcomingTemplate = template;
			container.addChildAt( template, 0 );
			
			// Notify the TransitionManager that our template has loaded
			TransitionManager.preloaded();
		}
	}
}