﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.framework.events.LocaleEvent;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.utils.Log;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
	
	/** Handles language-specific functionality of the framework. The LocaleManager uses the <code>locales</code> node in the preferences file to **/
	public class LocaleManager extends EventDispatcher {
        
		/** The current language used in the application. Changing this value will dispatch a <code>LocaleEvent.CHANGE</code> event causing the framework to reload all its assets according to the language specified.
         * <p>Acceptable values for this property must be specified in the preferences file. For example, you cannot use the "es" locale if you have not added "es" to the list of locales in the preferences file.</p>
         **/
		public static function get locale():String {
            return _locale;
        }
		public static function set locale( value:String ):void {
            
            if ( value != _locale && locales.indexOf(value) != -1 ) {
                _locale = value;
                dispatchEvent(new LocaleEvent(LocaleEvent.CHANGE));
            }
            else {
                Log.warn("Locale change was not processed.");
            }
        }
		private static var _locale:String;
        
        
		/** An array of all locales specified in the <code>locales</code> node of the preferences file **/
		public static function get locales():Vector.<String> {
            
            if (!_locales) {
                var localeArray:Vector.<String> = new Vector.<String>();
                var localeAmount:Number = PrefsManager.xml.locales.children().length();
                for ( var i:int = 0; i < localeAmount; i++ ) localeArray.push( PrefsManager.xml.locales..locale[i].attribute('name') );
                _locales = localeArray;
            }
			
			return _locales;
		}
        private static var _locales:Vector.<String>;
        
        
/** Events to make event dispatching possible **/


		/** The singleton of the LocaleManager
		* @private
		**/
		public static function get instance():LocaleManager {
			if ( _instance == null ) { _instance = new LocaleManager(); }
			return _instance;
		}
		static private var _instance:LocaleManager;
		
		
		/** @private **/
        public static function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
            instance.addEventListener(type, listener, useCapture, priority, useWeakReference);
        }
        
        /** @private **/
        public static function dispatchEvent(event:Event):Boolean {
            return instance.dispatchEvent(event);
        }
        
        /** @private **/
        public static function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
            instance.removeEventListener(type, listener, useCapture);
        }
        
        /** @private **/
        public static function hasEventListener(type:String):Boolean {
            return instance.hasEventListener(type);
        }
        
        /** @private **/
        public static function willTrigger(type:String):Boolean {
            return instance.willTrigger(type);
        }
	}
}