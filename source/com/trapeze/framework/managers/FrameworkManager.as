﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.managers {
    
    import com.trapeze.utils.Log;
    import com.trapeze.utils.StringUtils;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	
	/** Handles global properties that should be available to all templates and subclasses **/
  	public class FrameworkManager extends EventDispatcher {	
        
		static private var _instance:FrameworkManager;
		static private var _client:MovieClip = null;
        
        
        /** The flashvars that were passed to the project at embed time.
         */
        public static var flashvars:Object;
		
        
		/** The singleton of the FrameworkManager
		**/
		private static function get instance():FrameworkManager {
			if ( _instance == null ) { _instance = new FrameworkManager(); }
			return _instance;
		}
		
        
		/** The MovieClip that spawned the Framework **/
		public static function get client():MovieClip {
            return _client;
        }
		public static function set client( value:MovieClip ):void {
            if (!client) _client = value;
            else Log.error("Error setting the FrameworkManager client. Client has already been set.");
        }
		
        
		/** The Flash stage. This property is set when the framework is spawned, but would be null if the FrameworkManager's <code>client</code> has not been set. **/
		public static function get stage():Stage {
            return client.stage
        }
        
        
        /** The media URL as found in the Preferences file **/
        public static var mediaURL:String;
        
        
        /** Inserts the current locale and prepends the media URL (as found in the Preferences file) in front of the given path. If the path is an absolute URL (begins with <code>http://</code> or <code>https://</code> or <code>/</code>), the media URL is not prepended. **/
        public static function preparePath(path:String):String {
            
            if (path == null) return path;
            
            path = path.replace("%(locale)", LocaleManager.locale);
            if (!(StringUtils.beginsWith(path, "/") || StringUtils.beginsWith(path, "http://") || StringUtils.beginsWith(path, "https://"))) {
                return mediaURL + path;
            }
            else return path;
        }
        
        
        /** Returns a path based on the mediaURL
		
        
		/** @private **/
        public static function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
            instance.addEventListener(type, listener, useCapture, priority, useWeakReference);
        }
        
        
        /** @private **/
        public static function dispatchEvent(event:Event):Boolean {
            return instance.dispatchEvent(event);
        }
        
        
        /** @private **/
        public static function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
            instance.removeEventListener(type, listener, useCapture);
        }
        
        
        /** @private **/
        public static function hasEventListener(type:String):Boolean {
            return instance.hasEventListener(type);
        }
        
        
        /** @private **/
        public static function willTrigger(type:String):Boolean {
            return instance.willTrigger(type);
        }
	}
}