﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.events
{
	import flash.events.Event;

	public class LocaleEvent extends Event
	{
		public static const CHANGE:String = "change";

		public function LocaleEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new LocaleEvent(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return formatToString("LocaleEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	}
}