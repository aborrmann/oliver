﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.events
{
	import flash.events.Event;

	public class PreloadEvent extends Event
	{
		public static const START:String = "start";
		public static const PROGRESS:String = "progress";
		public static const FINISH:String = "finish";

		public function PreloadEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false )
		{
			super(type, bubbles, cancelable);
		}
		
		public override function clone():Event
		{
			return new PreloadEvent(type, bubbles, cancelable);
		}
		
		public override function toString():String
		{
			return formatToString("PreloadEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	}
}