﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.templates {
    
    import com.asual.SWFAddress;
    import com.greensock.TweenLite;
    import com.trapeze.framework.core.BaseFramework;
    import com.trapeze.framework.managers.TransitionManager;
	
	
	/** BaseTemplate, which extends BaseFramework. **/
	public class BaseTemplate extends BaseFramework {
        
		/** The property used to set SWF Address path for this template **/
		public function get title():String { return _title; }
		public function set title( t:String ):void {
			if ( t != _title ) {
				_title = t;
				SWFAddress.setTitle( title );
			}
		}
		private var _title:String = "";
		
        
		/** Defines a simple fade-in transition as the default entrance animation
		* @see com.trapeze.core.BaseFramework#transitionIn()
		**/
		public override function transitionIn():void {
            
			this.alpha = 0;
			TweenLite.to( this, 1, { alpha:1, onComplete: TransitionManager.transitionedIn } );
		}
		
        
		/** Defines a simple fade-out transition as the default exit animation
		* @see com.trapeze.core.BaseFramework#transitionOutPostLoad()
		**/
		public override function transitionOutPostLoad():void {
            
			this.alpha = 1;
			TweenLite.to( this, 1, { alpha:0, onComplete: TransitionManager.transitionedOut } );
		}
	}
}