﻿// TRAPEZE
// Copyright 2009 Trapeze, Inc. All Rights Reserved.
// This file is proprietary and confidential; unauthorized use or redistribution is prohibited.

package com.trapeze.framework.templates {
    
	import com.trapeze.framework.core.Framework;
	import com.trapeze.framework.managers.FrameworkManager;
	import flash.events.Event;
	import flash.display.MovieClip;
	
	/** Manages base listeners for navigation templates which extend the BaseNavigation, and spawning of the framework object. **/
	public class BaseNavigation extends MovieClip {
        
		/** Adds the various listeners required for extended base navigation templates. **/
		public function BaseNavigation() {
            
			FrameworkManager.addEventListener( Event.INIT, onFrameworkInit );
			FrameworkManager.addEventListener( Event.COMPLETE, onFrameworkComplete );
			
			/***
			*	 Let's check to see if we already have a framework spawned, which is referenced in 'client' in
			*   our FrameworkManager, if not, let's create a new Framework object and pass a DisplayObject
			*   reference of 'this' to be shared with the Framework Manager classes
			***/
			
			var client:MovieClip = FrameworkManager.client;
			if ( client == null ) {
				var fm:Framework = new Framework( this );
				fm.load();
			}
            else {
				// Otherwise, let's process the 'onFrameworkComplete' dispatch event which would be dispatched from the
				// Framework Manager class once the Framework has loaded all it's components
				onFrameworkComplete( null );
			}
		}
        
        
		/** Removes all listeners set in the constructor. 
         * @private
         **/
		public function destroy():void {
            
			// This method is called turning the turn over routine in the TemplateManager to remove
			// old listener set in this extended class ( Template / BaseTemplate / BaseFramework
			FrameworkManager.removeEventListener( Event.INIT, onFrameworkInit );
			FrameworkManager.removeEventListener( Event.COMPLETE, onFrameworkComplete );
		}
		
        
		/** The method called when Framework object is initilized
		* @param e:Event - The flash.events.Event dispatched with the Event.INIT event
		**/
		public function onFrameworkInit( event:Event ):void {}
		
		/** The method called when Framework loading is complete
		* @param e:Event - The flash.events.Event dispatched with the Event.COMPLETE event
		**/
		public function onFrameworkComplete( event:Event ):void {}
	}
}