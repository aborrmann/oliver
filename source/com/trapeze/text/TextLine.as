package com.trapeze.text {
    import com.trapeze.framework.managers.StyleManager;
    
    import flash.display.DisplayObjectContainer;
    import flash.text.AntiAliasType;
    import flash.text.GridFitType;
    import flash.text.TextField;
    
    
	/** A single-line TextField with attributes pre-set to work effortlessly with the Framework.
     * 
     * @example This example shows how to set up the framework with all the required assets for using the TextLine class.
     * 
     * <p>Create a new Framework project.</p>
     * 
     * <p>Place this copy into the copy file:</p>
     * <listing version="3.0">
     * &lt;?xml version="1.0" encoding="UTF-8"?&gt;
     * &lt;copyStrings&gt;
     *
     *     &lt;copy name="header"&gt;
     *         &lt;p class="header"&gt;Hello world&lt;/p&gt;
     *     &lt;/copy&gt;
     *
     * &lt;/copyStrings&gt;
     * </listing>
     * 
     * <p>Publish the fonts SWF file with the font <code>Mesquite</code> embedded (or embed a different font for different results) as in the following screenshot:</p>
     * <img src="examples/textline-font-embed.jpg" />
     * 
     * <p>Place these CSS styles into the stylesheet file:</p>
     * <listing version="3.0">
     * .header {
     *     font-family:"Mesquite Std";
     *     font-size:72;
     *     color:#990000;
     * }
     * </listing>
     * 
     * <p>To keep the example as simple as possible, remove all frames from the default FrameworkProject FLA file so that the stage is completely empty.</p>
     * 
     * <p>Set the base FrameworkProject class to this:</p>
     * <listing version="3.0">
     * package {
     *     
     *     import com.trapeze.framework.core.BaseFramework;
     * 	import com.trapeze.framework.managers.CopyManager;
     * 	import com.trapeze.text.TextLine;
     *     import flash.events.Event;
     * 	
     * 	public class FrameworkProject extends BaseFramework {
     *         
     * 		public function FrameworkProject() {}
     * 		
     *         
     * 		override public function onFrameworkComplete( e:Event ):void {
     *             
     * 			var textLine:TextLine = new TextLine();
     * 			textLine.htmlText = CopyManager.getValue("header");
     * 			addChild(textLine);
     * 		}
     * 	}
     * }
     * </listing>
     * 
     * <p>Compile and run the project to see the output. It should look like this:</p>
     * <img src="examples/textline-output.jpg" />
     * 
     */
    public class TextLine extends TextField {
        
        /** Converts an existing TextField into a TextLine. This is useful when authoring content in Flash Professional and a template has a dynamic TextField placed on stage. This function can be called to convert the TextField into a TextLine that can then use fonts from the FontManager and styles from the StyleManager.
         * @param textField The TextField to convert. If the textField is already added to the stage, it will be "hot swapped" with the new TextLine that is returned.
         * @return The TextLine that replaces the textField. Any references to the textField should be updated with this return value.
         **/
        public static function convertTextField(textField:TextField):TextLine {
            
            if (textField is TextLine) {
                return textField as TextLine;
            }
            var textLine:TextLine = new TextLine(false);
            
            textLine.rotation = textField.rotation;
            textLine.x = textField.x;
            textLine.y = textField.y;
            textLine.scaleX = textField.scaleX;
            textLine.scaleY = textField.scaleY;
            textLine.width = textField.width;
            textLine.height = textField.height;
            textLine.text = textField.text;
            textLine.name = textField.name;
            textLine.filters = textField.filters;
            textLine.textColor = textField.textColor;
            textLine.border = textField.border;
            textLine.borderColor = textField.borderColor;
            textLine.selectable = textField.selectable;
            
            textLine.setBasicStyles();
            
            var parent:DisplayObjectContainer = textField.parent;
            if (parent) {
                var displayIndex:int = parent.getChildIndex(textField);
                parent.removeChild(textField);
                parent.addChildAt(textLine, displayIndex);
            }
            
            return textLine;
        }
        
        
        /** Sets the text of this TextLine. When text is placed in the TextLine, the height is transformed to match the height of the text that is inside. The width of the TextLine is also transformed to match the width of the text inside, but only if the text alignment has not been set in the CSS. If the text alignment has been set, the width is not adjusted. This will preserve any special visual presentation of the text.
         * @example
         * <listing version="3.0">
         * var textLine:Textline = new TextLine();
         * textLine.htmlText = "<p>Click <a href="http://www.trapeze.com">here</a>.</p>
         * </listing>
         **/
        override public function get htmlText():String {
            return super.htmlText;
        }
        override public function set htmlText(value:String):void {
            
            super.htmlText = value;
            
            // If the alignment of the TextFormat is null, that means the text-align CSS property has been set.
            // When the CSS alignment property has been set, the alignment is relative to the width of this TextField.
            // If the text-align CSS property has not been set, then we want to make the width as small as possible.
            if (this.getTextFormat().align != null) {
                width = textWidth + 4;
            }
            height = textHeight + 4;
        }
        
        
        /** Constructor.
         * @param applyDefaultStyles Specifies whether to call <code>setBasicStyles</code> during instantiation. By default, the basic styles will be set, but you can turn this off if you plan to set the default styles later. If you do not plan to call <code>setBasicStyles</code> at all, consider using a TextField instead of a TextLine.
        **/
        public function TextLine(applyDefaultStyles:Boolean = true) {
            
            if (applyDefaultStyles) setBasicStyles();
        }
        
        
        /**
         * Sets the default attributes of the TextField to work optimally with the Framework. This function is called automatically on instantiation unless <code>false</code> is passed to the constructor. This method can be called to "reset" a TextLine after transformation.
         */
        public function setBasicStyles():void {
            
            multiline = false;
            condenseWhite = true;
            embedFonts = true;
            antiAliasType = AntiAliasType.ADVANCED;
            gridFitType = GridFitType.SUBPIXEL;
            styleSheet = StyleManager.styleSheet;
            sharpness = 0;
        }
        
    }

}