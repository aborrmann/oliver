package com.trapeze.text {
    import com.trapeze.framework.managers.StyleManager;
    import com.trapeze.utils.Log;
    import flash.display.DisplayObjectContainer;
    import flash.text.AntiAliasType;
    import flash.text.GridFitType;
    import flash.text.TextField;
    import flash.utils.describeType;
	/**
     * ...
     * @author Adrian Borrmann
     */
    public class Paragraph extends TextField {
        
        public static function convertTextField(textField:TextField):Paragraph {
            
            if (textField is Paragraph) {
                return textField as Paragraph;
            }
            var paragraph:Paragraph = new Paragraph(false);
            paragraph.heightSetFlag = true;
            
            paragraph.rotation = textField.rotation;
            paragraph.x = textField.x;
            paragraph.y = textField.y;
            paragraph.scaleX = textField.scaleX;
            paragraph.scaleY = textField.scaleY;
            paragraph.width = textField.width;
            paragraph.height = textField.height;
            paragraph.text = textField.text;
            paragraph.name = textField.name;
            paragraph.filters = textField.filters;
            paragraph.textColor = textField.textColor;
            paragraph.border = textField.border;
            paragraph.borderColor = textField.borderColor;
            paragraph.selectable = textField.selectable;
            
            paragraph.setBasicStyles();
            
            var parent:DisplayObjectContainer = textField.parent;
            if (parent) {
                var displayIndex:int = parent.getChildIndex(textField);
                parent.removeChild(textField);
                parent.addChildAt(paragraph, displayIndex);
            }
            
            return paragraph;
        }
        
        override public function set htmlText(value:String):void {
            
            super.htmlText = value;
            if (!heightSetFlag) {
                super.height = textHeight + 10;
            }
        }
        
        
        private var heightSetFlag:Boolean = false;
        override public function set height(value:Number):void {
            
            heightSetFlag = true;
            super.height = value;
        }
        
        /* A paragraph is a multi-line text field with a fixed width. By default, the height is dynamic unless you explicitly set the `height` property.
         * 
         * @param applyBasicStyles Specifies whether to set the basic properties to integrate with the Framework's StyleManager and FontManager. True by default. You can set this to false if you want to call the `setBasicStyles` function later or not at all */
        public function Paragraph(applyBasicStyles:Boolean = true) {
            
            if (applyBasicStyles) setBasicStyles();
        }
        
        
        public function setBasicStyles():void {
            
            multiline = true;
            wordWrap = true;
            condenseWhite = true;
            embedFonts = true;
            antiAliasType = AntiAliasType.ADVANCED;
            gridFitType = GridFitType.PIXEL;
            styleSheet = StyleManager.styleSheet;
            sharpness = 0;
            mouseWheelEnabled = true;
        }
        
    }

}