package com.oliver.events {
    import flash.events.Event;
	/**
     * ...
     * @author Adrian Borrmann
     */
    public class PresenceDetectionEvent extends Event {
        
        public static const INITIALIZING:String = "initializing";
        public static const PRESENCE_DETECTED_FAR_AWAY:String = "presence detected far away";
        public static const PRESENCE_DETECTED_CLOSE_UP:String = "presence detected close up";
		public static const PRESENCE_REMOVED:String = "presence removed";
        
        public function PresenceDetectionEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
            
            super(type, bubbles, cancelable);
        }
    }
}