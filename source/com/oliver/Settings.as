﻿package com.oliver {
	import com.oliver.settings.OnScreenCameraSettings;
	import com.oliver.settings.OnScreenMenuSettings;
	import flash.display.MovieClip;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class Settings {
		
		public static var keyboardFocusedElement:Object;
		public static var keyboard:MovieClip;
		public static var presenceDetector:Object;
		
		public static var webBrowserDefaultWebsite:String = "http://www.trapeze.com";
	}
}