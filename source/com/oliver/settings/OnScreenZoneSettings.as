package com.oliver.settings {
	import com.greensock.TweenLite;
	import com.trapeze.framework.managers.FrameworkManager;
	import com.trapeze.framework.managers.LocaleManager;
	import com.trapeze.framework.managers.PrefsManager;
	import com.trapeze.framework.managers.TemplateManager;
	import fl.controls.Button;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class OnScreenZoneSettings extends Popup {
		
		public var zone1:Button;
		public var zone2:Button;
		
		public function OnScreenZoneSettings() {
			
			zone1.addEventListener(MouseEvent.CLICK, onZone1);
			zone2.addEventListener(MouseEvent.CLICK, onZone2);
		}
		
		
		private function onZone1(e:MouseEvent):void {
			
			TemplateManager.load(PrefsManager.getValue('Blank'));
			TweenLite.delayedCall(1, changeZone, [0]);
		}
		
		
		private function onZone2(e:MouseEvent):void {
			
			TemplateManager.load(PrefsManager.getValue('Blank'));
			TweenLite.delayedCall(1, changeZone, [1]);
		}
		
		private function changeZone(zone:int):void {
			
			LocaleManager.locale = LocaleManager.locales[zone];
			hide();
		}
	}
}