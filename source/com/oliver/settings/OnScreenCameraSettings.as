package com.oliver.settings {
	import com.greensock.easing.Back;
	import com.greensock.TweenLite;
	import com.oliver.Settings;
	import com.oliver.webcam.PresenceDetector;
	import com.trapeze.framework.managers.FrameworkManager;
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.NumericStepper;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class OnScreenCameraSettings extends Popup {
		
		public var background:Sprite;
		public var calibrate:Button;
		public var inverter:ComboBox;
		public var nearMotion:NumericStepper;
		public var farMotion:NumericStepper;
		public var motionLevel:TextField;
		
		private var motionInterval:uint;
		private var presenceDetector:PresenceDetector;
		
		public function OnScreenCameraSettings() {
			
			calibrate.addEventListener(MouseEvent.CLICK, onCalibrate);
			inverter.addEventListener(Event.CHANGE, onInvert);
			nearMotion.addEventListener(Event.CHANGE, onNearMotionChange);
			farMotion.addEventListener(Event.CHANGE, onFarMotionChange);
		}
		
		
		override public function show():void {
			
			super.show();
			
			presenceDetector = PresenceDetector(Settings.presenceDetector);
			
			if (presenceDetector.cameraBackground == PresenceDetector.DARK_BACKGROUND) {
				inverter.selectedItem = inverter.getItemAt(0);
			}
			else {
				inverter.selectedItem = inverter.getItemAt(1);
			}
			
			nearMotion.value = presenceDetector.HIGH_MOTION_LEVEL;
			farMotion.value = presenceDetector.LOW_MOTION_LEVEL;
			motionInterval = setInterval(onInterval, 1000);
		}
		
		
		override public function hide():void {
			
			clearInterval(motionInterval);
			super.hide();
		}
		
		
		private function onCalibrate(e:MouseEvent):void {
			
			presenceDetector.calibrateCamera();
		}
		
		
		private function onInvert(e:Event):void {
			
			if (inverter.selectedLabel == "dark") {
				presenceDetector.cameraBackground = PresenceDetector.DARK_BACKGROUND;
			}
			else if (inverter.selectedLabel == "light") {
				presenceDetector.cameraBackground = PresenceDetector.LIGHT_BACKGROUND;
			}
		}
		
		
		private function onNearMotionChange(e:Event):void {
			
			presenceDetector.HIGH_MOTION_LEVEL = nearMotion.value;
		}
		
		
		private function onFarMotionChange(e:Event):void {
			
			presenceDetector.LOW_MOTION_LEVEL = farMotion.value;
		}
		
		
		private function onInterval():void {
			
			motionLevel.text = presenceDetector.motionLevel + "";
		}
	}
}