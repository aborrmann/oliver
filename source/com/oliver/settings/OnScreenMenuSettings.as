package com.oliver.settings {
	import com.greensock.easing.Back;
	import com.greensock.TweenLite;
	import com.oliver.Settings;
	import com.trapeze.framework.managers.FrameworkManager;
	import com.trapeze.framework.managers.LoadManager;
	import com.trapeze.framework.managers.PrefsManager;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class OnScreenMenuSettings extends Popup {
		
		public var background:Sprite;
		public var camera:Sprite;
		public var zone:Sprite;
		
		private var cameraSettings:Popup;
		private var zoneSettings:Popup;
		
		public function OnScreenMenuSettings() {
			
			if (Settings.presenceDetector) {
				LoadManager.load('camera settings', PrefsManager.getValue('cameraSettings'), onCameraSettingsLoad, 'swf');
			}
			else {
				camera.alpha = 0.5;
			}
			
			LoadManager.load("zone settings", PrefsManager.getValue("zoneSettings"), onZoneSettingsLoad, 'swf');
		}
		
		
		private function onCameraSettingsLoad(cameraSettings:DisplayObject) {
			
			this.cameraSettings = Popup(cameraSettings);
			camera.addEventListener(MouseEvent.CLICK, onCamera);
		}
		
		
		private function onZoneSettingsLoad(zoneSettings:DisplayObject) {
			
			this.zoneSettings = Popup(zoneSettings);
			zone.addEventListener(MouseEvent.CLICK, onZone);
		}
		
		
		private function onCamera(e:MouseEvent):void {
			
			cameraSettings.show();
			hide();
		}
		
		
		private function onZone(e:MouseEvent):void {
			
			zoneSettings.show();
			hide();
		}
	}
}