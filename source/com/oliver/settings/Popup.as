package com.oliver.settings {
	import com.greensock.easing.Back;
	import com.greensock.OverwriteManager;
	import com.greensock.TweenLite;
	import com.oliver.Settings;
	import com.trapeze.framework.managers.FrameworkManager;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class Popup extends MovieClip {
		
		public function Popup() {
			
		}
		
		
		public function show():void {
			
			if (stage) return;
			
			
			x = FrameworkManager.stage.stageWidth / 2 - width / 2;
			y = FrameworkManager.stage.stageHeight - height + 50;
			alpha = 0;
			FrameworkManager.stage.addChild(this);
			
			TweenLite.to(this, 0.5, { y:FrameworkManager.stage.stageHeight - height - 25, alpha:1.0, ease:Back.easeOut, onComplete: onShown } );
		}
		
		
		public function hide():void {
			
			if (!stage) return;
			
			for (var i:int = 0; i < stage.numChildren; i++) {
				stage.getChildAt(i).removeEventListener(MouseEvent.CLICK, onFocusOut);
			}
			
			TweenLite.to(this, 0.2, { y:y + 50, alpha:0, onComplete: onHidden } );
		}
		
		
		protected function onShown():void {
			
			for (var i:int = 0; i < stage.numChildren; i++) {
				var child:DisplayObject = stage.getChildAt(i);
				if (child != this) {
					child.addEventListener(MouseEvent.CLICK, onFocusOut);
				}
			}
		}
		
		
		protected function onHidden():void {
			
			FrameworkManager.stage.removeChild(this);
		}
		
		
		private function onFocusOut(e:Event):void {
			
			if (e.currentTarget != this && !Settings.keyboardFocusedElement){
				TweenLite.delayedCall(0.2, hide);
			}
		}
	}
}