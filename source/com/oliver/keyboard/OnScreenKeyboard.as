package com.oliver.keyboard {
    import com.greensock.easing.Back;
    import com.greensock.TweenLite;
    import com.oliver.Settings;
    import com.oliver.settings.Popup;
    import com.trapeze.framework.managers.FrameworkManager;
    import com.trapeze.utils.Log;
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.EventPhase;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.utils.getDefinitionByName;
    import flash.utils.getQualifiedClassName;
    /**
     * ...
     * @author Adrian Borrmann
     */
    public class OnScreenKeyboard extends Popup {
            
        public var marginBetweenKeys:int = 8;
        public var keyLayout:Array = [
            ['`', '~'], ['1', '!'], ['2', '@'], ['3', '#'], ['4', '$'], ['5', '%'], ['6', '^'], ['7', '&'], ['8', '*'], ['9', '('], ['0', ')'], ['-', '_'], ['=', '+'], 'Back', '\n',
            'Tab', ['q', 'Q'], ['w', 'W'], ['e', 'E'], ['r', 'R'], ['t', 'T'], ['y', 'Y'], ['u', 'U'], ['i', 'I'], ['o', 'O'], ['p', 'P'], ['[', '{'], [']', '}'], ['\\', '|'], '\n',
            'Caps', ['a', 'A'], ['s', 'S'], ['d', 'D'], ['f', 'F'], ['g', 'G'], ['h', 'H'], ['j', 'J'], ['k', 'K'], ['l', 'L'], [';', ':'], ["'", '"'], "Enter", '\n',
            'Shift', ['z', 'Z'], ['x', 'X'], ['c', 'C'], ['v', 'V'], ['b', 'B'], ['n', 'N'], ['m', 'M'], [',', '<'], ['.', '>'], ['/', '?'], 'Shift', '\n',
            'Space'
        ];
        
        public var background:Sprite;
        public var container:Sprite;
        
        
        public var textObject:Object;
        public var textProperty:String;
        public var textSelectionStart:String;
        public var textSelectionEnd:String;
        public var textSelectFunction:String;
        public var textOnComplete:Function;
        
        private var layoutMarkerX:Number = 0;
        private var layoutMarkerY:Number = 8;
        
        private var keys:Array = [];
        
        private var _state:uint;
        public function get state():uint {
            return _state;
        }
        public function set state(value:uint):void {
            
            var variableName:String;
            switch (value) {
                case KeyboardState.NORMAL:
                    variableName = "normalState";
                    break;
                
                case KeyboardState.SHIFT:
                case KeyboardState.CAPS:
                    variableName = "shiftState";
                    break;
                    
                default:
                    throw new Error("Invalid keyboard state.");
                    break;
            }
            
            for (var index in keys) {
                var key:Key = Key(keys[index]);
                try {
                    key.labelField.text = keys[index][variableName];
                }
                catch (error:Error) {
                    Log.info("Error setting state for " + key.labelField);
                }
            }
            
            _state = value;
        }
        
        
        public function OnScreenKeyboard() {
            
            addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
            
            for (var index in keyLayout) {
                
                if (keyLayout[index] == '\n') {
                    layoutMarkerX = 0;
                    layoutMarkerY += 56;
                }
                
                else {
                
                    var key:Key;
                    
                    if (keyLayout[index] == "Space") {
                        key = new Spacebar();
                        key.normalState = key.shiftState = " ";
                    }
                    
                    else if (keyLayout[index] is String) {
                        key = new LongKey();
                        key['is' + keyLayout[index] + 'Key'] = true;
                        key.normalState = key.shiftState = keyLayout[index];
                    }
                    
                    else {
                        key = new Key();
                        key.normalState = keyLayout[index][0];
                        key.shiftState = keyLayout[index][1];
                    }
                    
                    key.addEventListener(MouseEvent.CLICK, onKeyStroke);
                    keys.push(key);
                    
                    key.y = layoutMarkerY;
                    if (key is Spacebar) {
                        key.x = width / 2 - key.width / 2;
                    }
                    else {
                        key.x = marginBetweenKeys + layoutMarkerX;
                    }
                    
                    layoutMarkerX += marginBetweenKeys + key.width;
                    
                    container.addChild(key);
                }
            }
            
            background.width = width + marginBetweenKeys;
            background.height = height + marginBetweenKeys;
            
            state = KeyboardState.NORMAL;
        }
        
        
        public function showKeyboard(object:Object, property:String, selectionStart:String, selectionEnd:String, selectionFunction:String, onComplete:Function = null):void {
            
            background.alpha = 1.0;
            
            textObject = object;
            textProperty = property;
            textSelectionStart = selectionStart;
            textSelectionEnd = selectionEnd;
            textSelectFunction = selectionFunction;
            textOnComplete = onComplete;
            
            show();
        }
        
        
        override public function hide():void {
            
            super.hide();
            TweenLite.to(background, 0.1, { alpha:0 } );
        }
        
        
        override protected function onHidden():void {
            
            super.onHidden();
            textOnComplete = null;
        }
        
        
        private function onKeyStroke(e:Event):void {
            
            var key:Key = Key(e.target);
            
            
            var selectionStart:Number = textObject[textSelectionStart];
            var selectionEnd:Number = textObject[textSelectionEnd];
            var original:String = textObject[textProperty];
            
            var keypress:String;
            
            if (key.isShiftKey) {
                if (state == KeyboardState.NORMAL) {
                    state = KeyboardState.SHIFT;
                }
                else {
                    state = KeyboardState.NORMAL;
                }
                return;
            }
            
            else if (key.isCapsKey) {
                if (state == KeyboardState.NORMAL) {
                    state = KeyboardState.CAPS;
                }
                else {
                    state = KeyboardState.NORMAL;
                }
                return;
            }
            
            else if (key.isEnterKey) {
                if (textOnComplete != null) {
                    textOnComplete.call();
                }
                dispatchEvent(new Event(Event.COMPLETE));
                return;
            }
            
            else if (key.isBackKey) {
                textObject[textProperty] = original.substring(0, selectionStart - 1) + original.substr(selectionEnd);
                selectionStart--;
                textObject[textSelectFunction].call(textObject, selectionStart, selectionStart);
                return;
            }
            
            else if (key.isTabKey) {
                hide();
                return;
            }
            
            if (!keypress) {
                switch (state) {
                    
                    case KeyboardState.NORMAL:
                        keypress = key.normalState;
                        break;
                        
                    case KeyboardState.SHIFT:
                        keypress = key.shiftState;
                        state = KeyboardState.NORMAL;
                        break;
                        
                    case KeyboardState.CAPS:
                        keypress = key.shiftState;
                        
                    default:
                        break;
                }
            }
            
            var newText:String = original.substring(0, selectionStart) + keypress + original.substr(selectionEnd);
            textObject[textProperty] = newText;
            
            selectionStart++;
            textObject[textSelectFunction].call(textObject, selectionStart, selectionStart);
        }
        
        
        private function onRemovedFromStage(e:Event):void {
            
        }
    }
}