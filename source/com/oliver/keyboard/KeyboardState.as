package com.oliver.keyboard {
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class KeyboardState {
		
		public static var NORMAL:uint = 1;
		public static var SHIFT:uint = 2;
		public static var CAPS:uint = 3;
	}
}