package com.oliver.keyboard {
	import com.trapeze.ui.MovieClipButton;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class Key extends MovieClipButton {
		
		public var labelField:TextField;
		
		public var isShiftKey:Boolean = false;
		public var isTabKey:Boolean = false;
		public var isCapsKey:Boolean = false;
		public var isEnterKey:Boolean = false;
		public var isBackKey:Boolean = false;
		
		public var normalState:String;
		public var shiftState:String;
		
		public function Key() {
			
			mouseChildren = false;
		}
	}
}