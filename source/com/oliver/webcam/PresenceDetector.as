package com.oliver.webcam {
    import com.oliver.events.PresenceDetectionEvent;
    import com.trapeze.framework.managers.FrameworkManager;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.utils.Log;
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.events.ActivityEvent;
    import flash.events.EventDispatcher;
    import flash.filters.ColorMatrixFilter;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.media.Camera;
    import flash.media.Video;
    import flash.text.TextField;
    import flash.utils.setInterval;
    import flash.utils.setTimeout;
    /**
     * ...
     * @author Adrian Borrmann
     */
    public class PresenceDetector extends EventDispatcher {
        
        public static const LIGHT_BACKGROUND:Number = 0xFFEEEEEE;
        public static const DARK_BACKGROUND:Number = 0xFF111111;
        
        private var camera:Camera;
        private var video:Video = new Video();
        
        public var cameraBackground:Number = DARK_BACKGROUND;
        
        public var presenceStatus:String = PresenceDetectionEvent.PRESENCE_REMOVED;
        
        public var motionLevel:uint = 0;
        public var referenceWhitePixels:uint = 0;
        public var comparisonWhitePixels:uint = 0;
        
        public var referenceBitmap:Bitmap;
        public var comparisonBitmap:Bitmap;
        
        public var referenceBitmapData:BitmapData; // The background that the camera looks at when idle
        public var comparisonBitmapData:BitmapData; // A frame capture from the webcam that will be compared with the reference bitmap to detect presence
        
        public var console:TextField;
        
        
        private var _HIGH_MOTION_LEVEL:int = parseInt(PrefsManager.getValue("high motion level"));
        public function get HIGH_MOTION_LEVEL():int {
            return _HIGH_MOTION_LEVEL;
        }
        public function set HIGH_MOTION_LEVEL(value:int):void {
            _HIGH_MOTION_LEVEL = value;
        }
        
        
        private var _LOW_MOTION_LEVEL:int = parseInt(PrefsManager.getValue("low motion level"));
        public function get LOW_MOTION_LEVEL():int {
            return _LOW_MOTION_LEVEL;
        }
        public function set LOW_MOTION_LEVEL(value:int):void {
            _LOW_MOTION_LEVEL = value;
        }
        
                
        public function PresenceDetector(cameraNumber:int) {
            
            Log.info(Camera.names);
            if (Camera.names.length == 0) return;
            
            
            var motionLevel:int = parseInt(PrefsManager.getValue('motion level'));
            var timeOut:int = parseInt(PrefsManager.getValue('camera timeout'));
            Log.info("Initializing camera " + cameraNumber + " | " + motionLevel + " | " + timeOut);
            camera = Camera.getCamera(cameraNumber + "");
            camera.setMode(640, 480, 30);
            camera.setMotionLevel(motionLevel, timeOut);
            camera.addEventListener(ActivityEvent.ACTIVITY, onCameraInit);
           
            video.attachCamera(camera);
            
            if (PrefsManager.getValue("debug motion") == "true") {
                FrameworkManager.stage.addChild(video);
            }
        }
        
        
        public function calibrateCamera():void {
            
            referenceBitmapData = new BitmapData(video.width, video.height);
            referenceBitmapData.draw(video);
            referenceWhitePixels = converToBW(referenceBitmapData);
            
            if (PrefsManager.getValue("debug motion") == "true") {
                referenceBitmap = new Bitmap(referenceBitmapData);
                referenceBitmap.x = video.width;
                FrameworkManager.stage.addChild(referenceBitmap);
            }
            
            setInterval(detectPresence, parseInt(PrefsManager.getValue('camera refresh rate')));
        }
        
        
        private function converToBW(source:BitmapData):uint {
            source.threshold(source, source.rect, new Point(), "<=", cameraBackground, 0xFF000000, 0xFFFFFFFF);
            return source.threshold(source, source.rect, new Point(), ">", cameraBackground, 0xFFFFFFFF, 0xFFFFFFFF);
        }
        
        
        private function detectPresence():void {
            
            comparisonBitmapData = new BitmapData(video.width, video.height);
            comparisonBitmapData.draw(video);
            comparisonWhitePixels = converToBW(comparisonBitmapData);
            
            motionLevel = Math.abs(referenceWhitePixels - comparisonWhitePixels);
            
            if (motionLevel > HIGH_MOTION_LEVEL) {
                presenceStatus = PresenceDetectionEvent.PRESENCE_DETECTED_CLOSE_UP;
                dispatchEvent(new PresenceDetectionEvent(PresenceDetectionEvent.PRESENCE_DETECTED_CLOSE_UP));
            }
            else if (motionLevel > LOW_MOTION_LEVEL) {
                presenceStatus = PresenceDetectionEvent.PRESENCE_DETECTED_FAR_AWAY;
                dispatchEvent(new PresenceDetectionEvent(PresenceDetectionEvent.PRESENCE_DETECTED_FAR_AWAY));
            }
            else {
                presenceStatus = PresenceDetectionEvent.PRESENCE_REMOVED;
                dispatchEvent(new PresenceDetectionEvent(PresenceDetectionEvent.PRESENCE_REMOVED));
            }
            
            if (console) {
                console.text = "Current presence level: " + motionLevel + " | Low presence trigger: " + LOW_MOTION_LEVEL + " | High presence trigger: " + HIGH_MOTION_LEVEL;
            }
            
            if (PrefsManager.getValue("debug motion") == "true") {
                
                if (comparisonBitmap) {
                    FrameworkManager.stage.removeChild(comparisonBitmap);
                }
                comparisonBitmap = new Bitmap(comparisonBitmapData);
                comparisonBitmap.x = video.width*2;
                FrameworkManager.stage.addChild(comparisonBitmap);
            }
        }
        
        
        private function onCameraInit(e:ActivityEvent):void {
            
            if (!e.activating) {
                camera.removeEventListener(ActivityEvent.ACTIVITY, onCameraInit);
            }
            else {
                dispatchEvent(new PresenceDetectionEvent(PresenceDetectionEvent.INITIALIZING));
                setTimeout(calibrateCamera, 1000);
            }
        }
    }

}