package com.oliver.webcam {
	import com.greensock.easing.Back;
	import com.greensock.TweenLite;
	import com.trapeze.framework.managers.FrameworkManager;
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.NumericStepper;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Adrian Borrmann
	 */
	public class OnScreenCameraSettings extends MovieClip {
		
		public var background:Sprite;
		public var calibrate:Button;
		public var inverter:ComboBox;
		public var nearMotion:NumericStepper;
		public var farMotion:NumericStepper;
		
		private var presenceDetector:PresenceDetector;
		
		public function OnScreenCameraSettings() {
			
			calibrate.addEventListener(MouseEvent.CLICK, onCalibrate);
			inverter.addEventListener(Event.CHANGE, onInvert);
			nearMotion.addEventListener(Event.CHANGE, onNearMotionChange);
			farMotion.addEventListener(Event.CHANGE, onNearMotionChange);
		}
		
		
		public function show(presenceDetector:PresenceDetector):void {
			
			if (stage) return;
			
			this.presenceDetector = presenceDetector;
			
			if (presenceDetector.cameraBackground == PresenceDetector.DARK_BACKGROUND) {
				inverter.selectedItem = inverter.getItemAt(0);
			}
			else {
				inverter.selectedItem = inverter.getItemAt(1);
			}
			
			nearMotion.value = presenceDetector.HIGH_MOTION_LEVEL;
			farMotion.value = presenceDetector.LOW_MOTION_LEVEL;
			
			background.alpha = 1.0;
			x = FrameworkManager.stage.stageWidth / 2 - width / 2;
			y = FrameworkManager.stage.stageHeight - height + 50;
			alpha = 0;
			FrameworkManager.stage.addChild(this);
			
			TweenLite.to(this, 0.5, { y:FrameworkManager.stage.stageHeight - height - 25, alpha:1.0, ease:Back.easeOut, onComplete: onShown } );
		}
		
		
		public function hide():void {
			
			if (!stage) return;
			
			TweenLite.to(background, 0.1, { alpha:0 } );
			TweenLite.to(this, 0.2, { y:y + 50, alpha:0, onComplete: onHidden } );
		}
		
		
		private function onShown():void {
			
			
		}
		
		
		private function onHidden():void {
			
			FrameworkManager.stage.removeChild(this);
		}
		
		
		private function onCalibrate(e:MouseEvent):void {
			
			presenceDetector.calibrateCamera();
		}
		
		
		private function onInvert(e:Event):void {
			
			if (inverter.selectedLabel == "dark") {
				presenceDetector.cameraBackground = PresenceDetector.DARK_BACKGROUND;
			}
			else if (inverter.selectedLabel == "light") {
				presenceDetector.cameraBackground = PresenceDetector.LIGHT_BACKGROUND;
			}
		}
		
		
		private function onNearMotionChange(e:Event):void {
			
			presenceDetector.LOW_MOTION_LEVEL = nearMotion.value;
		}
		
		
		private function onFarMotionChange(e:Event):void {
			
			presenceDetector.HIGH_MOTION_LEVEL = farMotion.value;
		}
	}
}