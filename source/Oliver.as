﻿package {
    
    import com.greensock.easing.Strong;
    import com.greensock.TweenLite;
    import com.oliver.events.PresenceDetectionEvent;
    import com.oliver.Settings;
    import com.oliver.settings.OnScreenMenuSettings;
    import com.oliver.webcam.PresenceDetector;
    import com.trapeze.framework.core.BaseFramework;
    import com.trapeze.framework.events.LocaleEvent;
    import com.trapeze.framework.managers.FrameworkManager;
    import com.trapeze.framework.managers.LoadManager;
    import com.trapeze.framework.managers.LocaleManager;
    import com.trapeze.framework.managers.NavigationManager;
    import com.trapeze.framework.managers.PrefsManager;
    import com.trapeze.framework.managers.PreloadManager;
    import com.trapeze.framework.managers.TemplateManager;
    import com.trapeze.framework.managers.TransitionManager;
    import com.trapeze.utils.Log;
    import com.trapeze.utils.LogLevel;
    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.display.StageDisplayState;
    import flash.display.StageQuality;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    
    import flash.display.MovieClip;
    import flash.events.Event;
    
    
    /** Default base project class **/
    public class Oliver extends BaseFramework {
        
        public var navigationClip:MovieClip;
        public var templateContainerClip:MovieClip;
        public var preloaderClip:MovieClip;
        public var console:TextField;
        
        private var isInit:Boolean = false;
        
        private var mainSettings:OnScreenMenuSettings;
        
        private var ignorePresenceDetector:Boolean = false;
        private var presenceDetector:PresenceDetector;
        private var currentTemplate:String;
        
        public function Oliver() { 
            
            Log.level = LogLevel.INFO;
            
            TemplateManager.container = templateContainerClip;
            NavigationManager.container = navigationClip;
            PreloadManager.container = preloaderClip;
        }
        
        
        /** Application starting point. Project code can be safely executed here knowing that all framework assets and initialization is complete. **/
        override public function onFrameworkComplete( e:Event ):void {
            
            if (!isInit) init();
        }
        
        
        private function init(e:Event = null):void {
            
            stage.scaleMode = StageScaleMode.SHOW_ALL;
            
            var showFullScreen:Boolean = PrefsManager.getValue("full screen") == "true" ? true : false;
            if (showFullScreen) {
                stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
            }
            
            var detectMotion:Boolean = PrefsManager.getValue("detect motion") == "true" ? true : false;
            if (detectMotion) {
                
                TemplateManager.defaultTemplatePath = PrefsManager.getValue("Asleep");
                var cameraNumber:int = parseInt(PrefsManager.getValue('primary camera'));
                presenceDetector = new PresenceDetector(cameraNumber);
                presenceDetector.console = console;
                presenceDetector.addEventListener(PresenceDetectionEvent.PRESENCE_DETECTED_FAR_AWAY, onMovementFar);
                presenceDetector.addEventListener(PresenceDetectionEvent.PRESENCE_DETECTED_CLOSE_UP, onMovementNear);
                presenceDetector.addEventListener(PresenceDetectionEvent.PRESENCE_REMOVED, onMovementStop);
                
                
            }
            
            stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
            
            if (PrefsManager.getValue("debug motion") == "true") {
                templateContainerClip.alpha = 0.5;
            }
            
            initSettings();
            
            LocaleManager.addEventListener(LocaleEvent.CHANGE, onZoneChange);
            LoadManager.load('onscreen keyboard', FrameworkManager.preparePath(PrefsManager.getValue('onscreenKeyboard')), onOnscreenKeyboard, 'swf');
            isInit = true;
        }
        
        
        private function initSettings():void {
            
            Settings.presenceDetector = presenceDetector;
            Settings.keyboard = null;
            
            LoadManager.load("main settings", FrameworkManager.preparePath(PrefsManager.getValue('mainSettings')), onSettingsLoad, 'swf');
        }
        
        
        private function onZoneChange(e:LocaleEvent):void {
            
            TweenLite.delayedCall(0.5, TemplateManager.load, [PrefsManager.getValue('Lobby')]);
        }
        
        
        private function onMovementFar(e:PresenceDetectionEvent):void {
            
            if (ignorePresenceDetector) return;
            
            if (currentTemplate != "Awake") {
                currentTemplate = "Awake";
                triggerTemplate(PrefsManager.getValue(currentTemplate));
            }
        }
        
        
        private function onMovementNear(e:PresenceDetectionEvent):void {
            
            if (ignorePresenceDetector) return;
            
            if (currentTemplate != "Lobby") {
                currentTemplate = "Lobby";
                triggerTemplate(PrefsManager.getValue(currentTemplate));
            }
        }
        
        
        private function onMovementStop(e:PresenceDetectionEvent = null):void {
            
            if (ignorePresenceDetector) return;
            
            if (currentTemplate != "Asleep") {
                currentTemplate = "Asleep";
                triggerTemplate(PrefsManager.getValue(currentTemplate));
            }
        }
        
        
        private function triggerTemplate(newTemplate:String) {
            
            if (TransitionManager.inTransition) {
                TweenLite.killDelayedCallsTo(TemplateManager.load);
                TweenLite.delayedCall(0.5, TemplateManager.load, [newTemplate]);
            }
            else {
                TemplateManager.load(newTemplate);
            }
        }
        
        
        private function keyDown(e:KeyboardEvent):void {
            
            Log.info(e.keyCode);
            
            switch (e.keyCode) {
                case 107: // "+"
                case 187:
                case 192:
                    Log.info("Showing main settings");
                    mainSettings.show();
                    break;
                    
                case 49:
                    if (ignorePresenceDetector) {
                        ignorePresenceDetector = false;
                        var presenceUnlocked:DisplayObject = addChild(new PresenceUnlocked());
                        TweenLite.to(presenceUnlocked, 3, { alpha:0, ease:Strong.easeOut, onComplete:removeChild, onCompleteParams:[presenceUnlocked] } );
                    }
                    else {
                        ignorePresenceDetector = true;
                        currentTemplate = "Lobby";
                        triggerTemplate(PrefsManager.getValue(currentTemplate));
                        var presenceLocked:DisplayObject = addChild(new PresenceLocked());
                        TweenLite.to(presenceLocked, 3, { alpha:0, ease:Strong.easeOut, onComplete:removeChild, onCompleteParams:[presenceLocked] } );
                    }
                    break;
                    
                case 109: // Keypad "-"
                    break;
                    
                case 111: // Keypad "/"
                    break;
                    
                case 106: // Keypad "*"
                    break;
                    
                default:
                    break;
            }
        }
        
        
        private function onOnscreenKeyboard(keyboard:DisplayObject):void {
            
            Settings.keyboard = MovieClip(keyboard);
        }
        
        
        private function onSettingsLoad(settings:DisplayObject):void {
            
            mainSettings = OnScreenMenuSettings(settings);
        }
    }
}